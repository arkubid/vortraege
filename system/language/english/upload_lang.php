<?php

$lang['upload_userfile_not_set']        =   "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit']      =   "Die ausgewählte Datei ist größer als 4 MB, bitte verkleinern.";
$lang['upload_file_exceeds_form_limit'] =   "Die ausgewählte Datei ist größer als 4 MB, bitte verkleinern.";
$lang['upload_file_partial']            =   "Die Datei wurde fehlerhaft hochgeladen.";
$lang['upload_no_temp_directory']       =   "Es ist kein TMP Verzeichnis vorhanden.";
$lang['upload_unable_to_write_file']    =   "Die Datei konnte nicht angelegt werden.";
$lang['upload_stopped_by_extension']    =   "Der Datei Upload wurde unterbrochen.";
$lang['upload_no_file_selected']        =   "You did not select a file to upload.";
$lang['upload_invalid_filetype']        =   "Dieser Dateityp ist nicht erlaubt.";
$lang['upload_invalid_filesize']        =   "Die ausgewählte Datei ist größer als 4 MB, bitte verkleinern.";
$lang['upload_invalid_dimensions']      =   "The image you are attempting to upload exceedes the maximum height or width.";
$lang['upload_destination_error']       =   "A problem was encountered while attempting to move the uploaded file to the final destination.";
$lang['upload_no_filepath']             =   "Der Pfad zum Speichern der Datei ist fehlerhaft.";
$lang['upload_no_file_types']           =   "You have not specified any allowed file types.";
$lang['upload_bad_filename']            =   "The file name you submitted already exists on the server.";
$lang['upload_not_writable']            =   "The upload destination folder does not appear to be writable.";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */