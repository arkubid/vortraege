<?php

$lang['required'] 			= "Das Feld <b>%s</b> muss ausgef&uuml;llt werden.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "Es muss eine valide E-Mail Adresse angegeben werden.";
$lang['valid_emails'] 		= "The %s field must contain all valid email addresses.";
$lang['valid_url'] 			= "The %s field must contain a valid URL.";
$lang['valid_ip'] 			= "The %s field must contain a valid IP.";
$lang['min_length']			= "The %s field must be at least %s characters in length.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "The %s field must contain only numbers.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['matches']			= "The %s field does not match the %s field.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "Es muss eine Wert für das Feld <b>%s</b> ausgewählt werden.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */