<div class="main">
    <div class="post">
        <div class="post-body">            
            <?php echo validation_errors(); ?>            
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>ort/update" method="post">
                <input type="hidden" name="id" id="id" value="<?php echo $arr_Result['id'];?>">
                <table>
                    <tr>
                        <td class="caption">Ort:</td>
                        <td> <input type="text" name="ort" id="ort" value="<?php echo $arr_Result['ort']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Adresse:</td>
                        <td> <input type="text" name="adresse" id="adresse" value="<?php echo $arr_Result['adresse']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Webseite:</td>
                        <td> <input type="text" name="website" id="website" value="<?php echo $arr_Result['website']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Google Maps:</td>
                        <td> <input type="text" name="maps" id="maps" value="<?php echo $arr_Result['maps']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="stadt" name="stadt">
                                <option value="0"></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>
                                    <option value="<?php echo $arr_Element['id']; ?>"
                                        <?php echo ($arr_Element['id'] == $arr_Result['stadt']) ? 'selected' : '';?>>
                                        <?php echo $arr_Element['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="caption">Aktiv:</td>
                        <td>
                            <input type="checkbox" name="activated" id="activated" <?php echo ($arr_Result['activated'] == 1) ? 'checked' : ''; ?>> Ja
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="update" name="update" value="Speichern">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>ort/index'">
                        </td>
                    </tr>
                </table>                       
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>