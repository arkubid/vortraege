
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
            <?php endif; ?>
            <table class="data-table">
                <thead>
                    <th class="small"></th>
                    <th>
                        <a href="<?php echo base_url();?>ort/index/ort/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'ort') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Ort
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>ort/index/stadt/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'stadt') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Stadt
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>ort/index/adresse/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'adresse') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Adresse
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>ort/index/website/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'website') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Webseite
                        </a>
                    </th>
                    
                    <th class="small">
                        <a href="<?php echo base_url();?>ort/index/activated/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'activated') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Aktiv
                        </a>
                    </th>             
                </thead>
                <?php $b_Color = false; ?>
                <?php foreach($arr_Result as $arr_Element) : ?>                    
                    <tr <?php echo ($b_Color == true) ? 'class="even"' : ''?>>
                        <td>
                            <a href="<?php echo base_url() . 'ort/edit/' . $arr_Element['id']; ?>" class="ort-edit"></a>&nbsp;
                        </td>
                        <td>
                            <?php echo $arr_Element['ort']; ?>
                        </td>
                        <td>
                            <?php
                                foreach($arr_Stadt as $arr_StadtElement) {
                                    if ($arr_StadtElement['id'] == $arr_Element['stadt']) {
                                        echo $arr_StadtElement['name'];
                                        break;
                                    }
                                }
                            ?>                            
                        </td>
                        <td>
                            <?php echo $arr_Element['adresse']; ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['website']; ?>
                        </td>
                        
                        <td>
                            <?php if ($arr_Element['activated'] == '1') : ?>
                                <a href="<?php echo base_url() . 'ort/deactivate/' . $arr_Element['id']; ?>" class="activ"></a>
                            <?php else : ?>
                                <a href="<?php echo base_url() . 'ort/activate/' . $arr_Element['id']; ?>" class="inactiv"></a>
                            <?php endif; ?>
                        </td>                        
                    </tr>
                    <?php $b_Color = ($b_Color == true) ? false : true; ?>
                <?php endforeach; ?>
            </table>                           
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <div class="clearer"> </div>
</div>
