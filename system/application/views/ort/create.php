<div class="main">
    <div class="post">
        <div class="post-body">            
            <?php echo validation_errors(); ?>            
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>ort/save" method="post">
                <table>
                    <tr>
                        <td class="caption">Ort:</td>
                        <td> <input type="text" name="ort" id="ort" value="<?php echo set_value('ort'); ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Adresse:</td>
                        <td> <input type="text" name="adresse" id="adresse" value="<?php echo set_value('adresse'); ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Webseite:</td>
                        <td> <input type="text" name="website" id="website" value="<?php echo set_value('website'); ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Google Maps:</td>
                        <td> <input type="text" name="maps" id="maps" value="<?php echo set_value('maps'); ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="stadt" name="stadt">
                                <option value="0"></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>
                                    <option value="<?php echo $arr_Element['id']; ?>">
                                        <?php echo $arr_Element['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="caption">Aktiv:</td>
                        <td>
                            <input type="checkbox" name="activated" id="activated" checked> Ja
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="save" name="save" value="Speichern">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>ort/index'">
                        </td>
                    </tr>
                </table>                       
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>