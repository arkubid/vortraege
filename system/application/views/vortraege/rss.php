<?php echo '<?xml version="1.0" encoding="' . $str_Encoding . '" ?>'; ?>
<rss version="2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:admin="http://webns.net/mvcb/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:atom="http://www.w3.org/2005/Atom">

    <channel>
        <title><?php echo $str_Name; ?></title>
        <link><?php echo $str_Url; ?></link>
        <description><?php echo xml_convert($str_Description); ?></description>
        <dc:language><?php echo $str_Language; ?></dc:language>
        <dc:creator><?php echo $str_Mail; ?></dc:creator>
        <dc:rights>Copyright <?php echo gmdate('%Y', time()); ?></dc:rights>        
        <admin:generatorAgent rdf:resource="http://www.ai-vortraege.uni-bonn.de/" />
        <atom:link href="<?php echo $str_Url; ?>" rel="self" type="application/rss+xml" />

        <?php foreach($arr_Data as $arr_Row): ?>
            <item>
              <title><?php echo xml_convert($arr_Row['titel']); ?></title>
              <description>
                    <?php if (isset($arr_Row['Content'])) : ?>
                        <?php echo $arr_Row['Content']; ?>
                    <?php else : ?>
                        <![CDATA[
                            <?php
                                $i_Length   =   count($arr_Row['Body']);
                                echo '<table >';
                                for($i = 0; $i < $i_Length; $i++) {
                                    echo '<tr>',
                                            '<td><b>', $arr_Row['Body'][$i]['Titel'], ':</b></td>',
                                            '<td>', $arr_Row['Body'][$i]['Value'], '</td>',
                                         '</tr>';
                                }
                                echo '</table>';
                            ?>
                        ]]>
                    <?php endif; ?>
              </description>
              
              <link><?php echo $arr_Row['link']; ?></link>
              <guid><?php echo $arr_Row['link']; ?></guid>
            </item>
        <?php endforeach; ?>
    </channel>
</rss>
