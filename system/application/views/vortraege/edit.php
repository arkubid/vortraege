<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
            <div id="error" class="error"><?php echo validation_errors(); ?></div>
            <?php endif; ?>
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>vortraege/update" method="post" enctype="multipart/form-data">
                <input type="hidden" name="vortragID" id="vortragID" value="<?php echo $arr_Result['vortragID'];?>">
                <input type="hidden" name="belegungID" id="belegungID" value="<?php echo $arr_Result['belegungID'];?>">
                <input type="hidden" name="url" id="url" value="<?php echo $arr_Result['url'];?>">

                <table>
                    <tr>
                        <td class="caption">Datum:</td>
                        <td> <input type="text" name="datum" id="datum" value="<?php echo $arr_Result['datum'];?>" size="10" onchange="isRoomFree();"></td>
                    </tr>
                    <tr>
                        <td class="caption">Beginn:</td>
                        <td>
                            <?php $arr_Beginn = explode(':', $arr_Result['beginn']); ?>
                            <select name="beginnHour" id="beginnHour" onchange="isRoomFree();" class="short">
                                <?php for($i = 0; $i < 24; $i++) : ?>
                                <option value="<?php echo ($i < 10) ? '0' : ''; echo $i;?>"
                                    <?php echo ($arr_Beginn[0] == $i || $arr_Beginn[0] == '0' . $i)? 'selected' : '';?>>
                                    <?php echo ($i < 10) ? '0' : ''; echo $i;?>
                                </option>
                                <?php endfor; ?>
                            </select>
                            <select name="beginnMinute" id="beginnMinute" onchange="isRoomFree();" class="short">
                                <option value="00" <?php echo ($arr_Beginn[1] == '00')? 'selected' : '';?>>00</option>
                                <option value="15" <?php echo ($arr_Beginn[1] == '15')? 'selected' : '';?>>15</option>
                                <option value="30" <?php echo ($arr_Beginn[1] == '30')? 'selected' : '';?>>30</option>
                                <option value="45" <?php echo ($arr_Beginn[1] == '45')? 'selected' : '';?>>45</option>
                            </select>
                            <input type="checkbox" name="ct" id="ct" <?php echo ($arr_Result['ct'] == 1) ? 'checked' : ''?>> c. t.
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Ende:</td>
                        <td>
                            <?php $arr_Ende = explode(':', $arr_Result['ende']); ?>
                            <select name="endeHour" id="endeHour" onchange="isRoomFree();" class="short">
                                <?php for($i = 0; $i < 24; $i++) : ?>
                                <option value="<?php echo ($i < 10) ? '0' : ''; echo $i;?>"
                                    <?php echo ($arr_Ende[0] == $i || $arr_Ende[0] == '0' . $i)? 'selected' : '';?>>
                                    <?php echo ($i < 10) ? '0' : ''; echo $i;?>
                                </option>
                                <?php endfor; ?>
                            </select>
                            <select name="endeMinute" id="endeMinute" onchange="isRoomFree();" class="short">
                                <option value="00" <?php echo ($arr_Ende[1] == '00')? 'selected' : '';?>>00</option>
                                <option value="15" <?php echo ($arr_Ende[1] == '15')? 'selected' : '';?>>15</option>
                                <option value="30" <?php echo ($arr_Ende[1] == '30')? 'selected' : '';?>>30</option>
                                <option value="45" <?php echo ($arr_Ende[1] == '45')? 'selected' : '';?>>45</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Referent:</td>
                        <td>
                            <input type="text" id="referent" name="referent" value="<?php echo $arr_Result['referent'];?>" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Universit&auml;t:</td>
                        <td>
                            <input type="text" id="herkunft" name="herkunft" value="<?php echo $arr_Result['herkunft'];?>" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Titel:</td>
                        <td>
                            <input type="text" id="titel" name="titel" value="<?php echo $arr_Result['titel'];?>" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Ort:</td>
                        <td>
                            <select id="ort" name="ort" onChange="updateCity();">
                            <?php foreach ($arr_Ort as $arr_Element) : ?>
                            <option value="<?php echo $arr_Element['id']; ?>"
                                <?php echo ($arr_Element['id'] == $arr_Result['ort']) ? 'selected' : '';?>>
                                <?php echo $arr_Element['ort']; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="stadt" name="stadt">
                                <option value=""></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>"
                                    <?php echo ($arr_Element['id'] == $arr_Result['stadt']) ? 'selected' : '';?>>
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Veranstalter:</td>
                        <td>
                            <select id="veranstalter" name="veranstalter" onchange="updateArt();">
                                <?php foreach ($arr_Veranstalter as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['veranstalterID']; ?>"
                                    <?php echo ($arr_Element['veranstalterID'] == $arr_Result['veranstalter']) ? 'selected' : '';?>>
                                    <?php echo $arr_Element['veranstalterName']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Art:</td>
                        <td>
                            <select id="type" name="type">
                                <option value=""></option>
                                <?php foreach ($arr_Type as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>"
                                    <?php echo ($arr_Element['id'] == $arr_Result['type']) ? 'selected' : '';?>>
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Anzeigen:</td>
                        <td>
                            <input type="checkbox" name="activ" id="activ" <?php echo ($arr_Result['activ'] == '1') ? 'checked' : '';?>> Ja
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Kolloquium:</td>
                        <td>
                            <input type="checkbox" name="kolloquium" id="kolloquium" <?php echo ($arr_Result['kolloquium'] == '1') ? 'checked' : '';?>> Ja
                        </td>
                    </tr>
                    <?php if ($arr_Result['attachment'] == 1) : ?>
                    <tr>
                        <td class="caption">Datei:</td>
                        <td>
                            <a href="<?php echo base_url();?>attachment/vortrag<?php echo $arr_Result['vortragID']; ?>.pdf" target="_blank">Anzeigen</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Datei löschen:</td>
                        <td>
                            <input type="checkbox" id="attachmentDelete" name="attachmentDelete"> Ja
                        </td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td class="caption">Datei anhängen:</td>
                        <td>
                            <input type="file" name="attachment" id="attachment">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Link:</td>
                        <td>
                            <input type="text" name="link" id="link" value="<?php echo $arr_Result['link'];?>" size="100"><?php if ($arr_Result['link'] != '') : ?>&nbsp;&nbsp;<a href="<?php echo $arr_Result['link'];?>" target="_blank">Link</a><?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="save" name="save" value="Speichern">
                            <input type="button" id="delete" name="delete" value="Löschen" onclick="location.href='<?php echo base_url();?>vortraege/delete/<?php echo $arr_Result['vortragID']; ?>/<?php echo $arr_Result['belegungID']; ?>'">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/actual'">
                        </td>
                    </tr>
                </table>           
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>

<script type="text/javascript">
    /**
     *  Wählt anhand des ausgewählten Veranstalungsortes die dazu gehörige Stadt aus
     */
    function updateCity() {
        var arr_City    =   new Array();
        <?php
            foreach ($arr_Ort as $arr_Element) {
                echo 'arr_City[', $arr_Element['id'], '] = ', $arr_Element['stadt'], ';';
            }
        ?>

        var i_Ort   =   $('#ort').val();
        var i_Stadt =   arr_City[i_Ort];
        $('#stadt').val(i_Stadt);
    }

    /**
     *  Wählt anhand des asugewählten Veranstalters die dazu gehörige Art des Vortrages aus
     */
    function updateArt() {
        var arr_Art    =   new Array();
        <?php
            foreach ($arr_Veranstalter as $arr_Element) {
                echo 'arr_Art[', $arr_Element['veranstalterID'], '] = ', $arr_Element['veranstalterType'], ';';
            }
        ?>

        var i_Veranstalter  =   $('#veranstalter').val();
        var i_Art           =   arr_Art[i_Veranstalter];
        $('#type').val(i_Art);
    }

    $.getScript('<?php echo base_url(); ?>js/InputValidation.js');
</script>