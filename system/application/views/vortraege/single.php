<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
            <div id="error" class="error"><?php echo validation_errors(); ?></div>
            <?php endif; ?>
            <div id="room"></div>
            <div id="time"></div>

            <table>
                <tr>
                    <td colspan="2" class="SingleTableDate">
                        <?php echo gDay($arr_Result['datum']) . ', den ' . formatDate($arr_Result['datum'], '%d. %B %Y'); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="SingleTableReferent">                        
                        <?php echo $arr_Result['referent']; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="SingleTableHerkunft">
                        <?php echo $arr_Result['herkunft']; ?>                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="SingleTableTitel"><?php echo $arr_Result['titel']; ?></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td><b>Beginn:</b></td>
                    <td>
                        <?php echo gTime($arr_Result['beginn']) . ' Uhr '; ?>
                        <?php echo ($arr_Result['ct'] == 1) ? ' c. t.' : ' s. t.';?>
                    </td>
                </tr>                
                <tr>
                    <td><b>Veranstalter:</b></td>
                    <td>
                        <?php foreach ($arr_Veranstalter as $arr_Element) : ?>
                                <?php echo ($arr_Element['veranstalterID'] == $arr_Result['veranstalter']) ? $arr_Element['veranstalterName'] : '';?>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <td><b>Ort:</b></td>
                    <td>
                        <?php foreach ($arr_Ort as $arr_Element) : ?>
                                <?php echo ($arr_Element['id'] == $arr_Result['ort']) ? $arr_Element['adresse'] : '';?>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <?php if ($arr_Result['link'] != '') : ?>
                <tr>
                    <td><b>Link:</b></td>
                    <td>
                        <a href="<?php echo $arr_Result['link']; ?>" target="_blank">Weitere Informationen</a>                        
                    </td>
                </tr>
                <?php endif; ?>
            </table>           
        </div>
    </div>
    <div class="clearer"> </div>
</div>
