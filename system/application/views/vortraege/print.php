<div class="main">
    <div class="post">
        <div class="post-body">            
            <?php echo validation_errors(); ?>            
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>vortraege/printPDF" method="post" target="_blank">
                <table>
                    <tr>
                        <td class="caption">Überschrift:</td>
                        <td><input type="text" id="titel" name="titel" value="Archäologische Vorträge in Bonn und Köln" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Untertitel:</td>
                        <td><input type="text" id="subtitel" name="subtitel" value="" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Zeilen Abstand:</td>
                        <td><input type="text" id="distance" name="distance" value="8" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Anzeige Art:</td>
                        <td>
                            <select id="display" name="display">
                                <option value="vortrag">Vortrag</option>
                                <option value="kolloquium">Kolloquium</option>
                            </select>
                        </td>
                    </tr>
                </table>
                
                <h4>Zu filternde Einträge</h4>
                <table>                   
                    <tr>
                        <td class="caption">Ort:</td>
                        <td>
                            <select id="ort[]" name="ort[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Ort as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['ort']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="stadt[]" name="stadt[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Veranstalter:</td>
                        <td>
                            <select id="veranstalter[]" name="veranstalter[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Veranstalter as $arr_Element) : ?>                                
                                <option value="<?php echo $arr_Element['veranstalterID']; ?>">
                                    <?php echo $arr_Element['veranstalterName']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Art:</td>
                        <td>
                            <select id="type[]" name="type[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Type as $arr_Element) : ?>                                
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Kolloquium:</td>
                        <td>
                            <input type="radio" name="kolloquium" id="kolloquium" value="1"> Nur Kolloquien
                            <input type="radio" name="kolloquium" id="kolloquium" value="0" checked> Keine Kolloquien
                            <input type="radio" name="kolloquium" id="kolloquium" value="-1"> Vorträge und Kolloquien
                        </td>
                    </tr>
                </table>
                <h4>Zu markierende Einträge</h4>
                <i>Nur für Anzeige Art "Vortrag"</i>
                <table>
                   <tr>
                        <td class="caption">Ort:</td>
                        <td>
                            <select id="markerort" name="markerort">
                                <option value=""></option>
                                <?php foreach ($arr_Ort as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['ort']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="markerstadt" name="markerstadt">
                                <option value=""></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Veranstalter:</td>
                        <td>
                            <select id="markerveranstalter" name="markerveranstalter">
                                <option value=""></option>
                                <?php foreach ($arr_Veranstalter as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['veranstalterID']; ?>">
                                    <?php echo $arr_Element['veranstalterName']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Art:</td>
                        <td>
                            <select id="markertype" name="markertype">
                                <option value=""></option>
                                <?php foreach ($arr_Type as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="save" name="save" value="PDF erstellen">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/setPrintParameter'">
                        </td>
                    </tr>
                </table>                       
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>

<script type="text/javascript">
    /**
     *  Wählt anhand des ausgewählten Veranstalungsortes die dazu gehörige Stadt aus
     */
    function updateCity() {
        var arr_City    =   new Array();
        <?php
            foreach ($arr_Ort as $arr_Element) {
                echo 'arr_City[', $arr_Element['id'], '] = ', $arr_Element['stadt'], ';';
            }
        ?>

        var i_Ort   =   $('#ort').val();
        var i_Stadt =   arr_City[i_Ort];
        $('#stadt').val(i_Stadt);
    }

    /**
     *  Wählt anhand des asugewählten Veranstalters die dazu gehörige Art des Vortrages aus
     */
    function updateArt() {
        var arr_Art    =   new Array();
        <?php
            foreach ($arr_Veranstalter as $arr_Element) {
                echo 'arr_Art[', $arr_Element['veranstalterID'], '] = ', $arr_Element['veranstalterType'], ';';
            }
        ?>

        var i_Veranstalter  =   $('#veranstalter').val();
        var i_Art           =   arr_Art[i_Veranstalter];
        $('#type').val(i_Art);
    }

    $.getScript('<?php echo base_url(); ?>js/InputValidation.js');
</script>