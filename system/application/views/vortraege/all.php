
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
            <?php endif; ?>
            <table class="data-table">
                <thead>
                    <th class="small"></th>
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/datum/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'datum') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Datum
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/referent/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'referent') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Referent
                        </a>
                    </th>                    
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/titel/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'titel') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Titel
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/ort/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'ort') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Ort
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/veranstalter/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'veranstalter') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Veranstalter
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/beginn/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'beginn') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Beginn
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>vortraege/all/activ/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'activ') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Aktiv
                        </a>
                    </th>
                </thead>
                <?php $b_Color = false; ?>
                <?php foreach($arr_Result as $arr_Element) : ?>                    
                    <tr <?php echo ($b_Color == true) ? 'class="even"' : ''?>>
                        <td>
                            <a href="<?php echo base_url() . 'vortraege/edit/' . $arr_Element['vortragID']; ?>" class="edit"></a>&nbsp;
                        </td>
                        <td>
                            <?php echo gDate($arr_Element['datum']); ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['referent']; ?>
                        </td>                        
                        <td>
                            <?php echo $arr_Element['titel']; ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['ort']; ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['veranstalterName']; ?>
                        </td>
                        <td>
                            <?php echo gTime($arr_Element['beginn']); ?> Uhr <?php echo ($arr_Element['ct'] == '1') ? 'c.t.' : 's.t.';?>
                        </td>
                        <td>                            
                            <?php if ($arr_Element['activ'] == '1') : ?>
                                <a href="<?php echo base_url() . 'vortraege/activate/' . $arr_Element['vortragID']; ?>/0">
                                <img src="<?php echo base_url();?>css/images/icon-activ.png" alt="Aktiv">
                                </a>
                            <?php else : ?>
                                <a href="<?php echo base_url() . 'vortraege/activate/' . $arr_Element['vortragID']; ?>/1">
                                <img src="<?php echo base_url();?>css/images/icon-inactiv.png" alt="Inaktiv">
                                </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php $b_Color = ($b_Color == true) ? false : true; ?>
                <?php endforeach; ?>
            </table>                           
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <div class="clearer"> </div>
</div>
