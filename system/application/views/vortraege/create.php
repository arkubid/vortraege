<div class="main">
    <div class="post">
        <div class="post-body">            
            <?php echo validation_errors(); ?>            
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>vortraege/save" method="post" enctype="multipart/form-data">
                <table>
                    <tr>
                        <td class="caption">Datum:</td>
                        <td> <input type="text" name="datum" id="datum" value="" size="10" onchange="isRoomFree();"></td>
                    </tr>
                    <tr>
                        <td class="caption">Beginn:</td>
                        <td>
                            <select name="beginnHour" id="beginnHour" onchange="isRoomFree();" class="short">
                                <?php for($i = 0; $i < 24; $i++) : ?>
                                <option value="<?php echo ($i < 10) ? '0' : ''; echo $i;?>">
                                    <?php echo ($i < 10) ? '0' : ''; echo $i;?>
                                </option>
                                <?php endfor; ?>
                            </select>
                            <select name="beginnMinute" id="beginnMinute" onchange="isRoomFree();" class="short">
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                            <input type="checkbox" name="ct" id="ct"> c. t.
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Ende:</td>
                        <td>
                            <select name="endeHour" id="endeHour" onchange="isRoomFree();" class="short">
                                <?php for($i = 0; $i < 24; $i++) : ?>
                                <option value="<?php echo ($i < 10) ? '0' : ''; echo $i;?>">
                                    <?php echo ($i < 10) ? '0' : ''; echo $i;?>
                                </option>
                                <?php endfor; ?>
                            </select>
                            <select name="endeMinute" id="endeMinute" onchange="isRoomFree();" class="short">
                                <option value="00">00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Referent:</td>
                        <td>
                            <input type="text" id="referent" name="referent" value="" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Universit&auml;t:</td>
                        <td>
                            <input type="text" id="herkunft" name="herkunft" value="" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Titel:</td>
                        <td>
                            <input type="text" id="titel" name="titel" value="" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Ort:</td>
                        <td>
                            <select id="ort" name="ort" onChange="updateCity();">
                                <option value=""></option>
                                <?php foreach ($arr_Ort as $arr_Element) : ?>
                                    <?php if (isset($arr_Element['activated']) == false | $arr_Element['activated'] == 1) : ?>
                                        <option value="<?php echo $arr_Element['id']; ?>">
                                            <?php echo $arr_Element['ort']; ?>
                                        </option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="stadt" name="stadt">
                                <option value=""></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>

                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Veranstalter:</td>
                        <td>
                            <select id="veranstalter" name="veranstalter" onchange="updateArt();">
                                <option value=""></option>
                                <?php foreach ($arr_Veranstalter as $arr_Element) : ?>
                                    <?php if (isset($arr_Element['activated']) == false | $arr_Element['activated'] == 1) : ?>
                                        <option value="<?php echo $arr_Element['veranstalterID']; ?>">
                                            <?php echo $arr_Element['veranstalterName']; ?>
                                        </option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Art:</td>
                        <td>
                            <select id="type" name="type">
                                <option value=""></option>
                                <?php foreach ($arr_Type as $arr_Element) : ?>                                
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Anzeigen:</td>
                        <td>
                            <input type="checkbox" name="activ" id="activ"> Ja
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Kolloquium:</td>
                        <td>
                            <input type="checkbox" name="kolloquium" id="kolloquium"> Ja
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Datei anhängen:</td>
                        <td>
                            <input type="file" name="attachment" id="attachment">
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Link:</td>
                        <td>
                            <input type="text" name="link" id="link" size="100">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="save" name="save" value="Speichern">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/all'">
                        </td>
                    </tr>
                </table>                       
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>

<script type="text/javascript">
    /**
     *  Wählt anhand des ausgewählten Veranstalungsortes die dazu gehörige Stadt aus
     */
    function updateCity() {
        var arr_City    =   new Array();
        <?php
            foreach ($arr_Ort as $arr_Element) {
                echo 'arr_City[', $arr_Element['id'], '] = ', $arr_Element['stadt'], ';';
            }
        ?>

        var i_Ort   =   $('#ort').val();
        var i_Stadt =   arr_City[i_Ort];
        $('#stadt').val(i_Stadt);
    }

    /**
     *  Wählt anhand des asugewählten Veranstalters die dazu gehörige Art des Vortrages aus
     */
    function updateArt() {
        var arr_Art    =   new Array();
        <?php
            foreach ($arr_Veranstalter as $arr_Element) {
                echo 'arr_Art[', $arr_Element['veranstalterID'], '] = ', $arr_Element['veranstalterType'], ';';
            }
        ?>

        var i_Veranstalter  =   $('#veranstalter').val();
        var i_Art           =   arr_Art[i_Veranstalter];
        $('#type').val(i_Art);
    }

    $.getScript('<?php echo base_url(); ?>js/InputValidation.js');
</script>