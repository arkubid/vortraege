
<div class="main">
    <div class="post">
        <div class="post-body">
            <table class="liste">
                <?php foreach($arr_Daten as $arr_Element) : ?>                
                <tr <?php echo ($arr_Element['datum'] ==  date('Y-m-d')) ? 'class="today"' : ''; ?>
                    onclick="javascript:window.location.href='<?php echo base_url() . 'vortraege/single/' . $arr_Element['vortragID']; ?>'">
                    <td>
                        <?php if ($arr_Element['datum'] ==  date('Y-m-d')) : ?>
                            <p>Heute</p>
                        <?php else : ?>
                            <?php echo gDate($arr_Element['datum']); ?><br>
                            <i><?php echo gDay($arr_Element['datum']); ?></i>
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php echo ($arr_Element['referent'] !== '') ? $arr_Element['referent'] : ''; ?>
                        <?php echo ($arr_Element['herkunft'] !== '') ? '(' . $arr_Element['herkunft'] . '),' : ''; ?>
                        <b>"<?php echo $arr_Element['titel']; ?>"</b>,
                        <?php if ($arr_Element['veranstalterDisplay'] !== '') : ?>
                            (<acronym title="<?php echo $arr_Element['veranstalterName']; ?>"><?php echo $arr_Element['veranstalterDisplay']; ?></acronym>),
                        <?php endif; ?>                       
                        <acronym title="<?php echo $arr_Element['adresse']; ?>"><a href="<?php echo $arr_Element['maps']; ?>" target="_blank"><?php echo $arr_Element['ort'];?></a></acronym><?php if ($arr_Element['kolloquium'] == 1) : ?><?php echo ' <i>(Kolloquium)</i>'; ?><?php endif; ?><?php echo ', '. gTime($arr_Element['beginn']); ?> Uhr
                        <?php echo ($arr_Element['ct'] == '1') ? 'c. t.' : 's. t.';?>

                        <?php if ($arr_Element['attachment'] == 1) : ?>
                            (<a href="<?php echo base_url();?>attachment/vortrag<?php echo $arr_Element['vortragID']?>.pdf" target="_blank">siehe Anhang</a>)
                        <?php endif; ?>
                            
                        <?php if ($arr_Element['link'] != '') : ?>
                            (<a href="<?php echo $arr_Element['link']?>" target="_blank">weitere Informationen</a>)
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>                           
        </div>
    </div>
    <div class="clearer"> </div>
</div>           