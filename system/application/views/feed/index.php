
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
            <?php endif; ?>
            <table class="data-table">
                <thead>
                    <th class="small"></th>
                    <th>
                        <a href="<?php echo base_url();?>feed/index/link/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'link') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Link
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>feed/index/titel/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'titel') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Titel
                        </a>
                    </th> 
                    <th>
                        Vollständiger Link
                    </th>
                </thead>
                <?php $b_Color = false; ?>
                <?php foreach($arr_Result as $arr_Element) : ?>                    
                    <tr <?php echo ($b_Color == true) ? 'class="even"' : ''?>>
                        <td>
                            <a href="<?php echo base_url() . 'feed/edit/' . $arr_Element['id']; ?>" class="feed-edit"></a>&nbsp;
                        </td>
                        <td>
                            <?php echo $arr_Element['link']; ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['titel']; ?>
                        </td>   
                        <td>
                            <a href="<?php echo base_url();?>feed/rss/<?php echo $arr_Element['link']; ?>" target="_blank">
                                <?php echo base_url();?>feed/rss/<?php echo $arr_Element['link']; ?>
                            </a>
                        </td>
                    </tr>
                    <?php $b_Color = ($b_Color == true) ? false : true; ?>
                <?php endforeach; ?>
            </table>                           
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <div class="clearer"> </div>
</div>
