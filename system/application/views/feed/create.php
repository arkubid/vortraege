<div class="main">
    <div class="post">
        <div class="post-body">      
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
            <?php endif; ?>
            <?php echo validation_errors(); ?>            
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>feed/save" method="post">
                <table>
                    <tr>
                        <td class="caption">Link:</td>
                        <td> <input type="text" name="link" id="link" value="<?php echo set_value('link'); ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Titel:</td>
                        <td> <input type="text" name="titel" id="titel" value="<?php echo set_value('titel'); ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Ort:</td>
                        <td>
                            <select id="ort[]" name="ort[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Ort as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['ort']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Stadt:</td>
                        <td>
                            <select id="stadt[]" name="stadt[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Stadt as $arr_Element) : ?>
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Veranstalter:</td>
                        <td>
                            <select id="veranstalter[]" name="veranstalter[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Veranstalter as $arr_Element) : ?>                                
                                <option value="<?php echo $arr_Element['veranstalterID']; ?>">
                                    <?php echo $arr_Element['veranstalterName']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Art:</td>
                        <td>
                            <select id="type[]" name="type[]" multiple="multiple">
                                <option value=""></option>
                                <?php foreach ($arr_Type as $arr_Element) : ?>                                
                                <option value="<?php echo $arr_Element['id']; ?>">
                                    <?php echo $arr_Element['name']; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Kolloquium:</td>
                        <td>
                            <input type="radio" name="kolloquium" id="kolloquium" value="1"> Nur Kolloquien
                            <input type="radio" name="kolloquium" id="kolloquium" value="0"> Keine Kolloquien
                            <input type="radio" name="kolloquium" id="kolloquium" value="-1"> Vorträge und Kolloquien
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="save" name="save" value="Speichern">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>feed/index'">
                        </td>
                    </tr>
                </table>                       
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>