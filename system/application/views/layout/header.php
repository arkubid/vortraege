<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content=""/>
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<link rel="stylesheet" type="text/css" href="<?php echo site_url() . '/';?>css/styles.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url() . '/';?>css/jQuery.UI.css" media="screen" />
        <script type="text/javascript" src="<?php echo site_url();?>/js/jQuery.js" ></script>
        <script type="text/javascript" src="<?php echo site_url();?>/js/jQuery.UI.js" ></script>
        <script type="text/javascript" src="<?php echo site_url();?>/js/jQuery.UI.de.js" ></script>
        
        <title>Archäologische Vortr&auml;ge in Bonn und K&ouml;ln</title>
</head>

<body id="top">
    <div id="site">
	<div class="center-wrapper">
            <div id="header">
                <div id="site-title">
                    <h1><span>Arch&auml;ologische Vortr&auml;ge in Bonn und K&ouml;ln</span></h1>
                </div>                