<div id="navigation">
    <div id="main-nav">
        <ul class="tabbed">
            <li <?php echo ($str_Activ == 'vortraege') ? 'class="current-tab"' : ''; ?>>
                <a href="<?php echo site_url();?>vortraege">Vortr&auml;ge</a>
            </li>
            <li <?php echo ($str_Activ == 'newsletter') ? 'class="current-tab"' : ''; ?>>
                <a href="<?php echo site_url();?>newsletter">Newsletter</a>
            </li>
            <?php if ($b_LogedIn == true) : ?>
                <li <?php echo ($str_Activ == 'configuration') ? 'class="current-tab"' : ''; ?>>
                    <a href="<?php echo site_url();?>configuration">Einstellungen</a>
                </li>
            <?php endif; ?>
            <li <?php echo ($str_Activ == 'help') ? 'class="current-tab"' : ''; ?>>
                <a href="<?php echo site_url();?>help">Hilfe</a>
            </li>
        </ul>
        <div class="clearer">&nbsp;</div>
    </div>
    <div id="sub-nav">
        <ul class="tabbed">
            <?php foreach($arr_SubMenu as $arr_Element) : ?>
                <?php if ($arr_Element['b_Login'] == false || ($b_LogedIn == true && $arr_Element['b_Login'] == true)) : ?>
                <li>
                    <a href="
                        <?php if ($arr_Element['b_Foreign'] == false) : ?>
                            <?php echo site_url() . $str_Activ . '/' . $arr_Element['str_Target']; ?>"
                        <?php else : ?>
                            <?php echo $arr_Element['str_Target']; ?>"
                        <?php endif; ?>
                        <?php echo ($arr_Element['str_Class'] !== '') ? 'class="' . $arr_Element['str_Class'] . '"' : '';?>
                        target="<?php echo $arr_Element['str_Window']; ?>">
                        <?php echo $arr_Element['str_Display']; ?>
                    </a>
                </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <div class="clearer">&nbsp;</div>
    </div>
</div>
            
