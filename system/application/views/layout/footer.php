            <div id="footer">
                <div class="left">
                    &copy; 2010 Benjamin Gei&szlig;ler <span class="text-separator">&rarr;</span>
                    <a href="<?php echo base_url()?>vortraege">Vortr&auml;ge</a>
                    <span class="text-separator">|</span>
                    <a href="<?php echo base_url();?>newsletter">Newsletter</a>
                    <span class="text-separator">|</span>
                    <a href="<?php echo base_url();?>help">Hilfe</a>
                    <span class="text-separator">|</span>                    
                    <?php if ($b_LogedIn == true) : ?>
                        <a href="<?php echo base_url();?>auth/logout">Abmelden</a>
                    <?php else : ?>
                        <a href="<?php echo base_url();?>auth/login">Anmelden</a>
                    <?php endif; ?>
                </div>
                <div class="right"><a href="http://templates.arcsin.se/">website template basing on a template </a> by <a href="http://arcsin.se/">Arcsin</a></div>
                <div class="clearer">&nbsp;</div>
            </div>
	</div>
    </div>
</body>
</html>