<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content=""/>
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />
        <title>Archäologische Vortr&auml;ge in Bonn und K&ouml;ln</title>
</head>

<body id="top">
    <div id="site">
	<div class="center-wrapper">
            <div id="header">
                <div id="site-title">
                    <h1><span>Arch&auml;ologische Votr&auml;ge in Bonn und K&ouml;ln</span></h1>
                </div>
                <div id="navigation">
                    <div id="main-nav">
                        <ul class="tabbed">
                            <li class="current-tab"><a href="#">Vortr&auml;ge</a></li>
                            <li><a href="#">Newsletter</a></li>
                            <li><a href="#">Raumbuchung</a></li>
                            <li><a href="#">Einstellungen</a></li>
                        </ul>
                        <div class="clearer">&nbsp;</div>
                    </div>
                    <div id="sub-nav">
                        <ul class="tabbed">
                            <li><a href="#" class="feed">RSS Feed</a></li>
                            <li><a href="vortraege/download" class="pdf">Download</a></li>
                            <li><a href="two-columns.html">Two Columns</a></li>
                            <li class="current-tab"><a href="single-column.html">Single Column</a></li>
                            <li><a href="archives.html">Archives</a></li>
                            <li><a href="empty-page.html">Empty Page</a></li>
                        </ul>
                        <div class="clearer">&nbsp;</div>
                    </div>
                </div>
            </div>

            <div class="main">
                <div class="post">
                    <div class="post-body">
                        <p class="large">
                            This is a free website template by <a href="http://arcsin.se/">Arcsin</a>, built using tableless XHTML and CSS.
                        </p>
                    </div>
                </div>
                <div class="clearer">&nbsp;</div>
            </div>

            <div id="footer">
                <div class="left">&copy; 2010 Benjamin Gei&szlig;ler <span class="text-separator">&rarr;</span> <a href="vortraege">Vortr&auml;ge</a> <span class="text-separator">|</span><a href="newsletter">Newsletter</a> <span class="text-separator">|</span> <a href="booking">Raumbuchung</a> </div>
                <div class="right"><a href="http://templates.arcsin.se/">Website template base on a template </a> by <a href="http://arcsin.se/">Arcsin</a></div>
                <div class="clearer">&nbsp;</div>
            </div>
	</div>
    </div>
</body>
</html>