
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
            <?php endif; ?>
            <table class="data-table">
                <thead>
                    <th class="small"></th>
                    <th>
                        <a href="<?php echo base_url();?>veranstalter/index/veranstalterName/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'veranstalterName') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Veranstalter
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>veranstalter/index/veranstalterShort/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'veranstalterShort') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Abkürzung
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>veranstalter/index/veranstalterDisplay/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'veranstalterDisplay') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Anzeige
                        </a>
                    </th>
                    <th>
                        <a href="<?php echo base_url();?>veranstalter/index/veranstalterType/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'veranstalterType') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Fachgebiet
                        </a>
                    </th>                    
                    <th class="small">
                        <a href="<?php echo base_url();?>veranstalter/index/activated/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'activated') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Aktiv
                        </a>
                    </th>             
                </thead>
                <?php $b_Color = false; ?>
                <?php foreach($arr_Result as $arr_Element) : ?>                    
                    <tr <?php echo ($b_Color == true) ? 'class="even"' : ''?>>
                        <td>
                            <a href="<?php echo base_url() . 'veranstalter/edit/' . $arr_Element['veranstalterID']; ?>" class="veranstalter-edit"></a>&nbsp;
                        </td>
                        <td>
                            <?php echo $arr_Element['veranstalterName']; ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['veranstalterShort']; ?>
                        </td>
                        <td>
                            <?php echo $arr_Element['veranstalterDisplay']; ?>
                        </td>
                        <td>
                            <?php
                                foreach($arr_Type as $arr_TypeElement) {
                                    if ($arr_TypeElement['id'] == $arr_Element['veranstalterType']) {
                                        echo $arr_TypeElement['name'];
                                        break;
                                    }
                                }
                            ?>                            
                        </td>                                               
                        <td>
                            <?php if ($arr_Element['activated'] == '1') : ?>
                                <a href="<?php echo base_url() . 'veranstalter/deactivate/' . $arr_Element['veranstalterID']; ?>" class="activ"></a>
                            <?php else : ?>
                                <a href="<?php echo base_url() . 'veranstalter/activate/' . $arr_Element['veranstalterID']; ?>" class="inactiv"></a>
                            <?php endif; ?>
                        </td>                        
                    </tr>
                    <?php $b_Color = ($b_Color == true) ? false : true; ?>
                <?php endforeach; ?>
            </table>                           
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <div class="clearer"> </div>
</div>
