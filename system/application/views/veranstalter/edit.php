<div class="main">
    <div class="post">
        <div class="post-body">            
            <?php echo validation_errors(); ?>            
            <div id="room"></div>
            <div id="time"></div>

            <form action="<?php echo base_url();?>veranstalter/update" method="post">
                <input type="hidden" name="veranstalterID" id="veranstalterID" value="<?php echo $arr_Result['veranstalterID'];?>">
                <table>
                    <tr>
                        <td class="caption">Veranstalter:</td>
                        <td> <input type="text" name="veranstalterName" id="veranstalterName" value="<?php echo $arr_Result['veranstalterName']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Abkürzung:</td>
                        <td> <input type="text" name="veranstalterShort" id="veranstalterShort" value="<?php echo $arr_Result['veranstalterShort']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Anzeige:</td>
                        <td> <input type="text" name="veranstalterDisplay" id="veranstalterDisplay" value="<?php echo $arr_Result['veranstalterDisplay']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Fachgebiet:</td>
                        <td>
                            <select id="veranstalterType" name="veranstalterType">
                                <option value="0"></option>
                                <?php foreach ($arr_Type as $arr_Element) : ?>
                                    <option value="<?php echo $arr_Element['id']; ?>"
                                        <?php echo ($arr_Element['id'] == $arr_Result['veranstalterType']) ? 'selected' : '';?>>
                                        <?php echo $arr_Element['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Webseite:</td>
                        <td> <input type="text" name="website" id="website" value="<?php echo $arr_Result['website']; ?>" size="100"></td>
                    </tr>
                    <tr>
                        <td class="caption">Flyer:</td>
                        <td>
                            <select id="flyer" name="flyer">
                                <option value="0" <?php echo ($arr_Result['flyer'] == '0') ? 'selected' : '';?>></option>
                                <option value="1" <?php echo ($arr_Result['flyer'] == '1') ? 'selected' : '';?>>Klassische Archäologie</option>
                                <option value="2" <?php echo ($arr_Result['flyer'] == '2') ? 'selected' : '';?>>Christliche Archäologie</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption">Aktiv:</td>
                        <td>
                            <input type="checkbox" name="activated" id="activated" <?php echo ($arr_Result['activated'] == 1) ? 'checked' : ''; ?>> Ja
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="button">
                            <input type="submit" id="update" name="update" value="Speichern">
                            <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>veranstalter/index'">
                        </td>
                    </tr>
                </table>                       
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>