<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'E-Mail/Benutzername';
} else if ($login_by_username) {
	$login_label = 'Benutzername';
} else {
	$login_label = 'E-Mail';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>
<?php echo form_open($this->uri->uri_string()); ?>
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
                <?php echo validation_errors(); ?>
            <?php endif; ?>
            <form action="<?php echo base_url();?>auth/login" method="post">
                <table>
                    <tr>
                        <td class="caption">E-Mail o. Login:</td>
                        <td><input type="text" id="login" name="login" value="" size="25"></td>
                    </tr>
                    <tr>
                        <td class="caption">
                            Passwort:
                        </td>
                        <td>
                            <input type="password" id="password" name="password" value="" size="25">
                        </td>
                    <tr>
                        <td class="button" colspan="2">
                            <input type="submit" id="save" name="save" value="Anmelden">
                        </td>
                    </tr>
                </table>            
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>



