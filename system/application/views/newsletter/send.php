<script type="text/javascript">
    function load() {
        $('#form').before('<div id="notice" class="notice">Bitte warten, der Newsletter wird verschickt...</dv>');
    }
</script>
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>            
            <?php endif; ?>
            <form action="sendnewsletter" method="post" id="form" name="form">
                <p>
                    Soll ein Newsletter mit den aktuellen Vorträgen an alle Benutzer geschickt werden?
                </p>
                <p>
                    <input type="submit" name="save" id="save" value="Verschicken" onclick="load();">
                    <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>newsletter/all'">
                </p>
            </form>
        </div>
    </div>
    <div class="clearer"> </div>
</div>
