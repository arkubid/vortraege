
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
            <?php endif; ?>
            <table class="data-table">
                <thead>                                        
                    <th>
                        <a href="<?php echo base_url();?>newsletter/all/mail/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'mail') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            E-Mail
                        </a>
                    </th>
                    <th class="small">
                        <a href="<?php echo base_url();?>newsletter/all/activ/<?php echo ($str_SortTyp == 'ASC') ? 'DESC' : 'ASC'; ?>">
                            <?php if ($str_SortBy == 'activ') : ?>
                                <?php echo ($str_SortTyp == 'ASC') ? ' &dArr;' : ' &uArr;'; ?>
                            <?php endif; ?>
                            Aktiv
                        </a>
                    </th>
                    <th class="small">
                        L&ouml;schen
                    </th>                    
                </thead>
                <?php $b_Color = false; ?>
                <?php foreach($arr_Result as $arr_Element) : ?>                    
                    <tr <?php echo ($b_Color == true) ? 'class="even"' : ''?>>
                        
                        <td>
                            <?php echo $arr_Element['mail']; ?>
                        </td>
                        <td>
                            <?php if ($arr_Element['activ'] == '1') : ?>
                                <a href="<?php echo base_url() . 'newsletter/deactivate/' . $arr_Element['newsletterID']; ?>" class="activ"></a>
                            <?php else : ?>
                                <a href="<?php echo base_url() . 'newsletter/activate/' . $arr_Element['newsletterID']; ?>" class="inactiv"></a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo base_url() . 'newsletter/delete/' . $arr_Element['newsletterID']; ?>" class="delete"></a>&nbsp;
                        </td>
                        
                    </tr>
                    <?php $b_Color = ($b_Color == true) ? false : true; ?>
                <?php endforeach; ?>
            </table>                           
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <div class="clearer"> </div>
</div>
