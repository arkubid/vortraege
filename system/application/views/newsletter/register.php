<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
                <?php echo validation_errors(); ?>
            <?php endif; ?>
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
            <?php endif; ?>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>                
                Wenn Sie in Zukunft per E-Mail &uuml;ber kommende Vortr&auml;ge informiert werden m&ouml;chten, tragen sie bitte hier ihre E-Mail Adresse ein
                und geben sie in das Feld Wort das Wort aus dem darunterstehenden Bild ein:
                <form action="<?php echo base_url();?>newsletter/subscribe" method="post">
                    <table>
                        <tr>
                            <td class="caption">
                                <label for="email" class="register">E-Mail:</label>
                            </td>
                            <td>
                                <input type="text" name="email" id="email" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <label for="captcha" class="register">Wort:</label>
                            </td>
                            <td>
                                <input type="text" name="captcha" id="captcha" value="" size="50" maxlength="7">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                <label  class="register">Bild:</label>
                            </td>
                            <td>
                                <?php echo $str_Image; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="button" colspan="2">
                                <input type="submit" name="save" id="save" value="Anmelden">
                                <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/all'">
                            </td>
                        </tr>
                    </table>                    
                </form>
            
        </div>
    </div>
    <div class="clearer"> </div>
</div>
