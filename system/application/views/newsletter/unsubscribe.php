<div class="main">
    <div class="post">
        <div class="post-body">
            Der erste Schritt der Abmeldung war erfolgreich. Bitte überprüfen sie ihre E-Mails und folgenden Sie den Anweisungen in der E-Mail
            zum Abschließen der Abmeldung.
        </div>
    </div>
    <div class="clearer"> </div>
</div>
