<script type="text/javascript">
    function load() {
        $('#form').before('<div id="notice" class="notice">Bitte warten, der Newsletter wird verschickt...</dv>');
    }
</script>
<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
                <?php echo validation_errors(); ?>
            <?php endif; ?>
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
            <?php endif; ?>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>                                
                <form action="<?php echo base_url();?>newsletter/singlemail" method="post" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <td class="caption">
                                <label for="email" class="register">Betreff:</label>
                            </td>
                            <td>
                                <input type="text" name="topic" id="topic" value="" size="75">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">
                                Inhalt:
                            </td>
                            <td>
                                <textarea name="content" id="content" rows="15" cols="100"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">Anhang:</td>
                            <td>
                                <input type="file" name="attachment" id="attachment">
                            </td>
                        </tr>                        
                        <tr>
                            <td colspan="2" class="button">
                                <input type="submit" name="save" id="save" value="Verschicken">
                                <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/all'">
                            </td>
                        </tr>

                    </table>
                </form>            
        </div>
    </div>
    <div class="clearer"> </div>
</div>
