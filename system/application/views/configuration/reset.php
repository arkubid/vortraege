<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
                <?php echo validation_errors(); ?>
            <?php endif; ?>
            <?php if ($notice != '') : ?>
                <div id="notice" class="notice"><?php echo $notice; ?></div>
            <?php endif; ?>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>
                <form action="<?php echo base_url();?>configuration/setNewPassword" method="post">
                    <table>
                        <tr>
                            <td class="caption">Neues Passwort:</td>
                            <td>
                                <input type="password" name="password" id="password" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">Neues Passwort wiederholen:</td>
                            <td>
                                <input type="password" name="confirm_password" id="confirm_password" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="button">
                                <input type="hidden" id="userId" name="userId" value="<?php echo $userId; ?>">
                                <input type="hidden" id="token" name="token" value="<?php echo $token; ?>">
                                <input type="submit" name="save" id="save" value="Speichern">
                                <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/all'">
                            </td>
                        </tr>
                    </table>
                </form>
            
        </div>
    </div>
    <div class="clearer"> </div>
</div>
