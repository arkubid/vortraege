<div class="main">
    <div class="post">
        <div class="post-body">
            <?php if (validation_errors() !== '') : ?>
                <?php echo validation_errors(); ?>
            <?php endif; ?>
            <?php if ($str_Notice != '') : ?>
                <div id="notice" class="notice"><?php echo $str_Notice; ?></div>
            <?php endif; ?>
                <script type="text/javascript">
                    $('#notice').delay(6000).fadeOut();
                </script>                                
                <form action="<?php echo base_url();?>configuration/save" method="post">
                    <table>
                        <tr>
                            <td class="caption">E-Mail:</td>
                            <td>
                                <input type="text" name="email" id="email" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">Login:</td>
                            <td>
                                <input type="text" name="username" id="username" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">Passwort:</td>
                            <td>
                                <input type="password" name="password" id="password" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td class="caption">Wiederholen:</td>
                            <td>
                                <input type="password" name="confirm_password" id="confirm_password" value="" size="50">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="button">
                                <input type="submit" name="save" id="save" value="Erstellen">
                                <input type="button" id="chancel" name="chancel" value="Abbrechen" onclick="location.href='<?php echo base_url();?>vortraege/all'">
                            </td>
                        </tr>
                    </table>
                </form>
            
        </div>
    </div>
    <div class="clearer"> </div>
</div>
