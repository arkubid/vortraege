<div class="main">
    <div class="post">
        <div class="post-body">
            <h4>Thematik</h4>
            Diese Seite versucht alle wissenschaftlichen Vorträge zu den folgenden Themengebieten in der Region Köln/Bonn zu erfassen:<br>
            <br>
            <li>
                Klassische Archäologie
            </li>
            <li>
                Vor- und Frühgeschichtliche Archäologie bzw. Ur- und Frühgeschichte
            </li>
            <li>
                Christliche Archäologie
            </li>
            <li>
                Ägyptologie
            </li>
            <li>
                Provinzialrömische Archäologie
            </li>
            <li>
                Alte Geschichte
            </li>

            <h4>Veranstalter</h4>
            Die Vorträge werden von folgenden Institutionen durchgeführt:<br><br>
            <?php foreach($arr_Veranstalter as $arr_Element) : ?>
                <?php if ($arr_Element['activated'] == 1) : ?>
                    <li>
                        <acronym title="<?php echo $arr_Element['website']; ?>">
                            <?php if ($arr_Element['website'] !== '') : ?>
                                <a href="http://<?php echo $arr_Element['website']; ?>" target="_blank"><?php echo $arr_Element['veranstalterName']; ?></a>
                            <?php else : ?>
                                <?php echo $arr_Element['veranstalterName']; ?>
                            <?php endif; ?>
                        </acronym>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>

            <h4>Veranstaltungsorte</h4>
            Die Vorträge finden abwechselnd an folgenden Orten statt, wobei, wenn keine anderweitigen Angaben gemacht sind, der Hausherr der
            Veranstalter des jeweiligen Vortrages ist:<br><br>
            <?php foreach($arr_Ort as $arr_Element) : ?>
                <?php if ($arr_Element['activated'] == 1) : ?>
                    <li>
                        <acronym title="<?php echo $arr_Element['adresse']; ?>">
                            <?php if ($arr_Element['website'] !== '') : ?>
                                <a href="http://<?php echo $arr_Element['website']; ?>" target="_blank"><?php echo $arr_Element['adresse']; ?></a>
                            <?php else : ?>
                                <?php echo $arr_Element['adresse']; ?>
                            <?php endif; ?>
                        </acronym>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>

            <br>
            Es wird keine Garantie auf Vollständigkeit und Korrektheit der hier aufgeführten Daten gegeben. Der Besuch der Vorträge ist zumeist kostenlos.
        </div>
    </div>
    <div class="clearer"> </div>
</div>           