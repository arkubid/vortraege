
<div class="main">
    <div class="post">
        <div class="post-body">
            <h4>Kontakt</h4>
            Rheinische Friedrich-Wilhelms-Universität Bonn<br>
            Institut für Kunstgeschichte und Archäologie<br>
            Abteilung Klassische Archäologie mit Akademischem Kunstmuseum
            <br><br>
            Am Hofgarten 21<br>
            53113 Bonn<br>
            <br>
            Telefon: +49 (0)228 73-5011<br>
            Telefax: +49 (0)228 73-7282<br>
            E-Mail: archinst(at)uni-bonn.de<br>
            
            <h4>Internet</h4>
            <a href="http://www.ai.uni-bonn.de" target="_blank">http://www.ai.uni-bonn.de</a><br>
            <a href="http://www.ai-vortraege.uni-bonn.de" target="_blank">http://www.ai-vortraege.uni-bonn.de</a><br>
            
            <h4>Leitung</h4>
            Das Institut für Kunstgeschichte und Archäologie ist ein Institut der Rheinischen Friedrich-Wilhelms-Universität Bonn. Die Abteilung Klassische Archäologie ist eine Abteilung des Institut für Kunstgeschichte und Archäologie. Die Abteilung wird durch ihren geschäftsführenden Leiter Prof. Dr. F. Rumscheid vertreten.
            
            <h4>Zuständige Aufsichtsbehörde</h4>
            Ministerium für Wissenschaft und Forschung des Landes Nordrhein-Westfalen, Völklinger Straße 49, 40221 Düsseldorf<br>
            Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE 122119125
            
            <h4>Haftungshinweis</h4>
            Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.
            <h4>Technische Umsetzung</h4>
            Benjamin Geißler M. A.
        </div>
    </div>
    <div class="clearer"> </div>
</div>           