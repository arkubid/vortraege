
<div class="main">
    <div class="post">
        <div class="post-body">
            Alle Vorträge werden in einen freizugänglichen Google Kalender eingetragen und können dort eingesehen werden.<br>
            <br>
            Um den Kalender in einem bestehenden Google Kalender anzuzeigen bitte wie folgt vorgehen:<br>
            <ul>
                <li>
                    Im Kalender rechts auf das Dreieck links neben "Weitere Kalender" klicken
                </li>
                <li>
                    Auf "Über URL hinzufügen" klicken
                </li>
                <li>
                    In dem neuen Fenster folgenden Link eintragen und bestätigen:<br>
                    https://www.google.com/calendar/ical/1fju19jk735caqaravs83a9iek%40group.calendar.google.com/public/basic.ics
                </li>
            </ul>            
            Für weitere Informationen zu Google Kalender folgen sie bitte folgendem Link:<br>
            <a href="http://www.google.com/googlecalendar/overview.html" target="_blank">http://www.google.com/googlecalendar/overview.html</a>
        </div>
    </div>
    <div class="clearer"> </div>
</div>           