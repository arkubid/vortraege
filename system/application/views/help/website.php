
<div class="main">
    <div class="post">
        <div class="post-body">
            <h4>Einführung</h4>
            Wollen Sie den RSS Feed auf ihrer eigenen Homepage einbinden, haben jedoch kein Modul o. ä. hierfür, können sie
            folgenden JavaScript-Code verwenden, um die Vorträge anzuzeigen. Hierbei wird die Feed API von Google sowie
            jQuery zum Anzeigen der Vorträge verwendet. Mehr Informationen hierzu finden sie hier:<br>
            <a href="http://code.google.com/intl/de-DE/apis/ajaxfeeds/" target="_blank">http://code.google.com/intl/de-DE/apis/ajaxfeeds/</a><br>
            <a href="http://jquery.com/" target="_blank">http://jquery.com/</a><br>
            
            <h4>JavaScript Code</h4>
            <p class="code">                
                &lt;script type="text/javascript" src="http://www.google.com/jsapi"&gt;&lt;/script&gt;
                &lt;script type="text/javascript"&gt;
                    <font color="grey">/**
                     *  Die Google JavaScript API einbinden und jQuery 1.4.2 und die Feed API 1.0 einbinden
                     */</font>
                    google.load("jquery", "1.4.2");
                    google.load("feeds", "1");                                      
                    <font color="grey">
                    /**
                     *  HTML Zeichen in ihr anzuzeigendes Equivalent umwandeln
                     *
                     *  @param {string} str_Target
                     *  @return {string}
                     */</font>
                    function htmlentities(str_Target) {
			var arr_From    =   new Array('&amp;lt;', '&amp;gt;', '&amp;#45;');
		        var arr_To      =   new Array('<', '>', '-');

		        for(var i in arr_From) eval("str_Target=str_Target.replace(/"+ arr_From[i] +"/g,\""+arr_To[i]+"\");");

		        return str_Target;
                    }                    
                    <font color="grey">
                    /**
                     *  Lädt den gewünschten Feed und zeigt die Eintrag aus diesem innerhalb des angegeben Elementes (ID) an
                     */</font>
                    function initialize() {
                        // Parameter (hier ggf. Änderungen vornehmen)
                        var str_Feed    =   "http://www.ai-vortraege.uni-bonn.de/vortraege/rss";
                        var i_Anzahl    =   25;
                        var str_Element =   'feed';

                        // Den gewünschten Feed einbinden
			var obj_Feed    =   new google.feeds.Feed(str_Feed);

                        // Maximale Anzahl an Einträgen festlegen
			obj_Feed.setNumEntries(i_Anzahl);
			obj_Feed.load(function(arr_Result) {
                            for (var i = 0; i < arr_Result.feed.entries.length; i++) {
                                var arr_Entry   =   arr_Result.feed.entries[i];
                                var str_Title   =   arr_Entry['title'];
                                var str_Content =   htmlentities(arr_Entry['content']);
                                
                                // Den jeweiligen Eintrag in das benannten div-Element einfügen
                                $('#' + str_Element).append('&lt;b&gt;' + str_Title + '&lt;/b&gt;&lt;br&gt;' +  str_Content + '&lt;br&gt;');
                            }
			});
                    }

                    // Start Funktion aufrufen
                    google.setOnLoadCallback(initialize);
                &lt;/script&gt;
            </p>
            <h4>HTML Code</h4>
            Der HTML Code muss sich an der Stelle auf ihrer Website befinden, an der sie die Vorträge anzeigen möchten.
            <p class="code">
                &lt;div id="feed"&gt;&lt;/div&gt;
            </p>
            <h4>Beispiel</h4>
            Ein Beispiel, wie die Ausgabe der Vorträge aussieht finden sie hier:<br>
            <a href="http://www.varinst.de/de/node/30" target="_blank">http://www.varinst.de/de/node/30</a>
        </div>
    </div>
    <div class="clearer"> </div>
</div>           