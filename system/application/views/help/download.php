
<div class="main">
    <div class="post">
        <div class="post-body">
            Über den Menüpunkt <b>Download</b> kann eine Liste der aktuellen Vorträge in Form einer PDF Datei angesehen bzw.
            heruntergeladen werden.<br>
            Über den Menüpunkt <b>inkl. Kolloquien</b> kann eine Liste der aktuellen Vorträge und Kolloquien in Form einer PDF Datei angesehen bzw.
            heruntergeladen werden.<br>
            Zum Betrachten der PDF Datei wird der Acrobat Reader benötigt. Dieser kann auf folgender Website heruntergeladen werden:<br>
            <a href="http://get.adobe.com/de/reader/" target="_blank">http://get.adobe.com/de/reader/</a>
        </div>
    </div>
    <div class="clearer"> </div>
</div>           