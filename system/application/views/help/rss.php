<div class="main">
    <div class="post">
        <div class="post-body">
            <h4>Erklärung</h4>
            Der RSS Feed ermöglicht das Einbinden der aktuellen Vorträge auf jeder beliebigen anderen Internetseite oder das Lesen mit Hilfe eines Feed Readers.
            Durch das anhängen von bestimmten Parametern kann die Ausgabe der Feed Einträge auf bestimmte Einträge beschränkt werden. Standardmäßig werden alle
            Vorträge angezeigt.<br>
            Die Parameter müssen dabei an die eigentliche Adresse (<?php echo base_url(), 'vortraege/rss'; ?>) angehängt werden. Mehrere Parameter müssen mit einem
            Slash (<b>"/"</b>) von einander getrennt werden. Soll ein Parameter nicht berücksichtigt werden, dafür jedoch ein weiter hinten anzugebender Paramter,
            so ist für den nicht zu berücksichtigen Parameter <b>alle</b> anzugeben. Die Parameter <b>müssen</b> in der vorgegeben Reihenfolge angeben werden.
            <h4>Parameter</h4>
                Der erste Parameter selektiert die Art des Vortrages, z. B. klassisch, prähistorisch etc. Es sind folgende Eingaben möglich<br>
                <li>
                    Alle
                </li>
                <?php foreach($arr_Type as $arr_Element) : ?>
                <li>
                    <?php echo $arr_Element['name']; ?>
                </li>
                <?php endforeach; ?>
                <br>
                Der zweite Parameter selektiert die Stadt in der der Vortrag stattfindet. Es sind folgende Eingaben möglich<br>
                <li>
                    Alle
                </li>
                <?php foreach($arr_Stadt as $arr_Element) : ?>
                <li>
                    <?php echo $arr_Element['name']; ?>
                </li>
                <?php endforeach; ?>
                <br>
                Der dritte Parameter selektiert den genauen Veranstaltungsort des Vortrag. Es sind folgende Eingaben möglich<br>
                <li>
                    Alle
                </li>
                <?php foreach($arr_Ort as $arr_Element) : ?>
                    <?php if ($arr_Element['activated'] == 1) : ?>
                        <li>
                            <?php echo $arr_Element['ort']; ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
                <br>
                Der vierte Parameter selektiert den Veranstalter des Vortrag. Es sind folgende Eingaben möglich<br>
                <li>
                    Alle
                </li>
                <?php foreach($arr_Veranstalter as $arr_Element) : ?>
                    <?php if ($arr_Element['activated'] == 1) : ?>
                        <li>
                            <?php echo $arr_Element['veranstalterName']; ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
                <br>
                Der fünfte Parameter gibt an, ob Vorträge im Rahmen von Kolloquien angezeigt werden sollen. Es sind folgende Eingaben möchlich:<br>
                <table>
                    <tr>
                        <th>-1:</th>
                        <td>Es werden sowohl Vorträge als auch Kolloquien angezeigt.
                    </tr>
                    <tr>
                        <th>0:</th>
                        <td>Es werden <b>nur</b> Vorträge angezeigt. Standard Auswahl!</td>
                    </tr>
                    <tr>
                        <th>1:</th>
                        <td>Es werden <b>nur</b> Kolloquien angezeigt.</td>
                    </tr>
                </table>

                <h4>Beispiele</h4>
                Folgender Eintrag selektiert alle klassisch-archäologischen Vorträge in Bonn:<br>
                <a href="<?php echo base_url();?>vortraege/rss/klassisch/bonn" target="_blank">
                    <?php echo base_url();?>vortraege/rss/klassisch/bonn
                </a>
                <br><br>
                Folgender Eintrag selektiert alle Vorträge im Römisch-Germanischen Museum in Köln:<br>
                <a href="<?php echo base_url();?>vortraege/rss/alle/alle/RGM Köln" target="_blank">
                    <?php echo base_url();?>vortraege/rss/alle/alle/RGM Köln
                </a>
                <br><br>
                Folgender Eintrag selektiert alle Vorträge des Vereins von Altertumsfreunden im Rheinlande (AV-Vorträge):<br>
                <a href="<?php echo base_url();?>vortraege/rss/alle/alle/alle/Verein von Altertumsfreunden im Rheinlande" target="_blank">
                    <?php echo base_url();?>vortraege/rss/alle/alle/alle/Verein von Altertumsfreunden im Rheinlande
                </a>
                <br><br>
        </div>
    </div>
    <div class="clearer"> </div>
</div>           