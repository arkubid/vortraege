<?php
    /**
     *  
     *
     *  @author     Benjamin Geißler <benjamin.geissler@gmail.com>
     *  @version    1.0
     *  @package
     *  @subpackage Model
     *  @since      1.0
     *
     *  @property CI_DB_active_record $db
     */
    class CaptchaModel extends Model {
        public function getAll($i_Start = 0,  $i_MaxResults = 20, $str_SortBy = 'ort', $str_SortTyp = 'ASC') {
            $this->db->select('*');
            $this->db->from('ort');
            $this->db->order_by($str_SortBy, $str_SortTyp);
            if ($i_Start > -1) {
                $this->db->limit($i_MaxResults, $i_Start);
            }
            $arr_Result =   $this->db->get();

            return $arr_Result->result_array();
        }

        public function getOne($str_Word, $str_IP, $str_Time) {
            $arr_Result =   $this->db->get_where('captcha', array('id' => $i_ID));
            $obj_Result =   $arr_Result->row_array();

            return $obj_Result;
        }

        public function getCaptcha($str_Word, $str_IP, $str_Duration) {
            $b_Return   =   false;

            $this->db->select('captchaID');
            $this->db->where('word', $str_Word);
            $this->db->where('ipadresse', $str_IP);
            $this->db->where('zeit >', time() - $str_Duration);
            $arr_Result =   $this->db->get('captcha');
            
            if ($arr_Result->num_rows() == 1) {
                $arr_Result     =   $arr_Result->row_array();
                $this->db->delete('captcha', array('captchaID' => $arr_Result['captchaID']));
                $b_Return   =   true;
            }

            return $b_Return;
        }

        public function getNumberOfRecords() {
            return $this->db->count_all('ort');
        }

        public function create($arr_Data) {
            return $this->db->insert('captcha', $arr_Data);
        }

        public function deleteOld($str_Duration = '7200') {            
            $this->db->where('zeit <', time() - $str_Duration);
            return $this->db->delete('captcha');
        }
    }
?>
