<?php
require_once 'configurationmodel.php';

/**
 *  Model für die Verwaltung der Veranstaltungsortsangaben
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.0
 * @package    Model
 * @subpackage Model
 * @since      1.0
 *
 * @property CI_DB_active_record $db
 */
class OrtModel extends ConfigurationModel
{
    /**
     *  Konstruktor, der die Tabelle des Models fest legt
     */
    public function OrtModel()
    {
        parent::ConfigurationModel();
        $this->str_Table = 'ort';
    }

    /**
     *  Lädt alle Ortsangabe in der angegebene Sortierung und Anzahl
     *
     * @param  integer $i_Start Start Zahl für die Paginierung
     * @param  integer $i_MaxResults Anzahl der Maximalen Ergebnisse für die Paginierung
     * @param  string $str_SortBy Feld nach dem sortiert werden soll
     * @param  string $str_SortTyp Art der Sortierung
     * @return array
     */
    public function getAll($i_Start = -1, $i_MaxResults = 20, $str_SortBy = 'ort', $str_SortTyp = 'ASC')
    {
        return parent::getAll($i_Start, $i_MaxResults, $str_SortBy, $str_SortTyp);
    }

    /**
     *  Ermittelt die ID anhand des übergebene Ortes
     *
     * @param string $str_Ort
     *
     * @return integer
     */
    public function getIDByOrt($str_Ort)
    {
        $arr_Result = $this->db->get_where($this->str_Table, array('ort' => $str_Ort));
        if ($arr_Result->num_rows() == 1) {
            $obj_Result = $arr_Result->row();
            return $obj_Result->id;
        }

        return -1;
    }

    /**
     *  Legt einen neuen Veranstaltungsort an
     *
     * @param  array $arr_Data
     * @return boolean
     */
    public function create($arr_Data)
    {
        if ($this->db->insert($this->str_Table, $arr_Data) == true) {
            return true;
        }

        return false;
    }

    /**
     *  Orts Einträge können nicht gelöscht werden
     *
     * @param  integer $i_ID
     * @return boolean
     */
    public function  delete($i_ID)
    {
        return false;
    }
}
