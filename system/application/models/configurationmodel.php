<?php
/**
 *  Verwaltet den Datenbankzugriff auf die Benutzer, bildet zugleich Grundmodel das abgeleitet wird
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.0
 * @package    Configuration
 * @subpackage Model
 * @since      1.0
 *
 * @property   CI_DB_active_record $db
 * @property   string              $str_Table      Name der MySQL Datenbank
 * @property   string              $str_IDField    Name des ID Fields
 */
class ConfigurationModel extends Model
{
    protected $str_Table;
    protected $str_IDField;

    /**
     *  Konstruktor, der die Tabelle des Models fest legt
     */
    public function ConfigurationModel()
    {
        $this->str_Table   = 'users';
        $this->str_IDField = 'id';
    }

    /**
     *  Lädt alle Benutzerangaben in der angegebene Sortierung und Anzahl
     *
     * @param  integer $i_Start Start Zahl für die Paginierung
     * @param  integer $i_MaxResults Anzahl der Maximalen Ergebnisse für die Paginierung
     * @param  string $str_SortBy Feld nach dem sortiert werden soll
     * @param  string $str_SortTyp Art der Sortierung
     * @return array
     */
    public function getAll($i_Start = 0, $i_MaxResults = 20, $str_SortBy = 'username', $str_SortTyp = 'ASC')
    {
        $this->db->select('*');
        $this->db->from($this->str_Table);
        $this->db->order_by($str_SortBy, $str_SortTyp);

        if ($i_Start > -1) {
            $this->db->limit($i_MaxResults, $i_Start);
        }
        $arr_Result = $this->db->get();

        return $arr_Result->result_array();
    }

    /**
     *  Zugriff auf einen per ID bestimmten Datensatz
     *
     * @param  integer $i_ID
     * @return object
     */
    public function getOne($i_ID)
    {
        $arr_Result = $this->db->get_where($this->str_Table, array($this->str_IDField => $i_ID));
        $obj_Result = $arr_Result->row_array();

        return $obj_Result;
    }

    /**
     *  Gibt die Anzahl der vorhanden Datensätze zurück
     *
     * @return integer
     */
    public function getNumberOfRecords()
    {
        return $this->db->count_all($this->str_Table);
    }

    /**
     *  Aktuallisiert anhand der ID die angebene Felder in der Tabelle des Models
     *
     * @param  integer $i_ID ID des Datensatzes
     * @param  array $arr_Data Felder und Wert als assoziativer Array
     * @return boolean
     */
    public function update($i_ID, $arr_Data)
    {
        $this->db->where($this->str_IDField, $i_ID);
        return $this->db->update($this->str_Table, $arr_Data);
    }

    /**
     *  Löscht den Eintrag anhand der übergebene ID
     *
     * @param  integer $i_ID
     * @return boolean
     */
    public function delete($i_ID)
    {
        return $this->db->delete($this->str_Table, array($this->str_IDField => $i_ID));
    }
}

