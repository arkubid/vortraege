<?php
    require_once 'configurationmodel.php';

    /**
     *  Model für die Verwaltung der Feed Konfigurationen. Die Arrays im Feld "configuration" werden automatisch
     *  serialisiert bzw. de-serialisiert.
     *
     *  @author     Benjamin Geißler <benjamin.geissler@gmail.com>
     *  @version    1.0
     *  @package    Model
     *  @subpackage Model
     *  @since      1.0
     *
     *  @property CI_DB_active_record $db
     */
    class FeedModel extends ConfigurationModel {
        /**
         *  Konstruktor, der die Tabelle des Models fest legt
         */
        public function FeedModel() {
            parent::ConfigurationModel();
            $this->str_Table    =   'feed';
        }

        /**
         *  Lädt alle Feeds in der angegebene Sortierung und Anzahl
         *
         *  @param  integer $i_Start        Start Zahl für die Paginierung
         *  @param  integer $i_MaxResults   Anzahl der Maximalen Ergebnisse für die Paginierung
         *  @param  string  $str_SortBy     Feld nach dem sortiert werden soll
         *  @param  string  $str_SortTyp    Art der Sortierung
         *  @return array
         */
        public function getAll($i_Start = -1,  $i_MaxResults = 20, $str_SortBy = 'link', $str_SortTyp = 'ASC')
        {
            return parent::getAll($i_Start, $i_MaxResults, $str_SortBy, $str_SortTyp);
        }

        /**
         *  Findet eine Feed Konfiguration anhand der ID
         * 
         *  @param  integer $i_ID   ID
         *  @return array|boolean
         */
        public function find($i_ID)
        {
            return $this->findBy(array($this->str_IDField => $i_ID));            
        }
        
        /**
         *  Findet eine Feed Konfiguration anhand des Link Names
         *  
         *  @param  string  $str_Link   Link 
         *  @return array|boolean
         */
        public function findByLink($str_Link) 
        {
            return $this->findBy(array('link' => $str_Link));
        }

        /**
         *  Erstellt einen neuen Eintrag, sofern der Link Name nicht bereit verwendet wird.
         * 
         *  @param  array    $arr_Data  Daten Array (link und configuration als Keys)
         *  @return boolean
         */
        public function create($arr_Data)
        {
            if ($this->findByLink($arr_Data['link']) == false) {
                $arr_Data['configuration']  = serialize($arr_Data['configuration']);
                return  $this->db->insert($this->str_Table, $arr_Data);                
            }
            
            return false;
        }
        
        /**
         *  Aktuallisiert eine Feed Einstellung, sofern der Link Name nicht bereits verwendet wird
         * 
         *  @param  integer $i_ID       ID
         *  @param  array   $arr_Data   Daten Array
         *  @return boolean
         */
        public function update($i_ID, $arr_Data)
        {
            if ($this->findBy(array('link' => $arr_Data['link'], 'id !=' => $i_ID)) == false) {
                $arr_Data['configuration']  = serialize($arr_Data['configuration']);
                $this->db->where($this->str_IDField, $i_ID);
                
                return $this->db->update($this->str_Table, $arr_Data);                
            }
            
            return false;
        }
        
        /**
         *  Löscht die Feed Einstellungen
         * 
         *  @param  integer $i_ID   ID
         *  @return boolean
         */
        public function delete($i_ID)
        {
            return $this->db->delete($this->str_Table, array($this->str_IDField => $i_ID));    
        }

        /**
         *  Ermittelt die Feed Einstellungen anhand des übergebene Arrays und deserialisiert bei gefundenen Einträgen
         *  das configuration Feld.
         * 
         *  @param  array   $arr_Find   MySQL-Feld-name => Parameter
         *  @return array|boolean
         */
        private function findBy($arr_Find)
        {
            $arr_Result =   $this->db->get_where($this->str_Table, $arr_Find);
            
            if ($arr_Result->num_rows() == 1) {
                $arr_Return                     =   $arr_Result->row_array();
                $arr_Return['configuration']    =   unserialize($arr_Return['configuration']);
                
                return $arr_Return;
            }
            
            return false;
        }
    }
?>
