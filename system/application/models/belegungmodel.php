<?php
    /**
     *  
     *
     *  @author     Benjamin Geißler <benjamin.geissler@gmail.com>
     *  @version    1.0
     *  @package
     *  @subpackage Model
     *  @since      1.0
     *
     *  @property CI_DB_active_record $db
     */
    class BelegungModel extends Model {
        public function getAll($i_Start = 0,  $i_MaxResults = 20, $str_SortBy = 'ort', $str_SortTyp = 'ASC') {
            $this->db->select('*');
            $this->db->from('belegung');
            $this->db->order_by($str_SortBy, $str_SortTyp);
            if ($i_Start > -1) {
                $this->db->limit($i_MaxResults, $i_Start);
            }
            $arr_Result =   $this->db->get();

            return $arr_Result->result_array();
        }

        public function getOne($i_ID) {
            $arr_Result =   $this->db->get_where('belegung', array('belegungID' => $i_ID));
            $obj_Result =   $arr_Result->row_array();

            return $obj_Result;
        }

        public function getNumberOfRecords() {
            return $this->db->count_all('ort');
        }

        public function create($arr_Data) {
            if ($this->db->insert('belegung', $arr_Data) == true) {
                return $this->db->insert_id();
            }
            else {
                return false;
            }
        }
        
        public function update($i_ID, $arr_Data) {
            $this->db->where('belegungID', $i_ID);
            $this->db->update('belegung', $arr_Data);
        }

        public function delete($i_ID) {
            return $this->db->delete('belegung', array('belegungID' => $i_ID));
        }

        /**
         *  Prüft, ob an dem angegebenen Datum zu der ausgewählten Uhrzeit der gewünschte Raum frei ist oder nicht
         *
         *  @param integer  $i_BelegungsID
         *  @param string   $str_Datum
         *  @param string   $str_Beginn
         *  @param string   $str_Ende
         *  @param string   $str_Location
         *
         *  @return boolean
         */
        public function isFreeDate($str_Datum, $str_Beginn, $str_Ende, $str_Location = 'hoersaal', $i_BelegungsID = '') {
            $b_Return   =   false;

            $str_SQL    =   'SELECT belegungID
                                FROM belegung
                                WHERE
                                    datum = \'' . $str_Datum . '\'
                                    AND ' . $str_Location . ' = 1
                                    AND belegungID != \'' . $i_BelegungsID . '\'
                                    AND (
                                         (TIME(\'' . $str_Beginn . '\') BETWEEN beginn AND ende)
                                          OR (TIME(\'' . $str_Ende . '\') BETWEEN beginn AND ende)
                                          OR (beginn BETWEEN TIME(\'' . $str_Beginn . '\') AND TIME(\'' . $str_Ende . '\'))
                                          OR (ende BETWEEN TIME(\'' . $str_Beginn . '\') AND TIME(\'' . $str_Ende . '\')))';
                                            
            $obj_Query  =   $this->db->query($str_SQL);
            if ($obj_Query == true
                && $obj_Query->num_rows() == 0) {                    
                    $b_Return   =   true;
            }

            return $b_Return;
        }
    }
?>
