<?php
    /**
     *  
     *
     *  @author     Benjamin Geißler <benjamin.geissler@gmail.com>
     *  @version    1.0
     *  @package
     *  @subpackage Model
     *  @since      1.0
     *
     *  @property CI_DB_active_record $db
     */
    class StadtModel extends Model {
        public function getAll($i_Start = 0,  $i_MaxResults = 50, $str_SortBy = 'name', $str_SortTyp = 'ASC') {
            $this->db->select('*');
            $this->db->from('stadt');
            $this->db->order_by($str_SortBy, $str_SortTyp);
            if ($i_Start > -1) {
                $this->db->limit($i_MaxResults, $i_Start);
            }
            $arr_Result =   $this->db->get();

            return $arr_Result->result_array();
        }

        public function getOne($i_ID) {
            $arr_Result =   $this->db->get_where('stadt', array('id' => $i_ID));
            $obj_Result =   $arr_Result->row_array();

            return $obj_Result;
        }

        /**
         *  Ermittelt die ID anhand des übergebene Names
         *
         *  @param string $str_Name
         *
         *  @return integer
         */
        public function getIDByName($str_Name) {
            $i_Return   =   -1;

            $arr_Result =   $this->db->get_where('stadt', array('name' => $str_Name));
            if ($arr_Result->num_rows() == 1) {
                $obj_Result =   $arr_Result->row();
                $i_Return   =   $obj_Result->id;
            }

            return $i_Return;
        }

        public function getNumberOfRecords() {
            return $this->db->count_all('stadt');
        }
    }
?>
