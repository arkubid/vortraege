<?php
/**
 * Verwaltet die Art des Vortrages
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.1
 *
 * @property CI_DB_active_record $db
 */
class TypeModel extends Model
{
    /**
     * Gibt alle möglichen Arten der Vortragstypen zurück.
     *
     * @param int $start
     * @param int $maxResults
     * @param string $orderBy
     * @param string $orderType
     * @return mixed
     */
    public function getAll($start = 0, $maxResults = 50, $orderBy = 'name', $orderType = 'ASC')
    {
        $this->db->select('*');
        $this->db->from('type');
        $this->db->order_by($orderBy, $orderType);
        if ($start > -1) {
            $this->db->limit($maxResults, $start);
        }
        $arr_Result = $this->db->get();

        return $arr_Result->result_array();
    }

    /**
     * Ermittelt einen bestimmten Eintrag anhand der ID
     *
     * @param  integer $id
     * @return array
     */
    public function getOne($id)
    {
        $result = $this->db->get_where('type', array('id' => $id));

        return $result->row_array();
    }

    /**
     * Gibt den Titel zu dem Typus zurück.
     *
     * @param  integer $id
     * @return string
     */
    public function getTitel($id)
    {
        $result = $this->getOne($id);
        return $result['titel'];
    }

    /**
     * Gibt die Anzeige Art zurück.
     *
     * @param $id
     * @return string
     */
    public function getType($id)
    {
        $result = $this->getOne($id);
        return $result['type'];
    }

    /**
     * Ermittelt die ID anhand des übergebene Names.
     *
     * @param $name
     * @return int
     */
    public function getIDByName($name)
    {
        $result = $this->db->get_where('type', array('name' => $name));
        if ($result->num_rows() == 1) {
            $object = $result->row();
            return $object->id;
        }

        return -1;
    }

    /**
     * Gibt die Anzahl der vorhanden Möglichkeiten zurück
     *
     * @return integer
     */
    public function getNumberOfRecords()
    {
        return $this->db->count_all('type');
    }
}
