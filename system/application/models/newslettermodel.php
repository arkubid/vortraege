<?php
/**
 *
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.0
 * @package
 * @subpackage Model
 * @since      1.0
 *
 * @property CI_DB_active_record $db
 */
class NewsletterModel extends Model
{


    public function getList()
    {
        $this->db->select('*');
        $this->db->from('vortrag');
        $this->db->join('veranstalter', 'veranstalter.veranstalterID = vortrag.veranstalter');
        $this->db->join('ort', 'ort.id = vortrag.ort');
        $this->db->join('belegung', 'belegung.vortrag = vortrag.vortragID');
        $this->db->where('datum >=', 'NOW()', false);
        $this->db->where('activ', '1');
        $this->db->order_by('datum');
        $obj_Query = $this->db->get();

        return $obj_Query->result_array();
    }

    public function getAll($i_Start = 0, $i_MaxResults = 20, $str_SortBy = 'mail', $str_SortTyp = 'ASC')
    {
        $this->db->select('*');
        $this->db->from('newsletter');
        $this->db->order_by($str_SortBy, $str_SortTyp);

        if ($i_Start > -1) {
            $this->db->limit($i_MaxResults, $i_Start);
        }
        $arr_Result = $this->db->get();

        return $arr_Result->result_array();
    }

    public function getAllWhere($arr_Data)
    {
        $this->db->select('*');
        $this->db->from('newsletter');
        $this->db->where($arr_Data);
        $arr_Result = $this->db->get();

        return $arr_Result->result_array();
    }

    /**
     * Gibt alle E-Mail Adressen zurück, an die der Newsletter geschickt werden soll.
     *
     * @return array
     */
    public function getAllMails()
    {
        $this->db->select('mail');
        $this->db->from('newsletter');
        $this->db->where('activ', '1');
        $result = $this->db->get();
        $result = $result->result_array();

        $return = array();
        foreach ($result as $row) {
            $return[] = $row['mail'];
        }

        return $return;
    }

    public function getOne($str_Mail)
    {
        $this->db->where(array('mail' => $str_Mail));
        $arr_Result = $this->db->get('newsletter');

        return $arr_Result->row_array();
    }

    public function get($arr_Data)
    {
        $this->db->where($arr_Data);
        $arr_Result = $this->db->get('newsletter');

        return $arr_Result->row_array();
    }

    public function getNumberOfRecords()
    {
        return $this->db->count_all('newsletter');
    }

    public function create($arr_Data)
    {
        $b_Return   = false;
        $arr_Result = $this->getOne($arr_Data['mail']);

        if (count($arr_Result) == 0
            && $this->db->insert('newsletter', $arr_Data) == true
        ) {
            $b_Return = true;
        }

        return $b_Return;
    }

    public function update($i_ID, $arr_Data)
    {
        $this->db->where('newsletterID', $i_ID);
        return $this->db->update('newsletter', $arr_Data);
    }

    public function delete($i_ID)
    {
        return $this->db->delete('newsletter', array('newsletterID' => $i_ID));
    }
}
