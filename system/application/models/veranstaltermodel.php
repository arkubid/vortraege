<?php
require_once __DIR__ . '/ortmodel.php';

/**
 *  Verwaltung der Veranstalter
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.0
 * @package    Veranstalter
 * @subpackage Model
 * @since      1.0
 *
 * @property CI_DB_active_record $db
 */
class VeranstalterModel extends OrtModel
{
    /**
     *  Konstruktor, der den Namen der Tabelle festlegt
     */
    public function VeranstalterModel()
    {
        parent::OrtModel();

        $this->str_Table   = 'veranstalter';
        $this->str_IDField = 'veranstalterID';
    }

    /**
     *  Lädt alle Ortsangabe in der angegebene Sortierung und Anzahl
     *
     * @param  integer $i_Start Start Zahl für die Paginierung
     * @param  integer $i_MaxResults Anzahl der Maximalen Ergebnisse für die Paginierung
     * @param  string $str_SortBy Feld nach dem sortiert werden soll
     * @param  string $str_SortTyp Art der Sortierung
     * @return array
     */
    public function getAll($i_Start = -1, $i_MaxResults = 20, $str_SortBy = 'veranstalterName', $str_SortTyp = 'ASC')
    {
        return parent::getAll($i_Start, $i_MaxResults, $str_SortBy, $str_SortTyp);
    }

    /**
     *  Ermittelt die ID anhand des übergebene Names
     *
     * @param string $str_Name
     *
     * @return integer
     */
    public function getIDByName($str_Name)
    {
        $arr_Result = $this->db->get_where('veranstalter', array('veranstalterName' => $str_Name));
        if ($arr_Result->num_rows() == 1) {
            $obj_Result = $arr_Result->row();
            return $obj_Result->veranstalterID;
        }

        return -1;
    }
}
