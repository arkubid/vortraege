<?php
    /**
     *  Verwaltet den Zugriff auf die Vorträge
     *
     *  @author     Benjamin Geißler <benjamin.geissler@gmail.com>
     *  @version    1.0
     *  @package
     *  @subpackage Model
     *  @since      1.0
     *
     *  @property CI_DB_active_record $db
     */
    class VortraegeModel extends Model {

        /**
         *  Gibt eine Liste mit allen aktiven, in der Zukunft liegenden Vorträgen zurück
         *
         *  @param  array   $arr_Where  Zusätzliche Abfrage Kriterien (Feld => Wert)
         *  @return array
         */
        public function getList($arr_Where = array(), $arr_WhereIn = array()) {
            $this->db->select('*');
            $this->db->from('vortrag');
            $this->db->join('veranstalter', 'veranstalter.veranstalterID = vortrag.veranstalter');
            $this->db->join('ort', 'ort.id = vortrag.ort');
            $this->db->join('belegung', 'belegung.vortrag = vortrag.vortragID');
            $this->db->where('datum >=', date('Y-m-d'));
            $this->db->where('activ', '1');

            // ggf. Zusatz Einschränkung laden
            if (count($arr_Where) > 0) {
                foreach($arr_Where as $str_Field => $mix_Value) {
                    $this->db->where($str_Field, $mix_Value);
                }
            }
            
            if (count($arr_WhereIn) > 0) {
                foreach($arr_WhereIn as $field => $value) {
                    $this->db->where_in($field, $value);
                }
            }

            $this->db->order_by('datum');
            $obj_Query  =   $this->db->get();
            
            return $obj_Query->result_array();
        }

        /**
         *  Gibt alle Vorträge in der angegebenen Spanne zurück
         *
         *  @param  integer $i_Start        Start Zähler
         *  @param  integer $i_MaxResults   Maximale Anzahl an Treffern
         *  @param  string  $str_SortBy     Feld nach dem sortierte werden soll
         *  @param  string  $str_SortTyp    Art der Sortierung (ASC oder DESC)
         *  @return array
         */
        public function getAll($i_Start = 0,  $i_MaxResults = 20, $str_SortBy = 'name', $str_SortTyp = 'ASC') {
            $this->db->select('*');
            $this->db->from('vortrag');
            $this->db->join('veranstalter', 'veranstalter.veranstalterID = vortrag.veranstalter');
            $this->db->join('ort', 'ort.id = vortrag.ort');
            $this->db->join('belegung', 'belegung.vortrag = vortrag.vortragID');

            if ($str_SortBy == 'datum') {
                $this->db->order_by('belegung.datum', $str_SortTyp);
            }
            else {
                $this->db->order_by('vortrag.' . $str_SortBy, $str_SortTyp);
            }
            if ($i_Start > -1) {
                $this->db->limit($i_MaxResults, $i_Start);
            }
            $arr_Result =   $this->db->get();

            return $arr_Result->result_array();
        }

        /**
         * 
         */
        public function getActual($i_Start = 0,  $i_MaxResults = 20, $str_SortBy = 'name', $str_SortTyp = 'ASC') {
            $this->db->select('*');
            $this->db->from('vortrag');
            $this->db->where('belegung.datum >= \'' . date('Y-m-d') . '\'', null, false);
            $this->db->join('veranstalter', 'veranstalter.veranstalterID = vortrag.veranstalter');
            $this->db->join('ort', 'ort.id = vortrag.ort');
            $this->db->join('belegung', 'belegung.vortrag = vortrag.vortragID');

            if ($str_SortBy == 'datum') {
                $this->db->order_by('belegung.datum', $str_SortTyp);
            }
            else {
                $this->db->order_by('vortrag.' . $str_SortBy, $str_SortTyp);
            }
            if ($i_Start > -1) {
                $this->db->limit($i_MaxResults, $i_Start);
            }
            $arr_Result =   $this->db->get();

            return $arr_Result->result_array();
        }

        public function getOne($i_ID) {
            $this->db->join('belegung', 'belegung.vortrag = vortrag.vortragID');
            $this->db->where('vortragID', $i_ID);
            $arr_Result =   $this->db->get('vortrag');            

            return $arr_Result->row_array();
        }

        /**
         *  Gibt die Anzahl der vorhandenen Vorträge zurück
         *
         *  @return integer
         */
        public function getNumberOfRecords() {
            return $this->db->count_all('vortrag');
        }

        /**
         *  Legt einen neuen Vortrag an
         *
         *  @param  array   $arr_Data   Feld => Wert
         *  @return integer|boolean
         */
        public function create($arr_Data) {
            if ($this->db->insert('vortrag', $arr_Data) == true) {
                return $this->db->insert_id();
            }
            else {
                return false;
            }
        }

        /**
         *  Aktuallisiert einen bestehenden Eintrag
         *
         *  @param  integer $i_ID       ID des Vortrags
         *  @param  array   $arr_Data   Daten (Feld => Wert)
         */
        public function update($i_ID, $arr_Data) {
            $this->db->where('vortragID', $i_ID);
            $this->db->update('vortrag', $arr_Data);
        }

        /**
         *  Löscht den angegebenen Vortrag
         *
         *  @param  integer $i_ID   ID des Vortrags
         *  @return boolean
         */
        public function delete($i_ID) {
            return $this->db->delete('vortrag', array('vortragID' => $i_ID));
        }
    }
?>
