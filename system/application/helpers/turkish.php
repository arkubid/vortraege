<?php
/**
 * Entfernt türkische Sonderzeichen, die nicht im ISO-8859-1 Standard vorhanden sind und auch nicht automatisch
 * umgewandelt werden können.
 *
 * @param $input
 * @return string
 */
function replaceTurkishChars($input)
{
    $result = str_replace(
        array('ç', 'Ç', 'ğ', 'Ğ', 'ı', 'İ', 'ş', 'Ş'),
        array('c', 'C', 'g', 'G', 'i', 'I', 's', 'S'),
        $input
    );
    return iconv(mb_detect_encoding($result), 'ISO-8859-1//TRANSLIT//IGNORE', $result);
}
