<?php
    if (function_exists('formatFeedContent') == false) {
        function formatFeedContent($arr_Data, $b_Content = false) {
            $arr_Return =   array();
            $str_URL    =   base_url() . 'vortraege/single/';

            foreach ($arr_Data as $arr_Element) {                
                $arr_Row            =   array();

                $arr_Row['link']    =   $str_URL . $arr_Element['vortragID'];
                $arr_Row['datum']   =   $arr_Element['datum'] . 'T' . $arr_Element['beginn'] . '.00+02:00';

                // Leeren Referenten und Uni abfangen (für Kolloquium etc.)
                $arr_Row['titel']   =   '';
                if ($arr_Element['referent'] !== '') {
                    $arr_Row['titel']   =   $arr_Element['referent']. ' ';
                }
                if ($arr_Element['herkunft'] !== '') {
                    $arr_Row['titel']   .=   '(' . $arr_Element['herkunft'] . '), ';
                }
                if ($arr_Element['titel'] !== '') {
                    $arr_Row['titel']   .=  '"' . $arr_Element['titel'] . '"';
                }
               
                $arr_Time           =   array(0 => ' s. t.', 1 => ' c. t.');
                $str_Time           =   gTime($arr_Element['beginn']) . ' Uhr ' . $arr_Time[$arr_Element['ct']];
                $arr_Row['Body']    =   array(
                                            array(
                                                'Titel' =>  'Datum',
                                                'Value' =>  gDay($arr_Element['datum']) . ', ' . gDate($arr_Element['datum'])
                                            ),
                                            array(
                                                'Titel' => 'Uhrzeit',
                                                'Value' =>  $str_Time),
                                            array(
                                                'Titel' => 'Veranstalter',
                                                'Value' =>  $arr_Element['veranstalterName']),
                                            array(
                                                'Titel' => 'Ort',
                                                'Value' =>  $arr_Element['adresse']));

                if ($arr_Element['attachment'] == 1) {
                    $arr_Row['Body'][]  =   array(
                                'Titel' =>  'Informationen',
                                'Value' =>  '<a href="' . base_url() . 'attachment/vortrag' . $arr_Element['vortragID'] . '.pdf" target="_blank">Siehe Anhang</a>'
                    );
                }

                if ($b_Content == true) {
                    $arr_Row['Content'] =   $arr_Row['titel'];

                    if ($arr_Element['veranstalterDisplay'] !== '') {
                        $arr_Row['Content'] .=  ', (' . $arr_Element['veranstalterDisplay'] . ')';
                    }

                    if ($arr_Element['ort'] !== '') {
                        $arr_Row['Content'] .=  ', ' . $arr_Element['ort'];
                    }

                }

                $arr_Return[]   =   $arr_Row;                
            }

            return $arr_Return;
        }
    }
?>
