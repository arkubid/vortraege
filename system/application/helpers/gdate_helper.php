<?php
    if (function_exists('gDate') == false) {
        function gDate($str_Datum) {
            $arr_Date   =   explode('-', $str_Datum);

            if (count($arr_Date) == 3) {
                return $arr_Date[2] . '.' . $arr_Date[1] . '.' . $arr_Date[0];
            }
            else {
                return '';
            }
        }
    }

    if (function_exists('gDay') == false) {
        function gDay($str_Datum) {
         //   setlocale(LC_ALL, 'de_DE');
            $arr_Days   =   array('', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag');

            $arr_Date   =   explode('-', $str_Datum);
            return $arr_Days[date('w', mktime(0, 0, 0, $arr_Date[1], $arr_Date[2], $arr_Date[0]))];
        }
    }

    if (function_exists('gTime') == false) {
        function gTime($str_Time) {
            $arr_Time   =   explode(':', $str_Time);
            $str_Result =   $arr_Time[0];

            if ($arr_Time[1] !== '00') {
                $str_Result .=  '.' . $arr_Time[1];
            }

            return $str_Result;
        }
    }

    if (function_exists('formatDate') == false) {
        function formatDate($str_Date, $str_Output = 'd.m.Y') {
            $str_Return =   false;

            if ($str_Date !== '') {
                $str_Time   =   strtotime($str_Date);

                if ($str_Time !== false
                    && $str_Output !== '') {
                        date_default_timezone_set('Europe/Berlin');
                        setlocale(LC_TIME, 'de_DE');
                        
                        $str_Return =   strftime($str_Output, $str_Time);
                }
            }

            return $str_Return;
        }
    }
?>
