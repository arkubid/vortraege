<?php
require_once __DIR__ . '/FlyerInterface.php';

/**
 * Einen Flyer für die Vorträge der Christlichen Archäologie erstellen.
 *
 * @author  Benjamin Geißler <benjamin.geissler@gmail.com>
 */
class FlyerCA implements FlyerInterface
{
    /**
     * Einen Flyer für die Vorträge der Christlichen Archäologie erstellen.
     *
     * @param Cezpdf $pdf
     * @param array $content
     * @param array $type
     * @throws ErrorException
     * @return Cezpdf
     */
    public function create(Cezpdf $pdf, array $content, array $type)
    {
        // Kopf Logo (Insitutslogo links, rechts Uni-Logo)
        $logo = dirname(__FILE__) . '/flyer/ak/ca-logo-great.jpg';
        if (file_exists($logo) == false) {
            throw new ErrorException('Die Logo-Datei zum erstellen des PDFs fehlt!');
        }

        $pdf->ezSetCmMargins(0, 0, 0, 0);
        $pdf->ezImage($logo, 0, 0, 'full', 'left', '');
        $pdf->ezSetCmMargins(1, 1, 2, 2);

        $pdf->setLineStyle(1);
        $pdf->line(30, 700, 565, 700);

        // Datum
        $pdf->ezSetDy(-15);
        $pdf->ezText(
            '<b>' . gDay($content['datum']) . ', den ' . formatDate($content['datum'],'%d. %B %Y') . '</b>',
            24,
            array('justification' => 'center')
        );

        // Referent (auf türkische Umlaute prüfen)
        $pdf->ezSetDy(-60);
        $pdf->ezText(replaceTurkishChars($content['referent']), 34, array('justification' => 'center'));

        // Herkunft (auf türkische Umlaute prüfen)
        $pdf->ezText(
            utf8_decode('<i>' . $content['herkunft'] . '</i>'),
            18,
            array('justification' => 'center')
        );

        // Vortrags Titel (auf türkische Umlaute prüfen)
        $pdf->ezSetDy(-10);
        $pdf->ezText(replaceTurkishChars($content['titel']), 28, array('justification' => 'center'));

        // Veranstaltugnsort
        $pdf->ezSetDy(-80);
        $pdf->ezText(utf8_decode($type['ort']), 16, array('aleft' => 80));

        // Uhrzeit
        $str_CumTempore = 's. t.';
        if ($content['ct'] == 1) {
            $str_CumTempore = 'c. t.';
        }
        $pdf->ezText(
            utf8_decode('Beginn: ' . gTime($content['beginn']) . ' Uhr ' . $str_CumTempore),
            16,
            array('aleft' => 80)
        );

        return $pdf;
    }
}
