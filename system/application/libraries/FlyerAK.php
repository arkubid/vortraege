<?php
require_once __DIR__ . '/FlyerInterface.php';

/**
 * Eine Flyer für Vorträge der Klassische Archäologie erstellen.
 *
 * @author  Benjamin Geißler <benjamin.geissler@gmail.com>
 */
class FlyerAK implements FlyerInterface
{
    /**
     * Eine Flyer für Vorträge der Klassische Archäologie erstellen.
     *
     * @param \Cezpdf $pdf
     * @param array $content
     * @param array $type
     * @throws ErrorException
     * @return mixed
     */
    public function create(Cezpdf $pdf, array $content, array $type)
    {        
        // Kopf Logo
        $logo = dirname(__FILE__) . '/flyer/ak/logo.jpg';
        if (file_exists($logo) == false) {
            throw new ErrorException('Die Logo-Datei zum erstellen des PDFs fehlt!');
        }
        $pdf->ezImage($logo);

        // Abteilungsangabe
        $pdf->ezText(
            utf8_decode('<b>' . $type['abteilung'] . '</b>'),
            26,
            array('justification' => 'center')
        );

        // Institusanabge
        $pdf->ezSetDy(-6);
        $pdf->ezText(
            utf8_decode($type['institut']),
            16,
            array('justification' => 'center')
        );

        // Universität
        $pdf->ezText(
            utf8_decode($type['uni']),
            14,
            array('justification' => 'center')
        );
        $pdf->setLineStyle(1);
        $pdf->line(30, 630, 565, 630);

        // Datum
        $pdf->ezSetDy(-40);
        $pdf->ezText(
            '<b>' . gDay($content['datum']) . ', den ' . formatDate($content['datum'],'%d. %B %Y') . '</b>',
            24,
            array('justification' => 'center')
        );

        // Referent (auf türkische Umlaute prüfen)
        $pdf->ezSetDy(-80);
        $pdf->ezText(replaceTurkishChars($content['referent']), 34, array('justification' => 'center'));

        // Herkunft (auf türkische Umlaute prüfen)
        $pdf->ezText(
            utf8_decode('<i>' . $content['herkunft'] . '</i>'),
            18,
            array('justification' => 'center')
        );

        // Vortrags Titel (auf türkische Umlaute prüfen)
        $pdf->ezSetDy(-10);
        $pdf->ezText(replaceTurkishChars($content['titel']), 28, array('justification' => 'center'));

        // Veranstaltugnsort
        $pdf->ezSetDy(-80);
        $pdf->ezText(utf8_decode($type['ort']), 16, array('aleft' => 80));

        // Uhrzeit
        $str_CumTempore = 's. t.';
        if ($content['ct'] == 1) {
            $str_CumTempore = 'c. t.';
        }
        $pdf->ezText(
            utf8_decode('Beginn: ' . gTime($content['beginn']) . ' Uhr ' . $str_CumTempore),
            16,
            array('aleft' => 80)
        );

        // Impressum und Uni-Logo
        $pdf->ezSetY(80);
        $pdf->ezImage(dirname(__FILE__) . '/flyer/ak/unibonn.jpg', 0, 250, 'none', 'right');
        $pdf->ezSetY(65);
        $pdf->ezText(
            utf8_decode($type['impressum']),
            10,
            array('justification' => 'left')
        );

        return $pdf;
    }
}
