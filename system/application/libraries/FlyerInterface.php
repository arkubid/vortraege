<?php

/**
 * Einen Flyer nach den Vorgaben eines Instituts erstellen.
 *
 * @author  Benjamin Geißler <benjamin.geissler@gmail.com>
 */
interface FlyerInterface
{
    /**
     * Einen Flyer nach den Vorgaben eines Instituts erstellen.
     * @param Cezpdf $pdf
     * @param array $content
     * @param array $type
     * @return Cezpdf
     */
    public function create(Cezpdf $pdf, array $content, array $type);
}
