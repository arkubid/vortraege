<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 *  Erzeugt eine DIN-A4 PDF Dokument als Flyer für einen Vortrag, der je nach Typ verschieden gestalltet ist
 *
 * @author  Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version 1.2
 */
class Flyer
{
    /** @var \Cezpdf  */
    private $pdf;
    /** @var  array */
    private $types;
    /** @var string  */
    private $type;
    /** @var  array */
    private $content;
    /** @var  array */
    private $options;
    /** @var  FlyerInterface */
    private $flyer;

    /**
     *  Konstrutor, der die Daten aus der Konfigurations Datei lädt
     *
     * @param array $configuration Array mit den injected Konfigurations Daten aus der config/flyer.php
     */
    public function  __construct($configuration)
    {
        require_once __DIR__ . '/cezpdf.php';
        require_once __DIR__ . '/../helpers/gdate_helper.php';
        require_once __DIR__ . '/../helpers/turkish.php';

        // PDF inizialisieren
        $this->pdf = new Cezpdf();
        $this->pdf->setFontFamily($configuration['font'], $configuration['fontoptions']);
        $this->pdf->selectFont('./fonts/' . $configuration['font']);
        $this->pdf->ezSetCmMargins(1, 1, 2, 2);

        // Art des Typs (Abteilung etc.)
        $this->types = $configuration['type'];
        $this->type  = 'KA';

        // Alle gemeine Anzeige Optionen
        $this->options = $configuration['contentoptions'];
    }

    /**
     * Setzt die Art der PDF Vorlage fest
     *
     * @param  string $typ
     * @throws ErrorException
     * @return $this
     */
    public function setTyp($typ)
    {
        if (array_key_exists($typ, $this->types) == true) {
            $this->type = $typ;
            return $this;
        }

        throw new ErrorException('Fehlende Konfigurations Angaben für diesen Anzeige Typ (' . $typ . ')!');
    }

    /**
     * Injectet den Flyer Erstellungs Instanz.
     * @param FlyerInterface $flyer
     * @return $this
     */
    public function setFlyer(FlyerInterface $flyer)
    {
        $this->flyer = $flyer;
        return $this;
    }

    /**
     * Setzt den Array mit den Parameter für die Anzeige.
     *
     * @param array $arr_Content    Array mit den anzuzeigenden Parametern.
     * @throws ErrorException
     * @return $this
     */
    public function setContent($arr_Content)
    {
        if (is_array($arr_Content) == true
            && count($arr_Content) > 0) {
            $this->content = $arr_Content;
            return $this;
        }

        throw new ErrorException('Es wurden keine Daten zum erstellen des PDFs übergeben!');
    }

    /**
     * Erzeugt die gewünschte PDF-Datei und stream diese an den Browser zurück.
     */
    public function stream()
    {
        if ($this->flyer instanceof FlyerInterface) {
            $this->pdf = $this->flyer->create($this->pdf, $this->content, $this->types[$this->type]);
            $this->pdf->ezStream(array('Content-Disposition' => 'Flyer.pdf'));
        }

        throw new ErrorException('Es wurde keine Flyer-Konfiguration übergeben!');
    }
}
