<?php
require_once __DIR__ . '/vendor/autoload.php';

/**
 *  Google Calendar APIv3 Wrapper
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.3
 */
class GoogleCalendar
{
    /** @var string */
    private $mail = '848268685672-nhg07ds881eou3i6oag86o6ruemeahq3@developer.gserviceaccount.com';
    /** @var string */
    private $calendarId = '1fju19jk735caqaravs83a9iek@group.calendar.google.com';
    /** @var int */
    private $timezone = 1;
    /** @var bool */
    private $hasSummerTime = true;
    /** @var \Google_Service_Calendar */
    private $calendar;

    /**
     *  Konstruktor, die Einstellungen sind z. Z. fest eingebaut.
     *
     * @param array $config
     */
    public function  __construct($config)
    {
        $credentials = new Google_Auth_AssertionCredentials(
            $this->mail,
            array('https://www.googleapis.com/auth/calendar'),
            file_get_contents(__DIR__ . '/ai-vortraege.p12')
        );

        $client = new Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion();
        }
        $this->calendar = new Google_Service_Calendar($client);
    }

    /**
     *  Legt einen neuen Eintrag an und gibt die erzeugte URL zurück.
     *
     * @param string $title Titel
     * @param string $date Datum
     * @param string $from Uhrzeit von
     * @param string $till Uhrzeit bis
     * @param string $location Ort
     *
     * @return boolean
     */
    public function createEvent($title, $date, $from, $till, $location)
    {
        $event = $this->fillEvent($title, $date, $from, $till,$location);
        $result = $this->calendar->events->insert($this->calendarId, $event);
        return $result->getId();
    }

    /**
     *  Aktuallisiert den angegebenen Eintrag
     *
     * @param string $eventId URL zu dem Eintrag
     * @param string $title Titel
     * @param string $date Datum
     * @param string $from Uhrzeit von
     * @param string $till Uhrzeit bis
     * @param string $location Ort
     *
     * @return boolean
     */
    public function updateEvent($eventId, $title, $date, $from, $till, $location)
    {
        $event  = $this->fillEvent($title, $date, $from, $till, $location);
        $this->calendar->events->update($this->calendarId, $eventId, $event);

        return true;
    }

    /**
     *  Löscht den angegebenen Eintrag aus dem Kalender
     *
     * @param string $eventId
     * @return null
     */
    public function deleteEvent($eventId)
    {
        $result = $this->calendar->events->delete($this->calendarId, $eventId);
        return null;
    }

    private function fillEvent($title, $date, $from, $till, $location)
    {
        $event = new Google_Service_Calendar_Event();
        $event->setSummary($title);
        $event->setDescription($title);
        $event->setLocation($location);

        $start = new Google_Service_Calendar_EventDateTime();
        $start->setDateTime($this->formatTime($date, $from));
        $event->setStart($start);

        $end = new Google_Service_Calendar_EventDateTime();
        $end->setDateTime($this->formatTime($date, $till));
        $event->setEnd($end);

        return $event;
    }

    /**
     *  Trägt die Daten in das Kalender Event Objekt ein
     *
     * @param string $str_Titel Titel
     * @param string $str_Date Datum
     * @param string $str_From Uhrzeit von
     * @param string $str_Till Uhrzeit bis
     * @param string $str_Location Ort
     */
    private function setEvent($str_Titel, $str_Date, $str_From, $str_Till, $str_Location)
    {
        $this->obj_Event->title = $this->obj_Calendar->newTitle($str_Titel);
        $this->obj_Event->where = array($this->obj_Calendar->newWhere($str_Location));

        // Datum eintragen
        $obj_When              = $this->obj_Calendar->newWhen();
        $obj_When->startTime   = $this->formatTime($str_Date, $str_From);
        $obj_When->endTime     = $this->formatTime($str_Date, $str_Till);
        $this->obj_Event->when = array($obj_When);
    }

    /**
     *  Gibt das Datum im RFC 3339 Format zurück.
     *
     * @param string $date Tag
     * @param string $time Uhrzeit
     *
     * @return string
     */
    private function formatTime($date, $time)
    {
        // ':00.000+00:00'
        return $date . 'T' . $time . '.000+' . $this->formatTimeZone($date) . ':00';
    }

    /**
     *  Gibt den zu addieren Zeitzonen Wert inklusive ggf. gültiger Zeitumgstellungs zurück
     *
     * @param  string $date Datum in der US-Formatierung (Jahr-Monat-Tag)
     * @return string
     */
    private function formatTimeZone($date)
    {
        $timezone = $this->timezone;

        if ($this->hasSummerTime) {
            // Sommerzeit Periode bestimmen
            $year           = date('Y', strtotime($date));
            $summerTimeFrom = mktime(2, 0, 0, 3, 31 - date('w', mktime(2, 0, 0, 3, 31, $year)), $year);
            $summerTimeTill = mktime(2, 0, 0, 10, 31 - date('w', mktime(2, 0, 0, 10, 31, $year)), $year);
            $dateTime       = strtotime($date);

            // In der Sommerzeit 1 Stund dazu addieren
            if ($dateTime >= $summerTimeFrom
                && $dateTime < $summerTimeTill
            ) {
                $timezone++;
            }
        }

        if ($timezone < 10) {
            $timezone = '0' . $timezone;
        }

        return $timezone;
    }
}
