<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * PDF Erstellungs Klasse die den Zugriff auf die eigentliche Datei kapselt und vereinfacht.
 *
 * @author     Benjamin Geißler <benjamin.geissler@gmail.com>
 * @version    1.1
 *
 * @property   Cezpdf  $pdf            PDF Objekt
 * @property   string  $saveDir        Speicher Ort
 * @property   array   $options        Cezpdf Anzeige Optionen
 * @property   boolean $maker            Gibt ab, ob bestimmte Einträge markiert werden sollen
 * @property   integer $position         Aktuelle Position
 * @property   string  $display        Anzuzeigender Text
 * @property   boolean $displayFooter    Gibt an, ob eine Fußzeile angezeigt werden soll
 * @property   boolean $groupDate      Gibt an, ob gleiche Tage gruppiert werden sollen
 * @property   integer $footerPosition   Abstand der Fußzeile vom unteren Seitenrand
 */
class PDF
{
    /** @var \Cezpdf */
    private $pdf;
    /** @var  string */
    private $saveDir;
    /** @var  array */
    private $options;
    /** @var  bool */
    private $maker;
    /** @var  string */
    private $display;
    /** @var  bool */
    private $displayFooter;
    /** @var  bool */
    private $groupDate;
    /** @var  int */
    private $footerPosition;
    /** @var int */
    private $position;

    /**
     * Konstrutor, der die Daten aus der Konfigurations Datei lädt.
     *
     * @param array $configuration
     */
    public function  __construct($configuration)
    {
        require_once __DIR__ . '/cezpdf.php';
        require_once __DIR__ . '/../helpers/gdate_helper.php';
        require_once __DIR__ . '/../helpers/turkish.php';

        $this->saveDir        = $configuration['savepath'];
        $this->options        = $configuration['contentoptions'];
        $this->maker          = $configuration['marker'];
        $this->display        = $configuration['display'];
        $this->displayFooter  = $configuration['displayFooter'];
        $this->groupDate      = $configuration['dateGrouped'];
        $this->footerPosition = $configuration['footerPosition'];

        $this->pdf = new Cezpdf();
        $this->pdf->setFontFamily($configuration['font'], $configuration['fontoptions']);
        $this->pdf->selectFont('./fonts/' . $configuration['font']);
        $this->position = 0;
    }

    /**
     * Aktiviert oder deaktiviert die Markierung von bestimmten Zeilen
     *
     * @param $maker
     */
    public function setMaker($maker)
    {
        if (is_bool($maker) == true) {
            $this->maker = $maker;
        }
    }

    /**
     * Setzt die Art, wie das Datum angezeigt werden soll.
     * normal  =   Es wird bei jedem Eintrag ein Datum angezeigt
     * grouped =   Bei gleichen Datumsangaben wird nur eines angezeigt
     *
     * @param  string $display
     */
    public function setDisplay($display)
    {
        $arr_Display = array(
            'vortrag'    => array(
                'footer'  => true,
                'grouped' => false
            ),
            'kolloquium' => array(
                'footer'  => false,
                'grouped' => true
            )
        );

        if (array_key_exists($display, $arr_Display) == true) {
            $this->display       = $display;
            $this->displayFooter = $arr_Display[$display]['footer'];
            $this->groupDate     = $arr_Display[$display]['grouped'];
        }
    }

    /**
     * Setzt den Abstand zwischen den einzelnen Zeilen.
     *
     * @param integer $i_Distance
     */
    public function setDistance($i_Distance)
    {
        if ($i_Distance !== ''
            && is_int((int)$i_Distance) == true
            && $i_Distance > 0
        ) {
            $this->options['rowGap'] = $i_Distance;
        }
    }

    /**
     * Setzt die Überschrift für die Liste.
     *
     * @param string $title
     * @param string $subTitle
     */
    public function setTitel($title, $subTitle = '')
    {
        $this->pdf->ezText(utf8_decode('<b>' . $title . '</b>'), 18, array('justification' => 'center'));
        $this->pdf->ezSetDy(-6);

        if ($subTitle !== '') {
            $this->pdf->ezText(utf8_decode($subTitle), 12, array('justification' => 'center'));
            $this->pdf->ezSetDy(-6);
        }

        $this->pdf->ezText(
            '<i>Stand: ' . mdate('%d.%m.%Y', time()) . '</i>',
            10,
            array('justification' => 'center')
        );
        $this->pdf->ezSetDy(-30);
    }

    /**
     * Setzt den Inhalt der Liste fest.
     *
     * @param array $arr_Data
     */
    public function setContent($arr_Data)
    {
        $columns        = array('datum' => '', 'titel' => '');
        $this->position = $this->pdf->ezTable(
            $this->formatContent($arr_Data),
            $columns,
            '',
            $this->options
        );
    }

    /**
     * Schreibt den Footer, wobei darauf geachtet wird, dass dieser auch komplett zu lesen ist.
     *
     * @param string $footer
     */
    public function setFooter($footer)
    {
        if ($this->displayFooter == true) {
            if ($this->position < $this->footerPosition) {
                $this->pdf->ezSetDy(-100, 'makeSpace');
            }

            $this->pdf->ezSetY($this->footerPosition);
            $this->pdf->setLineStyle(1);
            $this->pdf->ezText($this->formatFooter($footer), 9, array('left' => 18, 'right' => 40));
        }
    }

    /**
     * Stream das PDF zurück an den Browser.
     */
    public function stream()
    {
        $this->pdf->ezStream(array('Content-Disposition' => 'Vortraege.pdf'));
    }

    /**
     * Speichert die Datei auf der lokalen Festplatte.
     *
     * @param string $fileName Dateiname
     * @return string
     */
    public function save($fileName = 'Vortraege')
    {
        $file_PDF = fopen($this->saveDir . $fileName . '.pdf', 'w');
        fwrite($file_PDF, $this->pdf->ezOutput());
        fclose($file_PDF);

        return $this->saveDir . $fileName . '.pdf';
    }

    /**
     * Formatiert die Vorträge für die Ausgabe als PDF um.
     *
     * @param array $data
     * @return array
     */
    private function formatContent($data)
    {
        $return = array();
        foreach ($data as $row) {
            if ($this->display == 'vortrag') {
                $return[] = $this->formatTalk($row);
            } else {
                $return[] = $this->formatColloquium($row);
            }
        }

        // ggf. gleiche Datumsangaben entfernen
        $return = $this->formatGrouped($return);

        return $return;
    }

    /**
     * Formatiert die Anzeige (Titel, Datum) für die Vortrags-Listen-Ausgabe.
     *
     * @param  array $input Parameter
     * @return array
     */
    private function formatTalk($input)
    {
        $row          = array();
        $row['titel'] = '';

        // ggf. Marker für einzelne Zeilen verwenden
        if ($this->maker == true
            && $input['marker'] == true
        ) {
            $row['titel'] = '<b>';
        }

        $row['datum'] = gDate($input['datum']) . "\n" . '<i>' . gDay($input['datum']) . '</i>';

        if ($input['referent'] !== '') {
            $row['titel'] .= $input['referent'];
        }

        if ($input['herkunft'] !== '') {
            $row['titel'] .= ' (' . $input['herkunft'] . ')';
        }

        if ($row['titel'] !== '') {
            $row['titel'] .= ', ';
        }

        $row['titel'] .= '"' . $input['titel'] . '"';

        if ($input['veranstalterDisplay'] != '') {
            $row['titel'] .= ' (' . $input['veranstalterDisplay'] . ')';
        }

        $row['titel'] .= ', ' . $input['ort'] . ', ' . gTime($input['beginn']) . ' Uhr';

        if ($input['ct'] == '1') {
            $row['titel'] .= ' c. t.';
        } else {
            $row['titel'] .= ' s. t.';
        }

        if ($this->maker == true
            && $input['marker'] == true
        ) {
            $row['titel'] .= '</b>';
        }

        $row['datum'] = utf8_decode($row['datum']);
        $row['titel'] = replaceTurkishChars($row['titel']);

        return $row;
    }

    /**
     * Formatiert die Anzeige (Datum, Titel) für Ausgabe einer Kolloquiums List.
     *
     * @param  array $input Parameter
     * @return array
     */
    private function formatColloquium($input)
    {
        $row = array('datum' => '', 'titel' => '');

        // Nur Datum anzeigen
        $row['datum'] = gDate($input['datum']);

        // Referent und Titel anzeigen
        if ($input['referent'] !== '') {
            $row['titel'] = $input['referent'] . ', ';
        }
        $row['titel'] .= '"' . $input['titel'] . '"';

        $row['datum'] = utf8_decode($row['datum']);
        $row['titel'] = replaceTurkishChars($row['titel']);

        return $row;
    }

    /**
     * Entfernt bei der gruppierten Datumsanzeige identische Datumsangaben.
     *
     * @param array $data
     * @return array
     */
    private function formatGrouped($data)
    {
        if ($this->groupDate == true) {
            $last   = '';
            $length = count($data);

            for ($i = 0; $i < $length; $i++) {
                if ($last == $data[$i]['datum']) {
                    $data[$i]['datum'] = '';
                }
                $last = $data[$i]['datum'];
            }
        }

        return $data;
    }

    /**
     * Die Fußzeile für das PDF erstellen
     *
     * @param $data
     * @return string
     */
    private function formatFooter($data)
    {
        $return = array();
        foreach ($data as $row) {
            $return[] = '<b>' . $row['ort'] . '</b>: ' . $row['adresse'];
        }

        return utf8_decode(implode('; ', $return));
    }
}
