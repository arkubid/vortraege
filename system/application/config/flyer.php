<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$config['font']           = 'Helvetica.afm';
$config['fontoptions']    = array(
    'b'  => 'Helvetica-Bold.afm',
    'i'  => 'Helvetica-Oblique.afm',
    'bi' => 'Helvetica-BoldOblique.afm',
    'ib' => 'Helvetica-BoldOblique.afm'
);
$config['contentoptions'] = array(
    'showLines'    => 0,
    'showHeadings' => 0,
    'shaded'       => 0,
    'maxWidth'     => 500,
    'fontSize'     => 12,
    'rowGap'       => 8
);
// Konfigurations Einstellungen für die weiteren Angaben des Fylers
// Der Type wird dabei immer anhand der Art des Vortrages ausgewählt
$config['type']           = array(
    'KA' => array(
        'abteilung' => 'Abteilung für Klassische Archäologie',
        'institut'  => 'Institut für Archäologie und Kulturanthropologie',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Klassische Archäologie, Am Hofgarten 21, 53113 Bonn'
    ),
    'CA' => array(
        'abteilung' => 'Abteilung für Christliche Archäologie',
        'institut'  => 'Institut für Archäologie und Kulturanthropologie',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/ca-logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Christliche Archäologie, Regina-Pacis-Weg 1, 53113 Bonn'
    ),
    'VFG' => array(
        'abteilung' => 'Abteilung für Vor- und Frühgeschichtliche Archäologie',
        'institut'  => 'Institut für Archäologie und Kulturanthropologie',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Vor- und Frühgeschichtliche Archäologie, Regina-Pacis-Weg 7, 53113 Bonn'
    ),
    'AG' => array(
        'abteilung' => 'Abteilung für Ägyptologie',
        'institut'  => 'Institut für Archäologie und Kulturanthropologie',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Ägyptologie, Regina-Pacis-Weg 7, 53113 Bonn'
    ),
    'HI' => array(
        'abteilung' => 'Abteilung für Alte Geschichte',
        'institut'  => 'Institut für Geschichtswissenschaft',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Alte Geschichte, Am Hof 1e, 53113 Bonn'
    ),
    'PA' => array(
        'abteilung' => 'Abteilung für Klassische Archäologie',
        'institut'  => 'Institut für Archäologie und Kulturanthropologie',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Klassische Archäologie, Am Hofgarten 21, 53113 Bonn'
    ),
    'AA' => array(
        'abteilung' => 'Abteilung für Altamerikanistik',
        'institut'  => 'Institut für Archäologie und Kulturanthropologie',
        'uni'       => 'Rheinische Friedrich-Wilhelms-Universität Bonn',
        'logo'      => '/ak/logo.jpg',
        'ort'       => 'Hörsaal der Abteilung Klassische Archäologie',
        'impressum' => 'Abteilung für Altamerikanistik, Oxfordstraße 15, 53111 Bonn'
    )
);