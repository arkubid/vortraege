<?php
    $config = array(
                 'vortrag' => array(
                                    array(
                                            'field' => 'datum',
                                            'label' => 'Datum',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'beginnHour',
                                            'label' => 'Beginn',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'beginnMinute',
                                            'label' => 'Beginn',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'endeHour',
                                            'label' => 'Ende',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'endeMinute',
                                            'label' => 'Beginn',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'ort',
                                            'label' => 'Ort',
                                            'rules' => 'integer|required'
                                         ),
                                    array(
                                            'field' => 'stadt',
                                            'label' => 'Stadt',
                                            'rules' => 'integer|required'
                                         ),
                                    array(
                                            'field' => 'veranstalter',
                                            'label' => 'Veranstalter',
                                            'rules' => 'integer|required'
                                         ),
                                    array(
                                            'field' => 'type',
                                            'label' => 'Art',
                                            'rules' => 'integer|required'
                                         ),
                                    array(
                                            'field' => 'referent',
                                            'label' => 'Referent',
                                            'rules' => 'trim'
                                         ),
                                    array(
                                            'field' => 'herkunft',
                                            'label' => 'Universität',
                                            'rules' => 'trim'
                                         ),
                                    array(
                                            'field' => 'titel',
                                            'label' => 'Titel',
                                            'rules' => 'trim'
                                         ),
                                    array(
                                            'field' => 'link',
                                            'label' => 'Link',
                                            'rules' => 'trim'
                                         )
                                    ),
                'newsletter' => array(
                                    array(
                                        'field' =>  'email',
                                        'label' =>  'E-Mail',
                                        'rules' =>  'trim|valid_email|required'
                                    )),
                'sendmail' => array(
                                    array(
                                        'field' =>  'topic',
                                        'label' =>  'Betreff',
                                        'rules' =>  'trim|required'
                                    ),
                                    array(
                                        'field' =>  'content',
                                        'label' =>  'Inhalt',
                                        'rules' =>  'required|min_length[15]'
                                    )),
                'print' =>  array(
                                array(
                                    'field' =>  'titel',
                                    'label' =>  'Überschrift',
                                    'rules' =>  'trim|required|min_length[7]'
                                ),
                                array(
                                    'field' =>  'distance',
                                    'label' =>  'Zeilen Abstand',
                                    'ruels' =>  'required|min_length[1]|is_natural_no_zero'
                                )
                ),
                'ort'   =>  array(
                                array(
                                    'field' =>  'ort',
                                    'label' =>  'Ort',
                                    'rules' =>  'trim|required|xss_clean'
                                ),
                                array(
                                    'field' =>  'adresse',
                                    'label' =>  'Adresse',
                                    'rules' =>  'trim|required|xss_clean'
                                ),
                                array(
                                    'field' =>  'maps',
                                    'label' =>  'Google Maps',
                                    'rules' =>  'trim|prep_url'
                                ),
                                array(
                                    'field' =>  'website',
                                    'label' =>  'Webseite',
                                    'rules' =>  'trim'
                                ),
                                array(
                                    'field' =>  'stadt',
                                    'label' =>  'Stadt',
                                    'rules' =>  'required|is_natural_no_zero'
                                ),
                                array(
                                    'field' =>  'activated',
                                    'label' =>  'Aktiv',
                                    'rules' =>  'trim'
                                )
                ),
            'veranstalter' => array(
                                array(
                                    'field' =>  'veranstalterName',
                                    'label' =>  'Veranstalter',
                                    'rules' =>  'trim|required|xss_clean'
                                ),
                                array(
                                    'field' =>  'veranstalterShort',
                                    'label' =>  'Abkürzung',
                                    'rules' =>  'trim|xss_clean'
                                ),
                                array(
                                    'field' =>  'veranstalterDisplay',
                                    'label' =>  'Anzeige',
                                    'rules' =>  'trim|xss_clean'
                                ),
                                array(
                                    'field' =>  'veranstalterType',
                                    'label' =>  'Fachgebiet',
                                    'rules' =>  'trim|required|is_natural_no_zero'
                                ),
                                array(
                                    'field' =>  'website',
                                    'label' =>  'Webseite',
                                    'rules' =>  'trim'
                                ),
                                array(
                                    'field' =>  'activated',
                                    'label' =>  'Aktiv',
                                    'rules' =>  'trim'
                                )),
            'feed'  =>  array(
                            array(
                                'field' =>  'link',
                                'label' =>  'Link',
                                'rules' =>  'trim|alpha_dash|required|xss_clean'
                            ),
                            array(
                                'field' =>  'titel',
                                'label' =>  'Titel',
                                'rules' =>  'trim|required|xss_clean'
                            ))
        );
?>
