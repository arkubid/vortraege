<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Konfigurations Einstellungen für den Google Kalender
 */
$config['user']         =   '';
$config['password']     =   '';
$config['calendar']     =   '';
$config['timezone']     =   1;
$config['summertime']   =   true;
