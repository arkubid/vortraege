<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Absoluter Pfad zum speicherort der PDFs
$config['savepath']         =   '/var/www/html/vortrag/download/';
$config['font']             =   'Helvetica.afm';
$config['fontoptions']      =   array(
                                    'b'     =>  'Helvetica-Bold.afm',
                                    'i'     =>  'Helvetica-Oblique.afm',
                                    'bi'    =>  'Helvetica-BoldOblique.afm',
                                    'ib'    =>  'Helvetica-BoldOblique.afm');
$config['contentoptions']   =   array(
                                    'showLines'     =>  0,
                                    'showHeadings'  =>  0,
                                    'shaded'        =>  0,
                                    'maxWidth'      =>  500,
                                    'fontSize'      =>  12,                                    
                                    'rowGap'        =>  8,
                                    'justification' => 'left');
$config['marker']           =   false;
$config['display']          =   'vortrag';
$config['displayFooter']    =   true;
$config['dateGrouped']      =   false;
$config['footerPosition']   =   180;
