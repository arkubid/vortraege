<?php
    require_once 'configuration.php';
    
    /**
     *  Verwaltet die Ortsangaben
     *
     *  @property   OrtModel    $OrtModel
     *  @property   string      $str_IDField    Name des ID Felds in der MySQL Datenbank Tabelle
     */
    class Ort extends Configuration {        
        protected   $str_IDField;

        /**
         *  Konstruktor, der das Model lädt und die View und Notice Werte setzt
         */
	public function Ort() {
            parent::Configuration();
            
            // Model laden
            $this->load->model('OrtModel');
            $this->obj_Model    =   $this->OrtModel;

            // View
            $this->str_View     =   'ort';
            $this->str_IDField  =   'id';

            // Notice
            $this->arr_Notice   =   array(
                                        'activate'      =>  'Der Veranstaltungsort wurde aktiviert',
                                        'deactivate'    =>  'Der Veranstaltungsort wurde deaktiviert',
                                        'delete'        =>  'Veranstaltungsorte können nicht gelöscht werden',
                                        'save'          =>  'Der Veranstaltungsort wurde angelegt',
                                        'update'        =>  'Der Veranstaltungsort wurde aktuallisiert',
                                        'error'         =>  'Parameter Fehler'
            );
	}

	/**
         *  Zeigt eine pagninierte Liste mit allen vorhandenen Orten an
         *
         *  @param string $str_SortBy   Feld nachdem sortiert wird
         *  @param string $str_SortTyp  Sortier Art
         *  @param integer $i_Start     Beginn der Anzuzeigenden Einträge
         */
        public function index($str_SortBy = 'ort', $str_SortTyp = 'ASC', $i_Start = 0) {
            parent::index($str_SortBy, $str_SortTyp, $i_Start);
        }

        /**
         *  Legt einen neuen Veranstaltungs Ort an, prüft zuvor, ob die eingebene Werte korrekt sind
         */
        public function save() {
            if ($this->b_LogedIn == true) {
                $this->load->library('form_validation');

                if ($this->form_validation->run($this->str_View) == true) {
                    $arr_Data   =   $this->loadInputData();

                    if ($this->obj_Model->create($arr_Data) == true) {
                        $this->str_Notice   =   $this->arr_Notice['save'];
                        $this->index();
                    }
                    else {
                        $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                        $this->create();
                    }
                }
                else {
                    $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                    $this->create();
                }
            }
            else {
                redirect('/vortraege');
            }
        }

        /**
         *  Lädt alle Daten des angegeben Datensatzes sowie die davon abhängigen, um den Eintrag zu editieren
         *
         *  @param  integer $i_ID
         */
        public function edit($i_ID) {
            if (isset($i_ID) == true
                && is_int((int)$i_ID) == true
                && $this->b_LogedIn == true) {                   
                    // Daten laden
                    $arr_Data               =   $this->loadDependingData();
                    $arr_Data['arr_Result'] =   $this->obj_Model->getOne($i_ID);

                    $this->setHeader();
                    $this->load->view($this->str_View . '/edit.php', $arr_Data);
                    $this->setFooter();
            }
            else {
                $this->str_Notice   =   'Bitte zuerst anmelden';
                $this->index();
            }
        }

        /**
         *  Speichert die Änderungen an dem Eintrag
         */
        public function update() {
            if ($this->b_LogedIn == true) {
                $i_ID   =   $this->input->post($this->str_IDField);
                
                if (is_numeric($i_ID) !== false) {
                    $this->load->library('form_validation');

                    if ($this->form_validation->run($this->str_View) == true) {
                        $arr_Data   =   $this->loadInputData();

                        if ($this->obj_Model->update($i_ID, $arr_Data) == true) {
                            $this->str_Notice   =   $this->arr_Notice['update'];
                            $this->index();
                        }
                        else {
                            $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                            $this->edit($i_ID);
                        }
                    }
                    else {
                        $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                        $this->edit($i_ID);
                    }
                }
                else {
                    $this->str_Notice   =   $this->arr_Notice['error'];
                    $this->index();
                }
            }
            else {
                redirect('/vortraege');
            }
        }

        /**
         *  Löschen von Orten verhindern
         *
         *  @param  integer $i_ID
         */
        public function delete($i_ID) {
            $this->str_Notice   =   $this->arr_Notice['delete'];
            $this->index();
        }

        /**
         *  Abhängigen Daten, d.h. per ID verknüpfte Felder in anderen Tabelle laden
         *
         *  @return array
         */
        protected function loadDependingData() {
            $arr_Data   =   array();

            $this->load->model('StadtModel');
            $arr_Data['arr_Stadt']  =   $this->StadtModel->getAll();

            return $arr_Data;
        }

        /**
         *  Lädt die in die Eingabe Felder eingebene Daten und gibt sie als Array zurück
         *
         *  @return array
         */
        protected function loadInputData() {
            $arr_Data   =   array(
                                'ort'       =>  $this->input->post('ort'),
                                'adresse'   =>  $this->input->post('adresse'),
                                'maps'      =>  $this->input->post('maps'),
                                'website'   =>  $this->input->post('website'),
                                'stadt'     =>  $this->input->post('stadt'));

            if ($this->input->post('activated') == 'on') {
                $arr_Data['activated']   =   1;
            }
            else {
                $arr_Data['activated']   =   0;
            }

            return $arr_Data;
        }
    }
?>
