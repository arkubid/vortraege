<?php
/**
 *  Vorträge Controller zum anzeigen, anlegen löschen etc von Vorträgen
 *
 * @property boolean           $b_LogedIn
 * @property VortraegeModel $VortraegeModel
 * @property PDF            $pdf
 * @property TypeModel      $TypeModel
 * @property VeranstalterModel $VeranstalterModel
 * @property Flyer $flyer
 */
class Vortraege extends Controller
{
    protected $b_LogedIn;
    protected $i_PerPage;
    protected $str_Notice;
    protected $str_Display;

    /**
     *  Konstruktor
     */
    public function Vortraege()
    {
        parent::Controller();

        // Paginierung und Auth
        $this->i_PerPage = 50;
        $this->b_LogedIn = false;
        $this->load->library('tank_auth');
        $this->b_LogedIn = $this->tank_auth->is_logged_in();

        // Model laden
        $this->load->model('VortraegeModel');
    }

    /**
     *  Zeigt die Liste mit den aktuellen Vorträgen an
     *
     * @param  integer $i_Location Orts Nr
     */
    public function index($i_Location = 0)
    {
        // ggf. Ort ermitteln
        $arr_Where = array();
        if ($i_Location > 0) {
            $arr_Where['vortrag.stadt'] = $i_Location;
        }

        $arr_Data               = array();
        $arr_Data['str_Notice'] = $this->str_Notice;
        $arr_Data['arr_Daten']  = $this->VortraegeModel->getList($arr_Where);

        $this->setHeader();
        $this->load->view('vortraege/list.php', $arr_Data);
        $this->setFooter();
    }

    /**
     *  Mapper auf die Index, um einen Ort als Parameter zuübergeben
     *
     * @param  integer $i_Location
     */
    public function liste($i_Location = 0)
    {
        $this->index($i_Location);
    }

    /**
     *  Zeigt eine Detail Informations Seite zu dem Vortrag an.
     *
     * @param  integer $i_ID
     */
    public function single($i_ID)
    {
        $this->setHeader();
        $this->load->view('vortraege/single.php', $this->loadSingleEntry($i_ID));
        $this->setFooter();
    }

    /**
     *  Gibt die aktuellen Vorträge als PDF zurück
     */
    public function download($b_JustLecture = 1)
    {
        $arr_Parameter = array('kolloquium' => 0);
        $str_Titel     = 'Archäologische Vorträge in Bonn und Köln';

        if ($b_JustLecture == 0) {
            $arr_Parameter = array();
            $str_Titel     = 'Archäologische Vorträge und Kolloquien in Bonn und Köln';
        }

        $this->load->library('pdf');
        $this->load->model('OrtModel');

        $this->pdf->setDisplay('vortrag');
        $this->pdf->setTitel($str_Titel, '');
        $this->pdf->setContent($this->VortraegeModel->getList($arr_Parameter));
        $this->pdf->setFooter($this->OrtModel->getAll());
        $this->pdf->stream();
    }

    /**
     *  Die kommenden Vorträge als RSS Feed anzeigen
     */
    public function rss(
        $str_Type = 'alle',
        $str_Stadt = 'alle',
        $str_Ort = 'alle',
        $str_Veranstalter = 'alle',
        $i_Kolloquium = 0,
        $b_Content = false
    ) {
        // Standard Titel festlegen
        $str_TitelTyp     = 'Archäologische';
        $str_TitelLecture = 'Vorträge';
        $str_TitelCity    = 'Bonn und Köln';

        $this->load->helper('xml');
        $this->load->helper('text');
        $this->load->helper('rss');

        // Zusätzliche Bedingung für den Feed
        $arr_Where = array();
        if ($str_Stadt !== 'alle') {
            $this->load->model('StadtModel');
            $arr_Where['vortrag.stadt'] = $this->StadtModel->getIDByName($str_Stadt);
            $str_TitelCity              = mb_strtoupper(mb_substr($str_Stadt, 0, 1)) . mb_substr($str_Stadt, 1);
        }

        if ($str_Type !== 'alle') {
            $this->load->model('TypeModel');
            $arr_Where['vortrag.type'] = $this->TypeModel->getIDByName($str_Type);
            $str_TitelTyp              = $this->TypeModel->getTitel($arr_Where['vortrag.type']);
        }

        if ($str_Veranstalter !== 'alle') {
            $this->load->model('VeranstalterModel');
            $arr_Where['vortrag.veranstalter'] = $this->VeranstalterModel->getIDByName($str_Veranstalter);
        }

        if ($str_Ort !== 'alle') {
            $this->load->model('OrtModel');
            $arr_Where['vortrag.ort'] = $this->OrtModel->getIDByOrt($str_Ort);
        }

        // Vorträge und/oder Kolloquien
        if ($i_Kolloquium >= 0) {
            $arr_Where['vortrag.kolloquium'] = $i_Kolloquium;

            if ($i_Kolloquium == 1) {
                $str_TitelLecture = 'Kolloquien';
            }

        } else {
            if ($i_Kolloquium == -1) {
                $str_TitelLecture = 'Vorträge und Kolloquien';
            }
        }

        $str_Titel = $str_TitelTyp . ' ' . $str_TitelLecture . ' in ' . $str_TitelCity;
        $arr_Data  = array(
            'str_Name'        => $str_Titel,
            'str_Encoding'    => 'utf-8',
            'str_Url'         => base_url(
                ) . 'vortraege/rss/' . $str_Type . '/' . $str_Stadt . '/' . $str_Ort . '/' . $str_Veranstalter . '/' . $i_Kolloquium,
            'str_Description' => $str_Titel,
            'str_Language'    => 'de-de',
            'str_Mail'        => 'archinst@uni-bonn.de',
            'arr_Data'        => formatFeedContent($this->VortraegeModel->getList($arr_Where), $b_Content)
        );

        header('Content-Type: application/rss+xml');
        $this->load->view('feed/rss.php', $arr_Data);
    }

    /**
     *  Zeigt eine pagninierte Liste mit allen vorhandenen Vorträgen an
     *
     * @param string $str_SortBy Feld nachdem sortiert wird
     * @param string $str_SortTyp Sortier Art
     * @param integer $i_Start Beginn der Anzuzeigenden Einträge
     */
    public function all($str_SortBy = 'datum', $str_SortTyp = 'ASC', $i_Start = 0)
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $arr_Data                = array();
            $arr_Data['arr_Result']  = $this->VortraegeModel->getAll(
                $i_Start,
                $this->i_PerPage,
                $str_SortBy,
                $str_SortTyp
            );
            $arr_Data['i_Results']   = $this->VortraegeModel->getNumberOfRecords();
            $arr_Data['i_First']     = $i_Start + 1;
            $arr_Data['i_Last']      = min($i_Start + 1 + $this->i_PerPage, $arr_Data['i_Results']);
            $arr_Data['str_SortTyp'] = $str_SortTyp;
            $arr_Data['str_SortBy']  = $str_SortBy;
            $arr_Data['str_Notice']  = $this->str_Notice;

            // Daten und Pagination
            $this->setHeader();
            $this->str_Display = 'all';
            $this->setPagination($str_SortBy . '/' . $str_SortTyp, $arr_Data['i_Results']);
            $this->load->view('vortraege/all.php', $arr_Data);
            $this->setFooter();
        } else {
            $this->index();
        }
    }

    /**
     *  Zeigt nur die noch kommenden Vorträge an
     *
     * @param string $str_SortBy Feld nach dem sortiert werden soll
     * @param string $str_SortTyp Art der Sortierung
     * @param integer $i_Start Treffer ab dem die Liste angezeigt werden soll
     */
    public function actual($str_SortBy = 'datum', $str_SortTyp = 'ASC', $i_Start = 0)
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $arr_Data                = array();
            $arr_Data['arr_Result']  = $this->VortraegeModel->getActual(
                $i_Start,
                $this->i_PerPage,
                $str_SortBy,
                $str_SortTyp
            );
            $arr_Data['i_Results']   = count($arr_Data['arr_Result']);
            $arr_Data['i_First']     = $i_Start + 1;
            $arr_Data['i_Last']      = min($i_Start + 1 + $this->i_PerPage, $arr_Data['i_Results']);
            $arr_Data['str_SortTyp'] = $str_SortTyp;
            $arr_Data['str_SortBy']  = $str_SortBy;
            $arr_Data['str_Notice']  = $this->str_Notice;

            // Daten und Pagination
            $this->str_Display = 'actual';
            $this->setHeader();
            $this->setPagination($str_SortBy . '/' . $str_SortTyp, $arr_Data['i_Results']);
            $this->load->view('vortraege/actual.php', $arr_Data);
            $this->setFooter();
        } else {
            $this->index();
        }
    }

    /**
     *  Die Ansicht zum Ändern eines beestehenden Vortrag anzeigen
     *
     * @param integer $vortragId
     */
    public function edit($vortragId)
    {
        if (isset($vortragId) == true
            && is_int((int)$vortragId) == true
            && $this->b_LogedIn == true
        ) {
            $this->setHeader();
            $this->load->view('vortraege/edit.php', $this->loadSingleEntry($vortragId));
            $this->setFooter();
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Speichert die Änderungen an einem Vortrag
     */
    public function update()
    {
        if ($this->b_LogedIn == true) {
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');

            if ($this->form_validation->run('vortrag') == false) {
                $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                $this->edit($_POST['vortragID']);
            } else {
                // Belegung aktuallisieren
                $this->load->model('BelegungModel');

                $str_Date = $this->input->post('datum');
                $str_From = $this->input->post('beginnHour') . ':' . $this->input->post('beginnMinute') . ':00';
                $str_Till = $this->input->post('endeHour') . ':' . $this->input->post('endeMinute') . ':00';

                $arr_Belegung = array(
                    'datum'  => $str_Date,
                    'beginn' => $str_From,
                    'ende'   => $str_Till
                );
                if ($this->input->post('hoersaal') == 'on') {
                    $arr_Belegung['hoersaal'] = 1;
                } else {
                    $arr_Belegung['hoersaal'] = 0;
                }
                if ($this->input->post('ct') == 'on') {
                    $arr_Belegung['ct'] = 1;
                } else {
                    $arr_Belegung['ct'] = 0;
                }
                $this->BelegungModel->update($this->input->post('belegungID'), $arr_Belegung);

                // Vortrag aktualisieren
                $i_VortragID = $this->input->post('vortragID');
                $arr_Vortrag = $this->loadData();

                $this->VortraegeModel->update($i_VortragID, $arr_Vortrag);
                $this->str_Notice = 'Die &Auml;nderung wurde gespeichert';

                // Kalender ggf. aktuallisieren
                if ($arr_Vortrag['activ'] == 1) {
                    $this->load->library('GoogleCalendar');
                    $str_URL = $this->input->post('url');

                    if ($str_URL == '') {
                        // Neuen Eintrag anlegen
                        $arr_CalendarData = $this->getCalendarData($arr_Vortrag);
                        if (count($arr_CalendarData['activ']) == true) {
                            $str_URL = $this->googlecalendar->createEvent(
                                $arr_CalendarData['titel'],
                                $str_Date,
                                $str_From,
                                $str_Till,
                                $arr_CalendarData['ort']
                            );
                            $this->BelegungModel->update($this->input->post('belegungID'), array('url' => $str_URL));
                        }
                    } else {
                        $arr_CalendarData = $this->getCalendarData($arr_Vortrag);
                        if ($arr_CalendarData['activ'] == true) {
                            $this->googlecalendar->updateEvent(
                                $str_URL,
                                $arr_CalendarData['titel'],
                                $str_Date,
                                $str_From,
                                $str_Till,
                                $arr_CalendarData['ort']
                            );
                        }
                    }
                }

                // ggf. bestehenden Anhang löschen
                if ($this->input->post('attachmentDelete') == 'on') {
                    unlink('attachment/vortrag' . $i_VortragID . '.pdf');
                    $this->VortraegeModel->update($i_VortragID, array('attachment' => 0));
                }

                // ggf. Anhang hochladen
                $arr_Upload = array(
                    'file_name'     => 'vortrag' . $i_VortragID,
                    'upload_path'   => 'attachment/',
                    'allowed_types' => 'pdf',
                    'max_size'      => 4000,
                    'overwrite'     => true,
                    'remove_spaces' => true
                );
                $this->load->library('upload', $arr_Upload);

                if ($this->upload->do_upload('attachment') == true) {
                    $this->VortraegeModel->update($i_VortragID, array('attachment' => 1));
                } else {
                    // Fehler nur bei ausgewählter Datei
                    if ($this->upload->display_errors() !== '<p>You did not select a file to upload.</p>') {
                        $this->str_Notice = $this->upload->display_errors();
                    }
                }

                $this->actual();
            }
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Ändert den Status des angebenen Datensatzes
     *
     * @param integer $i_ID Datensatz ID
     * @param integer $i_Status Neuer Status (0 = deaktiviert, 1 = aktiviert)
     * @param string $str_Call Anzuzeigende Gesamtansicht. Standard: actual
     */
    public function activate($i_ID, $i_Status, $str_Call = 'actual')
    {
        if (is_int((int)$i_ID) == true
            && preg_match('/^(0|1)$/', $i_Status) == 1
        ) {
            $this->VortraegeModel->update($i_ID, array('activ' => $i_Status));

            if ($i_Status == 1) {
                // Vortag in den Kalender eintragen
                $this->load->library('GoogleCalendar');
                $arr_Vortrag = $this->VortraegeModel->getOne($i_ID);

                if ($arr_Vortrag['url'] == '') {
                    // Neuen Eintrag anlegen
                    $arr_CalendarData = $this->getCalendarData($arr_Vortrag);
                    if (count($arr_CalendarData['activ']) == true) {
                        $str_URL = $this->googlecalendar->createEvent(
                            $arr_CalendarData['titel'],
                            $arr_Vortrag['datum'],
                            $arr_Vortrag['beginn'],
                            $arr_Vortrag['ende'],
                            $arr_CalendarData['ort']
                        );
                        $this->BelegungModel->update($arr_Vortrag['belegungID'], array('url' => $str_URL));
                    }
                } else {
                    $arr_CalendarData = $this->getCalendarData($arr_Vortrag);
                    if ($arr_CalendarData['activ'] == true) {
                        $this->googlecalendar->updateEvent(
                            $arr_Vortrag['url'],
                            $arr_CalendarData['titel'],
                            $arr_Vortrag['datum'],
                            $arr_Vortrag['beginn'],
                            $arr_Vortrag['ende'],
                            $arr_CalendarData['ort']
                        );
                    }
                }

                $this->str_Notice = 'Der Vortrag wurde aktiviert';
            } else {
                $this->str_Notice = 'Der Vortrag wurde deaktiviert';
            }
        } else {
            $this->str_Notice = 'Es wurden fehlerhafte Angaben übergeben!';
        }

        if ($str_Call == 'actual') {
            $this->actual();
        } else {
            $this->all();
        }
    }

    /**
     *  Zeitgt die View zum Anlegen eines neuen Vortrages an
     */
    public function create()
    {
        if ($this->b_LogedIn == true) {
            $this->load->model('OrtModel');
            $this->load->model('VeranstalterModel');
            $this->load->model('StadtModel');
            $this->load->model('TypeModel');

            // Daten laden
            $arr_Data                     = array();
            $arr_Data['arr_Ort']          = $this->OrtModel->getAll();
            $arr_Data['arr_Veranstalter'] = $this->VeranstalterModel->getAll();
            $arr_Data['arr_Stadt']        = $this->StadtModel->getAll();
            $arr_Data['arr_Type']         = $this->TypeModel->getAll();

            // Views anzeigen
            $this->setHeader();
            $this->load->view('vortraege/create.php', $arr_Data);
            $this->setFooter();
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Speichert einen neuen Vortrag ab und trägt ihn ggf. im Kalender nach
     */
    public function save()
    {
        if ($this->b_LogedIn == true) {
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');

            if ($this->form_validation->run('vortrag') == false) {
                $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                $this->create();
            } else {
                // Vortrag anlegen
                $arr_Vortrag               = $this->loadData();
                $arr_Vortrag['attachment'] = 0;
                $i_VortragID               = $this->VortraegeModel->create($arr_Vortrag);

                if ($i_VortragID !== false) {
                    // Belegungs Eintrag erstellen
                    $this->load->model('BelegungModel');

                    $str_Date = $this->input->post('datum');
                    $str_From = $this->input->post('beginnHour') . ':' . $this->input->post('beginnMinute') . ':00';
                    $str_Till = $this->input->post('endeHour') . ':' . $this->input->post('endeMinute') . ':00';

                    // Kalender Eintrag ggf. erstellen
                    $str_URL          = '';
                    $arr_CalendarData = $this->getCalendarData($arr_Vortrag);


                    if ($arr_CalendarData['activ'] == true) {
                        $this->load->library('GoogleCalendar');
                        $str_URL = $this->googlecalendar->createEvent(
                            $arr_CalendarData['titel'],
                            $str_Date,
                            $str_From,
                            $str_Till,
                            $arr_CalendarData['ort']
                        );
                    }

                    $arr_Belegung = array(
                        'vortrag' => $i_VortragID,
                        'datum'   => $str_Date,
                        'beginn'  => $str_From,
                        'ende'    => $str_Till,
                        'url'     => $str_URL
                    );
                    if ($this->input->post('hoersaal') == 'on') {
                        $arr_Belegung['hoersaal'] = 1;
                    } else {
                        $arr_Belegung['hoersaal'] = 0;
                    }

                    if ($this->input->post('ct') == 'on') {
                        $arr_Belegung['ct'] = 1;
                    } else {
                        $arr_Belegung['ct'] = 0;
                    }

                    $this->BelegungModel->create($arr_Belegung);
                    $this->str_Notice = 'Der Vortrag wurde hinzugef&uuml;gt';

                    // ggf. Anhang hochladen
                    $arr_Upload = array(
                        'file_name'     => 'vortrag' . $i_VortragID,
                        'upload_path'   => 'attachment/',
                        'allowed_types' => 'pdf|doc|docx|odt|xps',
                        'max_size'      => 4000,
                        'overwrite'     => true,
                        'remove_spaces' => true
                    );
                    $this->load->library('upload', $arr_Upload);

                    if ($this->upload->do_upload('attachment') == true) {
                        $this->VortraegeModel->update($i_VortragID, array('attachment' => 1));
                    } else {
                        // Fehler nur bei ausgewählter Datei
                        if ($this->upload->display_errors() !== '<p>You did not select a file to upload.</p>') {
                            $this->str_Notice = $this->upload->display_errors();
                        }
                    }

                    // Übersicht anzeigen
                    $this->actual();
                } else {
                    $this->str_Notice = 'Der Vortrag konnte nicht gespeichert werden';
                    $this->create();
                }
            }
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Löscht einen Vortrag aus dem Belegungsplan, ggf. dem Kalender und der Vortragsliste
     *
     * @param integer $i_VortragID
     * @param integer $i_BelegungID
     */
    public function delete($i_VortragID, $i_BelegungID)
    {
        if (isset($i_VortragID) == true
            && is_integer((int)$i_VortragID) == true
            && isset($i_BelegungID) == true
            && is_integer((int)$i_BelegungID) == true
            && $this->b_LogedIn == true
        ) {
            // Kalender prüfen
            $this->load->model('BelegungModel');
            $arr_Belegung = $this->BelegungModel->getOne($i_BelegungID);

            if ($arr_Belegung['url'] != '') {
                $this->load->library('GoogleCalendar');
                $this->googlecalendar->deleteEvent($arr_Belegung['url']);
            }

            if ($this->VortraegeModel->delete($i_VortragID) == true
                && $this->BelegungModel->delete($i_BelegungID) == true
            ) {
                // ggf. bestehenden Anhang löschen
                if (file_exists('attachment/vortrag' . $i_VortragID . '.pdf') == true) {
                    unlink('attachment/vortrag' . $i_VortragID . '.pdf');
                }

                $this->str_Notice = 'Der Vortrag wurde gelöscht';
                $this->actual();
            } else {
                $this->str_Notice = 'Der Vortrag konnte nicht gelöscht werden!';
                $this->edit($i_VortragID);
            }
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Lädt die Ansicht zum erstellen eines individuellen PDFs
     */
    public function setPrintParameter()
    {
        if ($this->b_LogedIn == true) {
            $this->load->model('OrtModel');
            $this->load->model('VeranstalterModel');
            $this->load->model('StadtModel');
            $this->load->model('TypeModel');

            // Daten laden
            $arr_Data                     = array();
            $arr_Data['arr_Ort']          = $this->OrtModel->getAll();
            $arr_Data['arr_Veranstalter'] = $this->VeranstalterModel->getAll();
            $arr_Data['arr_Stadt']        = $this->StadtModel->getAll();
            $arr_Data['arr_Type']         = $this->TypeModel->getAll();

            // Views anzeigen
            $this->setHeader();
            $this->load->view('vortraege/print.php', $arr_Data);
            $this->setFooter();
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Erstellt die Liste mit den Vorträgen aus PDF unterberücksichtigung der Benutzer definierten Ausgabe Wünschen
     */
    public function printPDF()
    {
        if ($this->b_LogedIn == true) {
            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');

            if ($this->form_validation->run('print') == false) {
                $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                $this->setPrintParameter();
            } else {
                // PDF erstellen
                $this->load->library('pdf');
                $this->load->model('OrtModel');

                // Anzeige Parameter ermitteln
                $arr_Where   = array();
                $arr_WhereIn = array();
                $arr_Options = array('ort', 'stadt', 'veranstalter', 'type');

                foreach ($arr_Options as $str_Select) {
                    $mix_Value = $this->input->post($str_Select);

                    if (is_array($mix_Value) == true
                        && count($mix_Value) > 0
                    ) {
                        $i_Length = count($mix_Value);
                        for ($i = 0; $i < $i_Length; $i++) {
                            if ($mix_Value[$i] == '') {
                                unset($mix_Value[$i]);
                            } else {
                                $mix_Value[$i] = (int)$mix_Value[$i];
                            }
                        }
                        $mix_Value = array_values($mix_Value);

                        if (count($mix_Value) > 0) {
                            $arr_WhereIn['vortrag.' . $str_Select] = $mix_Value;
                        }
                    } else {
                        if ($mix_Value !== ''
                            && $mix_Value !== false
                            && is_int((int)$mix_Value) == true
                        ) {
                            $arr_Where['vortrag.' . $str_Select] = $mix_Value;
                        }
                    }
                }

                // Nur Kolloquien, nur Vorträge oder beides anzeigen
                if ($this->input->post('kolloquium') >= 0) {
                    $arr_Where['vortrag.kolloquium'] = $this->input->post('kolloquium');
                }

                // Marker ermittlen
                $b_Marker    = false;
                $arr_Marker  = array();
                $arr_Options = array(
                    'ort'          => 'id',
                    'stadt'        => 'stadt',
                    'veranstalter' => 'veranstalterID',
                    'type'         => 'type'
                );

                foreach ($arr_Options as $str_Select => $str_Compare) {
                    $mix_Value = $this->input->post('marker' . $str_Select);
                    if ($mix_Value !== ''
                        && is_int((int)$mix_Value) == true
                    ) {
                        $arr_Marker[$str_Compare] = $mix_Value;
                        $b_Marker                 = true;
                    }
                }

                // Daten laden
                $arr_Content = $this->VortraegeModel->getList($arr_Where, $arr_WhereIn);

                // ggf. Marker anwenden
                if ($b_Marker == true) {
                    $this->pdf->setMaker(true);
                    $i_LengthContent = count($arr_Content);
                    $i_MarkerCount   = count($arr_Marker);

                    for ($i = 0; $i < $i_LengthContent; $i++) {
                        $arr_Content[$i]['marker'] = false;
                        $i_Counter                 = 0;

                        foreach ($arr_Marker as $str_Marker => $mix_Value) {
                            if ($arr_Content[$i][$str_Marker] == $mix_Value) {
                                $i_Counter++;
                            }
                        }

                        // Nur markieren, wenn alle Einträge gleich sind
                        if ($i_Counter == $i_MarkerCount) {
                            $arr_Content[$i]['marker'] = true;
                        }
                    }
                }

                // Datums Anzeige setzen
                $this->pdf->setDisplay($this->input->post('display'));

                // Daten in PDF schreiben und ausgeben
                $this->pdf->setTitel($this->input->post('titel'), $this->input->post('subtitel'));
                $this->pdf->setDistance($this->input->post('distance'));
                $this->pdf->setContent($arr_Content);
                $this->pdf->setFooter($this->OrtModel->getAll());
                $this->pdf->stream();
            }
        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }

    }

    /**
     * Einen Vortrags Flyer zu dem angegeben Vortrag unter Verwendung der entsprechenden Vorlage erstellen.
     *
     * @param  integer $id
     */
    public function printFlyer($id)
    {
        if ($this->b_LogedIn == true) {
            // PDF und Type-Model nachladen
            $this->load->model('VeranstalterModel');
            $this->load->model('TypeModel');
            $this->load->library('flyer');

            // Daten laden
            $data = $this->VortraegeModel->getOne($id);

            // Fyler Art laden
            $organizer = $this->VeranstalterModel->getOne($data['veranstalter']);
            switch ($organizer['flyer']) {
                case '1':
                    // Flyer Klassische Archäologie
                    $this->load->library('FlyerAK');
                    $this->flyer->setFlyer(new FlyerAK());
                    break;
                case '2':
                    // Flyer Christliche Archäologie
                    $this->load->library('FlyerCA');
                    $this->flyer->setFlyer(new FlyerCA());
                    break;
                default:
                    // Flyer Klassische Archäologie
                    $this->load->library('FlyerAK');
                    $this->flyer->setFlyer(new FlyerAK());
                    break;
            }

            try {
                $this
                    ->flyer
                    ->setTyp($this->TypeModel->getType($data['type']))
                    ->setContent($data)
                    ->stream();
            } catch (ErrorException $error) {
                $this->str_Notice = $error->getMessage();
                $this->index();
            }

        } else {
            $this->str_Notice = 'Bitte zuerst anmelden';
            $this->index();
        }
    }

    /**
     *  Lädt die geladenen Daten in einen Array und gibt diesen zurück
     *
     * @return array
     */
    protected function loadData()
    {
        $arr_Data = array(
            'referent'     => $this->input->post('referent'),
            'herkunft'     => $this->input->post('herkunft'),
            'titel'        => $this->input->post('titel'),
            'ort'          => $this->input->post('ort'),
            'stadt'        => $this->input->post('stadt'),
            'veranstalter' => $this->input->post('veranstalter'),
            'type'         => $this->input->post('type'),
            'link'         => $this->input->post('link')
        );

        // Aktiv
        if ($this->input->post('activ') == 'on') {
            $arr_Data['activ'] = 1;
        } else {
            $arr_Data['activ'] = 0;
        }

        // Kolloquium
        if ($this->input->post('kolloquium') == 'on') {
            $arr_Data['kolloquium'] = 1;
        } else {
            $arr_Data['kolloquium'] = 0;
        }

        return $arr_Data;
    }

    /**
     *  Formatiert die Daten des Vortrages so um, dass diese im Kaledner gespeichert werden können
     *
     * @param array $arr_Data Zu formatierende Daten
     *
     * @return array
     */
    protected function getCalendarData($arr_Data)
    {
        $arr_Return = array('activ' => false);

        if ($arr_Data['activ'] == 1) {
            if ($arr_Data['titel'] != '') {
                // Referent (Herkunft), Titel
                $arr_Return['activ'] = true;
                $arr_Return['titel'] = '';

                if ($arr_Data['referent'] !== '') {
                    $arr_Return['titel'] = $arr_Data['referent'] . ' ';
                }

                if ($arr_Data['herkunft'] != '') {
                    $arr_Return['titel'] .= '(' . $arr_Data['herkunft'] . '), ';
                }
                $arr_Return['titel'] .= $arr_Data['titel'];

                //Ort
                $this->load->model('ortmodel');
                $arr_Ort           = $this->ortmodel->getOne($arr_Data['ort']);
                $arr_Return['ort'] = $arr_Ort['adresse'];

                // Veranstalter
                $this->load->model('veranstaltermodel');
                $arr_Veranstalter = $this->veranstaltermodel->getOne($arr_Data['veranstalter']);

                if ($arr_Veranstalter['veranstalterDisplay'] != '') {
                    $arr_Return['titel'] .= ' (' . $arr_Veranstalter['veranstalterDisplay'] . ')';
                }
            }
        }

        return $arr_Return;
    }

    /**
     *  Formatiert die übergeben Daten für die Ausgabe als RSS Feed um
     *
     * @param $arr_Data
     * @param bool $b_Content
     * @internal param Array $array mit den Daten jeweils ans Array
     * @return array
     */
    protected function getRSSData($arr_Data, $b_Content = false)
    {
        $this->load->helper('rss');

        return formatFeedContent($arr_Data, $b_Content);

        $arr_Return = array();
        $str_URL    = base_url() . 'vortraege/single/';

        foreach ($arr_Data as $arr_Element) {
            $arr_Row = array();

            $arr_Row['link']  = $str_URL . $arr_Element['vortragID'];
            $arr_Row['datum'] = $arr_Element['datum'] . 'T' . $arr_Element['beginn'] . '.00+02:00';

            // Leeren Referenten und Uni abfangen (für Kolloquium etc.)
            $arr_Row['titel'] = '';
            if ($arr_Element['referent'] !== '') {
                $arr_Row['titel'] = $arr_Element['referent'] . ' ';
            }
            if ($arr_Element['herkunft'] !== '') {
                $arr_Row['titel'] .= '(' . $arr_Element['herkunft'] . '), ';
            }
            if ($arr_Element['titel'] !== '') {
                $arr_Row['titel'] .= '"' . $arr_Element['titel'] . '"';
            }

            $arr_Time        = array(0 => ' s. t.', 1 => ' c. t.');
            $str_Time        = gTime($arr_Element['beginn']) . ' Uhr ' . $arr_Time[$arr_Element['ct']];
            $arr_Row['Body'] = array(
                array(
                    'Titel' => 'Datum',
                    'Value' => gDay($arr_Element['datum']) . ', ' . gDate($arr_Element['datum'])
                ),
                array(
                    'Titel' => 'Uhrzeit',
                    'Value' => $str_Time
                ),
                array(
                    'Titel' => 'Veranstalter',
                    'Value' => $arr_Element['veranstalterName']
                ),
                array(
                    'Titel' => 'Ort',
                    'Value' => $arr_Element['adresse']
                )
            );

            if ($arr_Element['attachment'] == 1) {
                $arr_Row['Body'][] = array(
                    'Titel' => 'Informationen',
                    'Value' => '<a href="' . base_url(
                        ) . 'attachment/vortrag' . $arr_Element['vortragID'] . '.pdf" target="_blank">Siehe Anhang</a>'
                );
            }

            if ($b_Content == true) {
                $arr_Row['Content'] = $arr_Row['titel'];

                if ($arr_Element['veranstalterDisplay'] !== '') {
                    $arr_Row['Content'] .= ', (' . $arr_Element['veranstalterDisplay'] . ')';
                }

                if ($arr_Element['ort'] !== '') {
                    $arr_Row['Content'] .= ', ' . $arr_Element['ort'];
                }

            }

            $arr_Return[] = $arr_Row;
        }

        return $arr_Return;
    }

    /**
     *  Header inkulsive Menü erstellen
     */
    protected function setHeader()
    {
        $this->load->view('layout/header.php');
        $arr_Data = array(
            'str_Activ'   => 'vortraege',
            'b_LogedIn'   => $this->b_LogedIn,
            'arr_SubMenu' => array(
                array(
                    'str_Target'  => 'liste',
                    'str_Class'   => 'all',
                    'str_Window'  => '',
                    'str_Display' => 'Alle',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'liste/1',
                    'str_Class'   => 'actual',
                    'str_Window'  => '',
                    'str_Display' => 'in Bonn',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'liste/2',
                    'str_Class'   => 'actual',
                    'str_Window'  => '',
                    'str_Display' => 'in Köln',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'download/1',
                    'str_Class'   => 'pdf',
                    'str_Window'  => '_blank',
                    'str_Display' => 'Download',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'download/0',
                    'str_Class'   => 'pdf',
                    'str_Window'  => '_blank',
                    'str_Display' => 'inkl. Kolloquien',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'https://www.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=1fju19jk735caqaravs83a9iek%40group.calendar.google.com&amp;color=%238C500B&amp;ctz=Europe%2FBerlin',
                    'str_Class'   => 'calendar',
                    'str_Window'  => '_blank',
                    'str_Display' => 'Kalender',
                    'b_Foreign'   => true,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'rss',
                    'str_Class'   => 'feed',
                    'str_Window'  => '_blank',
                    'str_Display' => 'RSS Feed',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'actual',
                    'str_Class'   => 'actual',
                    'str_Window'  => '',
                    'str_Display' => 'Aktuelle',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'all',
                    'str_Class'   => 'all',
                    'str_Window'  => '',
                    'str_Display' => 'Alle',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'create',
                    'str_Class'   => 'create',
                    'str_Window'  => '',
                    'str_Display' => 'Anlegen',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'setPrintParameter',
                    'str_Class'   => 'print',
                    'str_Window'  => '',
                    'str_Display' => 'Drucken',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                )
            )
        );

        $this->load->view('layout/navigation.php', $arr_Data);
    }

    /**
     *  Abschluss Zeile anzeigen
     */
    protected function setFooter()
    {
        $this->load->view('layout/footer.php', array('b_LogedIn' => $this->b_LogedIn));
    }

    /**
     *  Erzeugt den pagninieruns Effekt
     *
     * @param string $str_Sort Storierung
     * @param integer $i_Total Gesamt Anzahl an Einträgen
     */
    protected function setPagination($str_Sort, $i_Total)
    {
        // Ensure the pagination library is loaded
        $this->load->library('pagination');

        // This is messy. I'm not sure why the pagination class can't work
        // this out itself...
        $uri_segment = count(explode('/', '/vortraege/' . $this->str_Display . '/' . $str_Sort));

        // Initialise the pagination class, passing in some minimum parameters
        $this->pagination->initialize(
            array(
                'base_url'        => site_url('/vortraege/' . $this->str_Display . '/' . $str_Sort),
                'full_tag_open'   => '<ul id="pagination">',
                'full_tag_close'  => '</ul>',
                'first_link'      => 'Erster',
                'first_tag_open'  => '<li>',
                'first_tag_close' => '</li>',
                'last_link'       => 'Letzter',
                'last_tag_open'   => '<li>',
                'last_tag_close'  => '</li>',
                'cur_tag_open'    => '<li class="active">',
                'cur_tag_close'   => '</li>',
                'next_link'       => 'Weiter »',
                'next_tag_open'   => '<li class="next">',
                'next_tag_close'  => '</li>',
                'prev_link'       => '&lsaquo;&lsaquo; Zurück',
                'prev_tag_open'   => '<li class="previous">',
                'prev_tag_close'  => '</li>',
                'num_tag_open'    => '<li>',
                'num_tag_close'   => '</li>',
                'uri_segment'     => $uri_segment,
                'total_rows'      => $i_Total,
                'per_page'        => $this->i_PerPage
            )
        );
    }

    /**
     *  Lädt alle Daten die zur Bearbeitung und Anzeige eines Vortrages gebraucht werden
     *
     * @param  integer $i_ID
     * @return array
     */
    protected function loadSingleEntry($i_ID)
    {
        $this->load->model('OrtModel');
        $this->load->model('VeranstalterModel');
        $this->load->model('StadtModel');
        $this->load->model('TypeModel');

        // Daten laden
        $arr_Data                     = array('arr_Result' => $this->VortraegeModel->getOne($i_ID));
        $arr_Data['arr_Ort']          = $this->OrtModel->getAll();
        $arr_Data['arr_Veranstalter'] = $this->VeranstalterModel->getAll();
        $arr_Data['arr_Stadt']        = $this->StadtModel->getAll();
        $arr_Data['arr_Type']         = $this->TypeModel->getAll();

        return $arr_Data;
    }
}
