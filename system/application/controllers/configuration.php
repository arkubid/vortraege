<?php
/**
 *  Verwaltet die Liste der Benutzer, zugleich Grund Controller, der weiter abgeleitet wird
 *
 * @property    boolean             $b_LogedIn
 * @property    ConfigurationModel  $ConfigurationModel
 * @property    ConfigurationModel  $obj_Model
 * @property    Tank_auth           $tank_auth
 * @property    CI_Form_validation  $form_validation
 */
class Configuration extends Controller
{
    protected $b_LogedIn;
    protected $i_PerPage;
    protected $str_Notice;
    protected $obj_Model;
    protected $str_View;
    protected $arr_Notice;

    /**
     *  Konstrukor, der die Start Werte (Paginierung, View) etc. setzt und das Model lädt
     */
    public function Configuration()
    {
        parent::Controller();

        // Paginierung und Auth
        $this->i_PerPage = 50;
        $this->b_LogedIn = false;
        $this->load->library('tank_auth');
        $this->b_LogedIn = $this->tank_auth->is_logged_in();

        // Model laden
        $this->load->model('ConfigurationModel');
        $this->obj_Model = $this->ConfigurationModel;

        // View Verzeichnis
        $this->str_View = 'configuration';

        $this->arr_Notice = array(
            'activate'   => 'Der Benutzer Account wurde aktiviert',
            'deactivate' => 'Der Benutzer Account wurde deaktiviert',
            'delete'     => 'Der Benutzer Account wurde gelöscht',
            'reset'      => 'Dem Benutzer wurde ein Link zum erstellen eines neuen Passwort zugeschickt',
            'notReset'   => 'Dem Benutzer konnte kein neues Passwort zugewiesen werden!'
        );
    }

    /**
     *  Zeigt eine pagninierte Liste mit allen vorhandenen Vorträgen an
     *
     * @param string $str_SortBy Feld nachdem sortiert wird
     * @param string $str_SortTyp Sortier Art
     * @param integer $i_Start Beginn der Anzuzeigenden Einträge
     */
    public function index($str_SortBy = 'email', $str_SortTyp = 'ASC', $i_Start = 0)
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $arr_Data                = array();
            $arr_Data['arr_Result']  = $this->obj_Model->getAll($i_Start, $this->i_PerPage, $str_SortBy, $str_SortTyp);
            $arr_Data['i_Results']   = $this->obj_Model->getNumberOfRecords();
            $arr_Data['i_First']     = $i_Start + 1;
            $arr_Data['i_Last']      = min($i_Start + 1 + $this->i_PerPage, $arr_Data['i_Results']);
            $arr_Data['str_SortTyp'] = $str_SortTyp;
            $arr_Data['str_SortBy']  = $str_SortBy;
            $arr_Data['str_Notice']  = $this->str_Notice;
            $arr_Data                = array_merge($arr_Data, $this->loadDependingData());

            // Daten und Pagination
            $this->setHeader();
            $this->setPagination($str_SortBy . '/' . $str_SortTyp, $arr_Data['i_Results']);
            $this->load->view($this->str_View . '/index.php', $arr_Data);
            $this->setFooter();
        } else {
            redirect('/vortraege');
        }
    }

    /**
     *  Einen Eintrag über die ID aktivieren
     *
     * @param integer $userId
     */
    public function activate($userId)
    {
        if ($this->b_LogedIn == true
            && is_integer((int)$userId) == true
        ) {
            $this->obj_Model->update($userId, array('activated' => 1));
            $this->str_Notice = $this->arr_Notice['activate'];
            $this->index();
        } else {
            redirect('/vortraege');
        }
    }

    /**
     *  Eine Eintrag über die ID deaktivieren
     *
     * @param integer $i_ID
     */
    public function deactivate($i_ID)
    {
        if ($this->b_LogedIn == true
            && is_integer((int)$i_ID) == true
        ) {
            $this->obj_Model->update($i_ID, array('activated' => 0));
            $this->str_Notice = $this->arr_Notice['deactivate'];
            $this->index();
        } else {
            redirect('/vortraege');
        }
    }

    /**
     *  Eine E-Mail aus dem Verteiler löschen
     *
     * @param integer $i_ID
     */
    public function delete($i_ID)
    {
        if ($this->b_LogedIn == true
            && is_integer((int)$i_ID) == true
        ) {
            $this->obj_Model->delete($i_ID);
            $this->str_Notice = $this->arr_Notice['delete'];
            $this->index();
        } else {
            redirect('/vortraege');
        }
    }

    /**
     *  Maske zum erstellen eines neuen Eintrages anzeigen
     */
    public function create()
    {
        if ($this->b_LogedIn == true) {
            $arr_Data               = array();
            $arr_Data['str_Notice'] = $this->str_Notice;

            // Abhängige Daten laden
            $arr_Data = $this->loadDependingData();

            // View anzeigen
            $this->setHeader();
            $this->load->view($this->str_View . '/create.php', $arr_Data);
            $this->setFooter();
        } else {
            redirect('/vortraege');
        }
    }

    /**
     *  Legt einen neuen Benuter Account an
     */
    public function save()
    {
        if ($this->b_LogedIn == true) {
            $this->form_validation->set_rules(
                'username',
                'Benutzername',
                'trim|required|xss_clean|min_length['
                . $this->config->item('username_min_length', 'tank_auth')
                . ']|max_length[' . $this->config->item('username_max_length', 'tank_auth') . ']|alpha_dash'
            );
            $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|xss_clean|valid_email');
            $this->form_validation->set_rules(
                'password',
                'Passwort',
                'trim|required|xss_clean|min_length[' . $this->config->item(
                    'password_min_length',
                    'tank_auth'
                ) . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash'
            );
            $this->form_validation->set_rules(
                'confirm_password',
                'Passwort Wiederholung',
                'trim|required|xss_clean|matches[password]'
            );

            if ($this->form_validation->run()) {
                $arr_Data = $this->tank_auth->create_user(
                    $this->form_validation->set_value('username'),
                    $this->form_validation->set_value('email'),
                    $this->form_validation->set_value('password'),
                    false
                );

                if (is_array($arr_Data) == true) {
                    $this->str_Notice = 'Benutzer angelegt';
                    $this->index();
                } else {
                    $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                    $this->create();
                }
            } else {
                $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                $this->create();
            }
        } else {
            redirect('/vortraege');
        }
    }

    /**
     * Dem Benutzer einen Link zum erstellen eines neuen Passwortes schicken.
     * @param $userId
     */
    public function resetPassword($userId)
    {
        if ($this->b_LogedIn == true) {
            $user = $this->obj_Model->getOne($userId);
            if ($user !== null) {
                // Password Token erzeugen
                $data = $this->tank_auth->forgot_password($user['username']);

                // E-Mail verschicken
                $link = base_url() . 'configuration/requestNewPassword/' . $data['user_id'] . '/' . $data['new_pass_key'];
                $this->load->library('email');
                $this->email->clear(true);
                $this->email->from('archinst@uni-bonn.de', 'Archäologische Vorträge in Bonn und Köln');
                $this->email->bcc($data['email']);
                $this->email->subject('Neues Passwort für die archäologischen Vorträge in Bonn und Köln');
                $this->email->message(
                    'Hallo,<br><br>um ein neues Passwort für die Vortragsliste der archäologischen Vorträge in Bonn und Köln zu erhalten klicken Sie bitte binnen 7 Tagen auf folgenden Link:'
                    . '<br><br><a href="' . $link . '">' . $link . '</a><br><br>'
                    . 'Sollte Sie kein neues Passwort benötigen, ignorieren Sie bitte diese E-Mail.'
                    . '<br><br>Mit freundlichen Grüßen,'
                    . '<br>Rheinische Friedrich-Wilhelms-Universität Bonn<br>Institut für Archäologie und Kulturanthropologie<br>Abteilung Klassische Archäologie'
                );

                if ($this->email->send() == true) {
                    $this->str_Notice = $this->arr_Notice['reset'];
                } else {
                    $this->str_Notice = $this->arr_Notice['notReset'];
                }

                $this->index();
            }
        }
    }

    /**
     * Eingabe Maske für ein neues Passwort anzeigen.
     * @param $userId
     * @param $token
     */
    public function requestNewPassword($userId, $token)
    {
        if ($this->tank_auth->can_reset_password($userId, $token)) {
            $this->load->view('layout/header.php');
            $this->load->view(
                'layout/navigation.php',
                array(
                    'str_Activ'   => 'vortraege',
                    'b_LogedIn'   => false,
                    'arr_SubMenu' => array(
                        array(
                            'str_Target'  => 'liste',
                            'str_Class'   => 'all',
                            'str_Window'  => '',
                            'str_Display' => 'Alle',
                            'b_Foreign'   => false,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'liste/1',
                            'str_Class'   => 'actual',
                            'str_Window'  => '',
                            'str_Display' => 'in Bonn',
                            'b_Foreign'   => false,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'liste/2',
                            'str_Class'   => 'actual',
                            'str_Window'  => '',
                            'str_Display' => 'in Köln',
                            'b_Foreign'   => false,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'download/1',
                            'str_Class'   => 'pdf',
                            'str_Window'  => '_blank',
                            'str_Display' => 'Download',
                            'b_Foreign'   => false,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'download/0',
                            'str_Class'   => 'pdf',
                            'str_Window'  => '_blank',
                            'str_Display' => 'inkl. Kolloquien',
                            'b_Foreign'   => false,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'http://www.google.com/calendar/hosted/ai.no-ip.org/embed?src=ai.no-ip.org_ck48gp2d64654ej3h7fcbqgb0k@group.calendar.google.com',
                            'str_Class'   => 'calendar',
                            'str_Window'  => '_blank',
                            'str_Display' => 'Kalender',
                            'b_Foreign'   => true,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'rss',
                            'str_Class'   => 'feed',
                            'str_Window'  => '_blank',
                            'str_Display' => 'RSS Feed',
                            'b_Foreign'   => false,
                            'b_Login'     => false
                        ),
                        array(
                            'str_Target'  => 'actual',
                            'str_Class'   => 'actual',
                            'str_Window'  => '',
                            'str_Display' => 'Aktuelle',
                            'b_Foreign'   => false,
                            'b_Login'     => true
                        ),
                        array(
                            'str_Target'  => 'all',
                            'str_Class'   => 'all',
                            'str_Window'  => '',
                            'str_Display' => 'Alle',
                            'b_Foreign'   => false,
                            'b_Login'     => true
                        ),
                        array(
                            'str_Target'  => 'create',
                            'str_Class'   => 'create',
                            'str_Window'  => '',
                            'str_Display' => 'Anlegen',
                            'b_Foreign'   => false,
                            'b_Login'     => true
                        ),
                        array(
                            'str_Target'  => 'setPrintParameter',
                            'str_Class'   => 'print',
                            'str_Window'  => '',
                            'str_Display' => 'Drucken',
                            'b_Foreign'   => false,
                            'b_Login'     => true
                        )
                    )
                )
            );
            $this->load->view(
                $this->str_View . '/reset.php',
                array(
                    'userId' => $userId,
                    'token'  => $token,
                    'notice' => $this->str_Notice
                )
            );
            $this->setFooter();
        } else {
            redirect('/vortraege');
        }
    }

    /**
     * Neues Passwort für den Benutzer anlegen.
     */
    public function setNewPassword()
    {
        $this->form_validation->set_rules(
            'password',
            'Passwort',
            'trim|required|xss_clean|min_length[' . $this->config->item(
                'password_min_length',
                'tank_auth'
            ) . ']|max_length[' . $this->config->item('password_max_length', 'tank_auth') . ']|alpha_dash'
        );

        if ($this->form_validation->run()) {
            if ($this->tank_auth->reset_password($this->input->post('userId'), $this->input->post('token'), $this->input->post('password'))) {
                $this->str_Notice = 'Das Passwort wurde geändert!';
                redirect('/auth/login');
            } else {
                $this->str_Notice = $this->tank_auth->get_error_message();
                $this->requestNewPassword($this->input->post('userId'), $this->input->post('token'));
            }
        } else {
            $this->str_Notice = $this->form_validation->error_string;
            $this->requestNewPassword($this->input->post('userId'), $this->input->post('token'));
        }
    }

    /**
     *  Abhängigen Daten, d.h. per ID verknüpfte Felder in anderen Tabelle laden
     *
     * @return array
     */
    protected function loadDependingData()
    {
        return array();
    }

    /**
     *  Header inkulsive Menü erstellen
     */
    protected function setHeader()
    {
        $this->load->view('layout/header.php');
        $arr_Data = array(
            'str_Activ'   => 'configuration',
            'b_LogedIn'   => $this->b_LogedIn,
            'arr_SubMenu' => array(
                array(
                    'str_Target'  => 'index',
                    'str_Class'   => 'alluser',
                    'str_Window'  => '',
                    'str_Display' => 'Alle Benutzer',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'create',
                    'str_Class'   => 'adduser',
                    'str_Window'  => '',
                    'str_Display' => 'Neuer Benutzer',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => '../veranstalter/index',
                    'str_Class'   => 'veranstalter',
                    'str_Window'  => '',
                    'str_Display' => 'Alle Veranstalter',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => '../veranstalter/create',
                    'str_Class'   => 'veranstalter-add',
                    'str_Window'  => '',
                    'str_Display' => 'Neuer Veranstalter',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => '../ort/index',
                    'str_Class'   => 'ort',
                    'str_Window'  => '',
                    'str_Display' => 'Alle Orte',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => '../ort/create',
                    'str_Class'   => 'ort-add',
                    'str_Window'  => '',
                    'str_Display' => 'Neuer Ort',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => '../feed/index',
                    'str_Class'   => 'feed',
                    'str_Window'  => '',
                    'str_Display' => 'Feed',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => '../feed/create',
                    'str_Class'   => 'feed-add',
                    'str_Window'  => '',
                    'str_Display' => 'Neuer Feed',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                )
            )
        );

        $this->load->view('layout/navigation.php', $arr_Data);
    }

    /**
     *  Abschluss Zeile anzeigen
     */
    protected function setFooter()
    {
        $this->load->view('layout/footer.php', array('b_LogedIn' => $this->b_LogedIn));
    }

    /**
     *  Erzeugt den pagninieruns Effekt
     *
     * @param string $str_Sort Storierung
     * @param integer $i_Total Gesamt Anzahl an Einträgen
     */
    protected function setPagination($str_Sort, $i_Total)
    {
        // Ensure the pagination library is loaded
        $this->load->library('pagination');

        // This is messy. I'm not sure why the pagination class can't work
        // this out itself...
        $uri_segment = count(explode('/', '/' . $this->str_View . '/all/' . $str_Sort));

        // Initialise the pagination class, passing in some minimum parameters
        $this->pagination->initialize(
            array(
                'base_url'        => site_url('/' . $this->str_View . '/all/' . $str_Sort),
                'full_tag_open'   => '<ul id="pagination">',
                'full_tag_close'  => '</ul>',
                'first_link'      => 'Erster',
                'first_tag_open'  => '<li>',
                'first_tag_close' => '</li>',
                'last_link'       => 'Letzter',
                'last_tag_open'   => '<li>',
                'last_tag_close'  => '</li>',
                'cur_tag_open'    => '<li class="active">',
                'cur_tag_close'   => '</li>',
                'next_link'       => 'Weiter »',
                'next_tag_open'   => '<li class="next">',
                'next_tag_close'  => '</li>',
                'prev_link'       => '&lsaquo;&lsaquo; Zurück',
                'prev_tag_open'   => '<li class="previous">',
                'prev_tag_close'  => '</li>',
                'num_tag_open'    => '<li>',
                'num_tag_close'   => '</li>',
                'uri_segment'     => $uri_segment,
                'total_rows'      => $i_Total,
                'per_page'        => $this->i_PerPage
            )
        );
    }
}
