<?php
/**
 *
 * @property boolean            $b_LogedIn
 * @property NewsletterModel    $NewsletterModel
 * @property CaptchaModel       $CaptchaModel
 * @property CI_Email           $email
 */
class Newsletter extends Controller
{
    protected $b_LogedIn;
    protected $i_PerPage;
    protected $str_Notice;
    protected $str_Duration;
    protected $str_Mail;

    public function Newsletter()
    {
        parent::Controller();

        // Paginierung und Auth
        $this->i_PerPage = 50;
        $this->b_LogedIn = false;
        $this->load->library('tank_auth');
        $this->b_LogedIn    = $this->tank_auth->is_logged_in();
        $this->str_Duration = 7200;
        $this->str_Mail     = 'archinst@uni-bonn.de';

        // Model laden
        $this->load->model('NewsletterModel');
    }

    /**
     *  Zeigt die Liste mit den aktuellen Vorträgen an
     */
    public function index()
    {
        $arr_Data               = array();
        $arr_Data['str_Notice'] = $this->str_Notice;
        $arr_Data['str_Image']  = $this->createCaptcha();

        $this->setHeader();
        $this->load->view('newsletter/register.php', $arr_Data);
        $this->setFooter();
    }

    /**
     *  Fügt einen neuen Benutzer der Mailing Liste unter vorbehalt hinzu
     */
    public function subscribe()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if ($this->form_validation->run('newsletter') == false) {
            $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
            $this->index();
        } else {
            $this->load->model('NewsletterModel');
            $arr_Data = array(
                'mail'     => $this->input->post('email'),
                'name'     => '',
                'password' => random_string('unique'),
                'activ'    => 0
            );

            if ($this->NewsletterModel->create($arr_Data) == true) {
                // Mail verschicken
                $this->load->library('email');
                $this->email->from($this->str_Mail, 'Archaeologische Vortraege in Bonn und Koeln');

                $arr_Data = $this->NewsletterModel->getOne($arr_Data['mail']);
                $this->email->to($arr_Data['mail']);

                $this->email->subject('Archäologische Vorträge Newsletter Bestätigung');
                $this->email->message(
                    'Sehr geehrter Benutzer,<br><br>willkommen bei dem Newsletter zu den '
                    . 'Archäologischen Vorträgen in Bonn und Köln. Klicken sie auf diesen Link, um ihre '
                    . 'E-Mail Adresse zu bestätigen<br><br>{unwrap}<a href="' . base_url()
                    . 'newsletter/confirm/' . $arr_Data['newsletterID'] . '/' . $arr_Data['password'] . '">'
                    . base_url() . 'newsletter/confirm/' . $arr_Data['newsletterID'] . '/' . $arr_Data['password']
                    . '</a>{/unwrap}<br><br>Haben Sie diese E-Mail fälschlicherweise erhalten, hat'
                    . ' wahrscheinlich ein Nutzer Ihre E-Mail-Adresse eingegeben, als er eine andere'
                    . ' E-Mail-Adresse anmelden wollte. Wir bitten dies zu entschuldige. Ihre E-Mail '
                    . ' Adresse wird nur aufgenommen, wenn Sie auf den Bestätigungslink klicken.'
                    . '<br><br>Mit freundlichen Grüßen,<br>Universität Bonn, Abteilung Klassiche Archäologie'
                );
                $this->email->send();

                // View laden
                $this->setHeader();
                $this->load->view('newsletter/subscribe.php');
                $this->setFooter();
            } else {
                $this->str_Notice = 'Diese E-Mail Adresse erhält bereits den Newsletter';
                $this->index();
            }
        }
    }

    /**
     *  Anmeldung zu Newsletter bestätigen
     *
     * @param integer $i_NewsletterID Newsletter ID
     * @param string $str_Password Passwort
     */
    public function confirm($i_NewsletterID, $str_Password)
    {
        $arr_Data  = array('b_Result' => false);
        $arr_Daten = $this->NewsletterModel->get(
            array(
                'newsletterID' => $i_NewsletterID,
                'password'     => $str_Password,
                'activ'        => 0
            )
        );

        if (count($arr_Daten) > 0) {
            $this->NewsletterModel->update($i_NewsletterID, array('password' => '', 'activ' => 1));
            $this->str_Notice     = 'Ihre E-Mail Adresse wurde dem Newsletter hinzugefügt!';
            $arr_Data['b_Result'] = true;
        } else {
            $this->str_Notice = 'Ihre Anmeldung konnte nicht bestätigt werden!';
        }

        $arr_Data['str_Notice'] = $this->str_Notice;
        $this->setHeader();
        $this->load->view('newsletter/confirm.php', $arr_Data);
        $this->setFooter();
    }

    /**
     *  Zeigt die Ansicht zum Abmelden vom Newsletter an
     */
    public function remove()
    {
        $arr_Data               = array();
        $arr_Data['str_Notice'] = $this->str_Notice;
        $arr_Data['str_Image']  = $this->createCaptcha();

        $this->setHeader();
        $this->load->view('newsletter/remove.php', $arr_Data);
        $this->setFooter();
    }

    /**
     *  Benutzer per E-Mail über die Entfernung von der Newsletter Liste informieren
     */
    public function unsubscribe()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if ($this->form_validation->run('newsletter') == false) {
            $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
            $this->remove();
        } else {
            $this->load->model('NewsletterModel');
            $arr_ID = $this->NewsletterModel->getOne($this->input->post('email'));

            if (count($arr_ID) > 0) {
                $arr_Data = array('password' => random_string('unique'), 'activ' => 0);

                if ($this->NewsletterModel->update($arr_ID['newsletterID'], $arr_Data) == true) {
                    // Mail verschicken
                    $this->load->library('email');
                    $this->email->from($this->str_Mail, 'Archaeologische Vortraege in Bonn und Koeln');

                    $arr_Data = $this->NewsletterModel->getOne($this->input->post('email'));
                    $this->email->to($arr_Data['mail']);

                    $this->email->subject('Archäologische Vorträge Newsletter Abmeldung');
                    $this->email->message(
                        'Sehr geehrter Benutzer,<br><br>um sich bei dem Newsletter zu den '
                        . 'Archäologische Vorträgen in Bonn und Köln abzumelden, klicken sie auf '
                        . 'diesen Link, um ihre Abmeldung zu bestätigen<br><br>{unwrap}'
                        . '<a href="' . base_url() . '/newsletter/confirmunsubscribe/'
                        . $arr_Data['newsletterID'] . '/' . $arr_Data['password'] . '">'
                        . base_url() . 'newsletter/confirmunsubscribe/' . $arr_Data['newsletterID']
                        . '/' . $arr_Data['password']
                        . '</a>{/unwrap}<br><br>Haben Sie diese E-Mail fälschlicherweise erhalten, hat'
                        . ' wahrscheinlich ein Nutzer Ihre E-Mail-Adresse '
                        . ' eingegeben, als er eine andere E-Mail-Adresse abmelden wollte.'
                        . ' Wir bitten dies zu entschuldige. Ihre E-Mail Adresse wird nur entfernt, '
                        . 'wenn Sie auf den Bestätigungslink klicken.<br><br>Mit freundlichen Grüßen,'
                        . '<br><br>Abteilung Klassiche Archäologie der Universität Bonn'
                    );
                    $this->email->send();

                    // View laden
                    $this->setHeader();
                    $this->load->view('newsletter/unsubscribe.php');
                    $this->setFooter();
                } else {
                    $this->str_Notice = 'Diese E-Mail Adresse erhält den Newsletter nicht';
                    $this->remove();
                }
            } else {
                $this->str_Notice = 'Diese E-Mail Adresse erhält den Newsletter nicht';
                $this->remove();
            }
        }
    }

    /**
     *  Benutzer vom Newsletter abmelden
     *
     * @param <type> $i_NewsletterID
     * @param <type> $str_Password
     */
    public function confirmunsubscribe($i_NewsletterID, $str_Password)
    {
        $arr_Data  = array('b_Result' => false);
        $arr_Daten = $this->NewsletterModel->get(
            array(
                'newsletterID' => $i_NewsletterID,
                'password'     => $str_Password,
                'activ'        => 0
            )
        );

        if (count($arr_Daten) > 0) {
            $this->NewsletterModel->delete($i_NewsletterID);
            $this->str_Notice     = 'Ihre E-Mail Adresse wurde entfernt!';
            $arr_Data['b_Result'] = true;
        } else {
            $this->str_Notice = 'Ihre Abmeldung konnte nicht bestätigt werden!';
        }

        $arr_Data['str_Notice'] = $this->str_Notice;
        $this->setHeader();
        $this->load->view('newsletter/confirm.php', $arr_Data);
        $this->setFooter();
    }

    /**
     *  View zum Benutzer hinzufügen anzeigen
     */
    public function create()
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $this->setHeader();
            $this->load->view('newsletter/create.php', array('str_Notice' => $this->str_Notice));
            $this->setFooter();
        } else {
            $this->index();
        }
    }

    /**
     *  Trägt einen neuen Empfänger in die Newsletter Liste ein
     */
    public function createreciever()
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|xss_clean|valid_email');

            if ($this->form_validation->run()) {
                $str_Mail   = $this->input->post('email');
                $arr_Result = $this->NewsletterModel->getOne($str_Mail);

                if (count($arr_Result) == 0) {
                    $this->NewsletterModel->create(array('mail' => $str_Mail, 'activ' => 1));

                    $this->str_Notice = 'Die E-Mail wurde in den Verteiler mitaufgenommen';
                    $this->all();
                } else {
                    $this->str_Notice = 'Die E-Mail ist bereits vorhanden';
                    $this->create();
                }
            } else {
                $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                $this->create();
            }
        } else {
            $this->index();
        }
    }

    /**
     *  View zum Newsletter verschicken anzeigen
     */
    public function send()
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $this->setHeader();
            $this->load->view('newsletter/send.php', array('str_Notice' => $this->str_Notice));
            $this->setFooter();
        } else {
            $this->index();
        }
    }

    /**
     *  Verschickt den Newsletter an alle Empfänger
     */
    public function sendnewsletter()
    {
        if ($this->b_LogedIn == true) {
            set_time_limit(0);
            // PDF erstellen
            $this->load->library('pdf');
            $this->load->model('VortraegeModel');
            $this->load->model('OrtModel');
            $this->load->library('email');

            $this->pdf->setTitel('Archäologische Vorträge in Bonn und Köln');
            $this->pdf->setContent($this->VortraegeModel->getList(array('kolloquium' => 0)));
            $this->pdf->setFooter($this->OrtModel->getAll());
            $pdf = $this->pdf->save();

            $receiver = $this->NewsletterModel->getAllMails();
            $this->email->clear(true);
            $this->email->from($this->str_Mail, 'Archaeologische Vortraege in Bonn und Koeln');
            $this->email->bcc($receiver);
            $this->email->subject('Archäologische Vorträge in Bonn und Köln');
            $this->email->message(
                'Sehr geehrte Damen und Herren,<br><br>anbei erhalten sie die aktuelle Liste mit den '
                . ' archäologischen Vorträgen in Bonn und Köln.<br><br>Mit freundlichen Grüßen,'
                . '<br>Rheinische Friedrich-Wilhelms-Universität Bonn<br>Institut für Archäologie und Kulturanthropologie<br>Abteilung Klassische Archäologie'
                . '<br><br><br>Sollten Sie diesen Newsletter in Zukunft nicht mehr erhalten wollen, '
                . 'klicken Sie bitte auf folgenden Link:<br>'
                . '<a href="' . base_url() . 'newsletter/remove">' . base_url() . 'newsletter/remove</a>'
            );
            $this->email->attach($pdf);

            if ($this->email->send() == true) {
                $this->str_Notice = 'Der Newsletter wurde verschickt!';
            } else {
                $this->str_Notice = 'Fehler! Der Newsletter wurde nicht verschickt!';
                //$this->str_Notice = $this->email->print_debugger();
            }

            unlink($pdf);
            $this->send();
        } else {
            $this->index();
        }
    }

    /**
     *  Zeigt eine pagninierte Liste mit allen vorhandenen Vorträgen an
     *
     * @param string $str_SortBy Feld nachdem sortiert wird
     * @param string $str_SortTyp Sortier Art
     * @param integer $i_Start Beginn der Anzuzeigenden Einträge
     */
    public function all($str_SortBy = 'mail', $str_SortTyp = 'ASC', $i_Start = 0)
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $arr_Data                = array();
            $arr_Data['arr_Result']  = $this->NewsletterModel->getAll(
                $i_Start,
                $this->i_PerPage,
                $str_SortBy,
                $str_SortTyp
            );
            $arr_Data['i_Results']   = $this->NewsletterModel->getNumberOfRecords();
            $arr_Data['i_First']     = $i_Start + 1;
            $arr_Data['i_Last']      = min($i_Start + 1 + $this->i_PerPage, $arr_Data['i_Results']);
            $arr_Data['str_SortTyp'] = $str_SortTyp;
            $arr_Data['str_SortBy']  = $str_SortBy;
            $arr_Data['str_Notice']  = $this->str_Notice;

            // Daten und Pagination
            $this->setHeader();
            $this->setPagination($str_SortBy . '/' . $str_SortTyp, $arr_Data['i_Results']);
            $this->load->view('newsletter/all.php', $arr_Data);
            $this->setFooter();
        } else {
            $this->index();
        }
    }

    /**
     *  Eine E-Mail aktivieren
     *
     * @param integer $i_ID
     */
    public function activate($i_ID)
    {
        if ($this->b_LogedIn == true
            && is_integer((int)$i_ID) == true
        ) {
            $this->NewsletterModel->update($i_ID, array('activ' => 1));
            $this->str_Notice = 'Die E-Mail Adresse wurde aktiviert';
            $this->all();
        } else {
            $this->index();
        }
    }

    /**
     *  Eine E-Mail deaktivieren
     *
     * @param integer $i_ID
     */
    public function deactivate($i_ID)
    {
        if ($this->b_LogedIn == true
            && is_integer((int)$i_ID) == true
        ) {
            $this->NewsletterModel->update($i_ID, array('activ' => 0));
            $this->str_Notice = 'Die E-Mail Adresse wurde deaktiviert';
            $this->all();
        } else {
            $this->index();
        }
    }

    /**
     *  Eine E-Mail aus dem Verteiler löschen
     *
     * @param integer $i_ID
     */
    public function delete($i_ID)
    {
        if ($this->b_LogedIn == true
            && is_integer((int)$i_ID) == true
        ) {
            $this->NewsletterModel->delete($i_ID);
            $this->str_Notice = 'Die E-Mail Adresse wurde gelöscht';
            $this->all();
        } else {
            $this->index();
        }
    }

    /**
     *  Zeigt die Maske zum verschicken eine Rundmail mit beliebigem Text und ggf. Anhang an
     */
    public function single()
    {
        if ($this->b_LogedIn == true) {
            $arr_Data               = array();
            $arr_Data['str_Notice'] = $this->str_Notice;
            $arr_Data['str_Image']  = $this->createCaptcha();

            // View anzeigen
            $this->setHeader();
            $this->load->view('newsletter/single.php', $arr_Data);
            $this->setFooter();
        } else {
            $this->index();
        }
    }

    /**
     *  Verschickt eine E-Mail mit dem eingeben Text an alle Empfänger des Newsletters
     */
    public function singlemail()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        if ($this->b_LogedIn == false) {
            $this->index();
        }

        if ($this->form_validation->run('sendmail') == false) {
            $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
            $this->single();
        } else {
            $this->load->library('email');
            $b_Error     = false;
            $str_Topic   = $this->input->post('topic');
            $str_Content = nl2br($this->input->post('content'));
            $str_Content .= '<br><br><br>Sollten Sie diesen Newsletter in Zukunft nicht mehr erhalten wollen, '
                . 'klicken Sie bitte auf folgenden Link:<br>'
                . '<a href="' . base_url() . 'newsletter/remove">' . base_url() . 'newsletter/remove</a>';

            $arr_Newsletter = $this->NewsletterModel->getAllMails();

            $this->email->clear(true);
            $this->email->from($this->str_Mail, 'Archaeologische Vortraege in Bonn und Koeln');
            $this->email->bcc($arr_Newsletter);
            $this->email->subject($str_Topic);
            $this->email->message($str_Content);

            // ggf. Anhang hochladen
            $b_Attachment = false;
            $arr_Upload   = array(
                'file_name'     => 'Anhang',
                'upload_path'   => 'attachment/',
                'allowed_types' => 'pdf',
                'max_size'      => 4000,
                'overwrite'     => true,
                'remove_spaces' => true
            );
            $this->load->library('upload', $arr_Upload);

            if ($this->upload->do_upload('attachment') == true) {
                $this->email->attach('attachment/Anhang.pdf');
                $b_Attachment = true;
            } else {
                // Fehler nur bei ausgewählter Datei
                if ($this->upload->display_errors() !== '<p>You did not select a file to upload.</p>') {
                    $this->str_Notice = $this->upload->display_errors();
                    $b_Error          = true;
                }
            }

            if ($b_Error == false) {
                // Mail verschicken
                $this->email->send();
                $this->str_Notice = 'Die E-Mail wurde verschickt!';

                // ggf. Anhang entfernen
                if ($b_Attachment == true) {
                    unlink('attachment/Anhang.pdf');
                }
            }

            // Anzeige
            $this->single();
        }
    }

    /**
     *  Prüft, ob das Captcha richtig ausgefüllt wurde
     *
     * @param string $str_Word
     *
     * @return boolean
     */
    public function checkCaptcha($str_Word)
    {
        $b_Return = false;
        $this->load->model('CaptchaModel');
        $this->CaptchaModel->deleteOld($this->str_Duration);

        if ($this->CaptchaModel->getCaptcha(
                $this->input->post('captcha'),
                $this->input->ip_address(),
                $this->str_Duration
            ) == true
        ) {
            $b_Return = true;
        } else {
            $this->form_validation->set_message(
                'checkCaptcha',
                'Der Wert von Wort muss mit dem Text im Bild übereinstimmen'
            );
        }

        return $b_Return;
    }

    /**
     *  Erzeugt ein Captcha Bild und gibt den Pfad zu dem Bild zurück
     *
     * @return string
     */
    protected function createCaptcha()
    {
        // Captcha erzeugen und in Datenbank speichern
        $this->load->model('CaptchaModel');
        $this->load->plugin('captcha');
        $arr_Options = array(
            'word'       => '',
            'img_path'   => './captcha/',
            'img_url'    => base_url() . 'captcha/',
            'img_width'  => '150',
            'img_height' => 30,
            'expiration' => $this->str_Duration
        );
        $arr_Captcha = create_captcha($arr_Options);

        $this->CaptchaModel->create(
            array(
                'zeit'      => $arr_Captcha['time'],
                'ipadresse' => $this->input->ip_address(),
                'word'      => $arr_Captcha['word']
            )
        );

        return $arr_Captcha['image'];
    }

    /**
     *  Header inkulsive Menü erstellen
     */
    protected function setHeader()
    {
        $this->load->view('layout/header.php');
        $arr_Data = array(
            'str_Activ'   => 'newsletter',
            'b_LogedIn'   => $this->b_LogedIn,
            'arr_SubMenu' => array(
                array(
                    'str_Target'  => 'index',
                    'str_Class'   => 'create',
                    'str_Window'  => '',
                    'str_Display' => 'Anmelden',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'remove',
                    'str_Class'   => 'delete',
                    'str_Window'  => '',
                    'str_Display' => 'Abmelden',
                    'b_Foreign'   => false,
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'all',
                    'str_Class'   => 'alluser',
                    'str_Window'  => '',
                    'str_Display' => 'Alle Empf&auml;nger',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'create',
                    'str_Class'   => 'adduser',
                    'str_Window'  => '',
                    'str_Display' => 'Neuer Empf&auml;nger',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'send',
                    'str_Class'   => 'newsletter',
                    'str_Window'  => '',
                    'str_Display' => 'Vortragsliste verschicken',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'single',
                    'str_Class'   => 'mail',
                    'str_Window'  => '',
                    'str_Display' => 'E-Mail schreiben',
                    'b_Foreign'   => false,
                    'b_Login'     => true
                )
            )
        );

        $this->load->view('layout/navigation.php', $arr_Data);
    }

    /**
     *  Abschluss Zeile anzeigen
     */
    protected function setFooter()
    {
        $this->load->view('layout/footer.php', array('b_LogedIn' => $this->b_LogedIn));
    }

    /**
     *  Erzeugt den pagninieruns Effekt
     *
     * @param string $str_Sort Storierung
     * @param integer $i_Total Gesamt Anzahl an Einträgen
     */
    protected function setPagination($str_Sort, $i_Total)
    {
        // Ensure the pagination library is loaded
        $this->load->library('pagination');

        // This is messy. I'm not sure why the pagination class can't work
        // this out itself...
        $uri_segment = count(explode('/', '/newsletter/all/' . $str_Sort));

        // Initialise the pagination class, passing in some minimum parameters
        $this->pagination->initialize(
            array(
                'base_url'        => site_url('/newsletter/all/' . $str_Sort),
                'full_tag_open'   => '<ul id="pagination">',
                'full_tag_close'  => '</ul>',
                'first_link'      => 'Erster',
                'first_tag_open'  => '<li>',
                'first_tag_close' => '</li>',
                'last_link'       => 'Letzter',
                'last_tag_open'   => '<li>',
                'last_tag_close'  => '</li>',
                'cur_tag_open'    => '<li class="active">',
                'cur_tag_close'   => '</li>',
                'next_link'       => 'Weiter »',
                'next_tag_open'   => '<li class="next">',
                'next_tag_close'  => '</li>',
                'prev_link'       => '&lsaquo;&lsaquo; Zurück',
                'prev_tag_open'   => '<li class="previous">',
                'prev_tag_close'  => '</li>',
                'num_tag_open'    => '<li>',
                'num_tag_close'   => '</li>',
                'uri_segment'     => $uri_segment,
                'total_rows'      => $i_Total,
                'per_page'        => $this->i_PerPage
            )
        );
    }
}

?>
