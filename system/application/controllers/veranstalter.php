<?php
require_once 'ort.php';

/**
 *  Verwaltet die Ortsangaben
 *
 * @property   integer         $i_PerPage
 * @property   boolean         $b_LogedIn
 * @property   OrtModel    $OrtModel
 */
class Veranstalter extends Ort
{
    /**
     *  Konstruktor, der das Model lädt und die View und Notice Werte setzt
     */
    public function Veranstalter()
    {
        parent::Ort();

        // Model laden
        $this->load->model('VeranstalterModel');
        $this->obj_Model = $this->VeranstalterModel;

        // View
        $this->str_View    = 'veranstalter';
        $this->str_IDField = 'veranstalterID';

        // Notice
        $this->arr_Notice = array(
            'activate'   => 'Der Veranstalter wurde aktiviert',
            'deactivate' => 'Der Veranstalter wurde deaktiviert',
            'delete'     => 'Veranstalter können nicht gelöscht werden',
            'save'       => 'Der Veranstalter wurde angelegt',
            'update'     => 'Der Veranstalter wurde aktuallisiert',
            'error'      => 'Parameter Fehler'
        );
    }

    /**
     *  Zeigt eine pagninierte Liste mit allen vorhandenen Orten an
     *
     * @param string $str_SortBy Feld nachdem sortiert wird
     * @param string $str_SortTyp Sortier Art
     * @param integer $i_Start Beginn der Anzuzeigenden Einträge
     */
    public function index($str_SortBy = 'veranstalterName', $str_SortTyp = 'ASC', $i_Start = 0)
    {
        parent::index($str_SortBy, $str_SortTyp, $i_Start);
    }

    /**
     *  Abhängigen Daten, d.h. per ID verknüpfte Felder in anderen Tabelle laden
     *
     * @return array
     */
    protected function loadDependingData()
    {
        $data = array();

        $this->load->model('TypeModel');
        $data['arr_Type'] = $this->TypeModel->getAll();

        return $data;
    }

    /**
     *  Lädt die in die Eingabe Felder eingebene Daten und gibt sie als Array zurück
     *
     * @return array
     */
    protected function loadInputData()
    {
        $data = array(
            'veranstalterName'    => $this->input->post('veranstalterName'),
            'veranstalterShort'   => $this->input->post('veranstalterShort'),
            'veranstalterDisplay' => $this->input->post('veranstalterDisplay'),
            'veranstalterType'    => $this->input->post('veranstalterType'),
            'website'             => $this->input->post('website'),
            'flyer'               => $this->input->post('flyer')
        );

        if ($this->input->post('activated') == 'on') {
            $data['activated'] = 1;
        } else {
            $data['activated'] = 0;
        }

        return $data;
    }
}
