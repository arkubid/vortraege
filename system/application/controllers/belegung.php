<?php
/**
 *
 * @property boolean           $b_LogedIn
 * @property BelegungModel    $BelegungModel
 */
class Belegung extends Controller
{
    protected $b_LogedIn;
    protected $i_PerPage;

    public function Belegung()
    {
        parent::Controller();

        // Paginierung und Auth
        $this->i_PerPage = 20;
        $this->b_LogedIn = false;
        $this->load->library('tank_auth');
        $this->b_LogedIn = $this->tank_auth->is_logged_in();

        // Model laden
        $this->load->model('BelegungModel');
    }

    /**
     *  Zeigt eine pagninierte Liste mit allen vorhandenen Belegungen an
     *
     * @param string $str_SortBy Feld nachdem sortiert wird
     * @param string $str_SortTyp Sortier Art
     * @param integer $i_Start Beginn der Anzuzeigenden Einträge
     */
    public function index($str_SortBy = 'datum', $str_SortTyp = 'ASC', $i_Start = 0)
    {
        if ($this->b_LogedIn == true) {
            // Daten laden
            $arr_Data                = array();
            $arr_Data['arr_Result']  = $this->BelegungModel->getAll(
                $i_Start,
                $this->i_PerPage,
                $str_SortBy,
                $str_SortTyp
            );
            $arr_Data['i_Results']   = $this->BelegungModel->getNumberOfRecords();
            $arr_Data['i_First']     = $i_Start + 1;
            $arr_Data['i_Last']      = min($i_Start + 1 + $this->i_PerPage, $arr_Data['i_Results']);
            $arr_Data['str_SortTyp'] = $str_SortTyp;
            $arr_Data['str_SortBy']  = $str_SortBy;

            // Daten und Pagination
            $this->setPagination($str_SortBy . '/' . $str_SortTyp, $arr_Data['i_Results']);
            $this->load->view('belegung/index.php', $arr_Data);

            // Footer
            $this->setFooter();
        } else {
            // ?
        }
    }

    public function edit($i_ID)
    {
        if (isset($i_ID) == true
            && is_int((int)$i_ID) == true
        ) {
            $this->load->model('OrtModel');
            $this->load->model('VeranstalterModel');

            // Daten laden
            $arr_Data                     = array('arr_Result' => $this->BelegungModel->getOne($i_ID));
            $arr_Data['arr_Ort']          = $this->OrtModel->getAll();
            $arr_Data['arr_Veranstalter'] = $this->VeranstalterModel->getAll();

            $this->load->view('belegung/edit.php', $arr_Data);
        } else {
            $this->index();
        }
    }

    public function create()
    {

    }

    public function delete($i_ID)
    {

    }

    public function isfree($str_Datum, $str_Beginn, $str_Ende, $str_Location = 'hoersaal', $i_BelegungsID = '')
    {
        echo $this->BelegungModel->isFreeDate($str_Datum, $str_Beginn, $str_Ende, $str_Location, $i_BelegungsID);
    }

    /**
     *  Setzt den HTML Ansichts Kopf und die Navigation
     */
    protected function setHeader()
    {
        // View laden
        $this->load->view('layout/header.php');
        $arr_Data = array(
            'str_Activ'   => 'belegung',
            'b_LogedIn'   => $this->b_LogedIn,
            'arr_SubMenu' => array(
                array(
                    'str_Target'  => 'rss',
                    'str_Class'   => 'feed',
                    'str_Window'  => '_blank',
                    'str_Display' => 'RSS Feed',
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'download',
                    'str_Class'   => 'pdf',
                    'str_Window'  => '_blank',
                    'str_Display' => 'Download',
                    'b_Login'     => false
                ),
                array(
                    'str_Target'  => 'all',
                    'str_Class'   => '',
                    'str_Window'  => '',
                    'str_Display' => 'Alle Vortr&auml;ge',
                    'b_Login'     => true
                ),
                array(
                    'str_Target'  => 'new',
                    'str_Class'   => '',
                    'str_Window'  => '',
                    'str_Display' => 'Vortrag anlegen',
                    'b_Login'     => true
                )
            )
        );

        $this->load->view('layout/navigation.php', $arr_Data);
    }

    /**
     *  Abschluss Zeile anzeigen
     */
    protected function setFooter()
    {
        $this->load->view('layout/footer.php', array('b_LogedIn' => $this->b_LogedIn));
    }

    /**
     *  Erzeugt den pagninieruns Effekt
     *
     * @param string $str_Sort Storierung
     * @param integer $i_Total Gesamt Anzahl an Einträgen
     */
    protected function setPagination($str_Sort, $i_Total)
    {
        // Ensure the pagination library is loaded
        $this->load->library('pagination');

        // This is messy. I'm not sure why the pagination class can't work
        // this out itself...
        $uri_segment = count(explode('/', '/vortraege/all/' . $str_Sort));

        // Initialise the pagination class, passing in some minimum parameters
        $this->pagination->initialize(
            array(
                'base_url'       => site_url('/vortraege/all/' . $str_Sort),
                'full_tag_open'  => '<p class="postmeta">',
                'full_tag_close' => '</p>',
                'first_link'     => 'Erster',
                'next_link'      => '&rsaquo;&rsaquo;&rsaquo;',
                'prev_link'      => '&lsaquo;&lsaquo;&lsaquo;',
                'uri_segment'    => $uri_segment,
                'total_rows'     => $i_Total,
                'per_page'       => $this->i_PerPage
            )
        );
    }
}

?>