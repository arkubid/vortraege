<?php
    require_once dirname(__FILE__) . '/configuration.php';
    
    /**
     *  Verwaltet die Liste der Benutzer, zugleich Grund Controller, der weiter abgeleitet wird
     * 
     *  @property   FeedModel   $obj_Model  Feed Model Objekt
     */
    class Feed extends Configuration {        
        /**
         *  Konstrukor, der die Start Werte (Paginierung, View) etc. setzt und das Model lädt
         */
        public function Feed() {
            parent::Configuration();
           
            // Model laden
            $this->load->model('FeedModel');
            $this->obj_Model    =   $this->FeedModel;

            // View Verzeichnis
            $this->str_View     =   'feed';
            $this->arr_Notice   =   array(
                                        'created'   =>  'Der RSS-Feed wurde erstellt',
                                        'missing'   =>  'Der RSS-Feed konnte nicht geladen werden',
                                        'updated'   =>  'Der RSS-Feed wurde aktualisiert',
                                        'deleted'   =>  'Der RSS-Feed wurde gelöscht',
                                        'noAccess'  =>  'Bitte zuerst anmelden',
                                        'existing'  =>  'Dieser Link wird bereits verwendet!');
	}

	/**
         *  Zeigt eine pagninierte Liste mit allen vorhandenen Feed Konfigurationen an.
         *
         *  @param string $str_SortBy   Feld nachdem sortiert wird
         *  @param string $str_SortTyp  Sortier Art
         *  @param integer $i_Start     Beginn der Anzuzeigenden Einträge
         */
        public function index($str_SortBy = 'link', $str_SortTyp = 'ASC', $i_Start = -1) {
            if ($this->b_LogedIn == true) {
                // Daten laden
                $arr_Data   =   array(
                    'arr_Result'    =>   $this->obj_Model->getAll($i_Start, $this->i_PerPage, $str_SortBy, $str_SortTyp),
                    'i_Results'     =>   $this->obj_Model->getNumberOfRecords(),
                    'i_First'       =>   $i_Start + 1,
                    'i_Last'        =>   min($i_Start + 1 + $this->i_PerPage, $arr_Data['i_Results']),
                    'str_SortTyp'   =>   $str_SortTyp,
                    'str_SortBy'    =>   $str_SortBy,
                    'str_Notice'    =>   $this->str_Notice);
                
                // Daten und Pagination
                $this->setHeader();
                $this->setPagination($str_SortBy . '/' . $str_SortTyp, $arr_Data['i_Results']);
                $this->load->view($this->str_View . '/index.php', $arr_Data);
                $this->setFooter();
            }
            else {
                redirect('/vortraege');
            }
	}
        
        public function rss($str_Link = '') {
            $arr_Feed   =   $this->obj_Model->findByLink($str_Link);
            
            if ($arr_Feed !== false) {                
                $this->load->helper('xml');
                $this->load->helper('text');
                $this->load->helper('rss');            
                $this->load->model('VortraegeModel');
                
                // Zusätzliche Bedingung für den Feed             
                $arr_Where  =   array();
                if ($arr_Feed['configuration']['ort'] !== false) {    
                    $arr_Where['vortrag.ort']   =   $arr_Feed['configuration']['ort'];
                }
                
                if ($arr_Feed['configuration']['stadt'] !== false) {    
                    $arr_Where['vortrag.stadt']   =   $arr_Feed['configuration']['stadt'];
                }
                
                if ($arr_Feed['configuration']['veranstalter'] !== false) {    
                    $arr_Where['vortrag.ort']   =   $arr_Feed['configuration']['veranstalter'];
                }
                
                if ($arr_Feed['configuration']['type'] !== false) {    
                    $arr_Where['vortrag.type']   =   $arr_Feed['configuration']['type'];
                }

                // Vorträge und/oder Kolloquien
                if ($arr_Feed['configuration']['kolloquium'] !== '-1') {
                    $arr_Where['vortrag.kolloquium']    =   $arr_Feed['configuration']['kolloquium'];
                }                

                $arr_Data   =   array(
                    'str_Name'          =>   $arr_Feed['titel'],
                    'str_Encoding'      =>   'utf-8',
                    'str_Url'           =>   base_url() . 'feed/rss/' . $str_Link,
                    'str_Description'   =>   $arr_Feed['titel'],
                    'str_Language'      =>   'de-de',
                    'str_Mail'          =>   'archinst@uni-bonn.de',
                    'arr_Data'          =>   formatFeedContent($this->VortraegeModel->getList(array(), $arr_Where), false));

                header('Content-Type: application/rss+xml');
                $this->load->view('feed/rss.php', $arr_Data);
            }
            else {
                redirect('vortraege/rss');
            }
        }


        /**
         *  Maske zum erstellen einer neuen RSS-Feed Konfiguration
         */
        public function create() {
            if ($this->b_LogedIn == true) {
                $arr_Data   =   array('str_Notice' =>   $this->str_Notice);

                // View anzeigen
                $this->setHeader();
                $this->load->view($this->str_View . '/create.php', array_merge($arr_Data, $this->loadDependingData()));
                $this->setFooter();
            }
            else {
                redirect('/vortraege');
            }
        }

        /**
         *  Erstellt eine neue Feed-Konfiguration
         */
        public function save() {
            if ($this->b_LogedIn == true) {
                $this->load->library('form_validation');

                if ($this->form_validation->run($this->str_View) == true) {
                    $arr_Data   =   $this->loadInputData();

                    if ($this->obj_Model->create($arr_Data) == true) {
                        $this->str_Notice   =   $this->arr_Notice['created'];
                        $this->index();
                    }
                    else {
                        $this->str_Notice   =   $this->arr_Notice['existing'];
                        $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                        $this->create();
                    }
                }
                else {
                    $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                    $this->create();
                }                         
            }
            else {
                redirect('/vortraege');
            }
        }

        /**
         *  Zeigt die Maske zum editieren einer bestehenden Feed Konfiguration an.
         * 
         *  @param  integer $i_ID   ID
         */
        public function edit($i_ID)
        {
            if ($this->grantAccess($i_ID) == true) {                   
                // Daten laden
                $arr_Result    =   $this->obj_Model->find($i_ID);  
                
                if ($arr_Result !== false) {    
                    $arr_Data   = array_merge(
                                            array(
                                                'arr_Result'    =>  $arr_Result,
                                                'str_Notice'    =>   $this->str_Notice), 
                                            $this->loadDependingData());

                    $this->setHeader();
                    $this->load->view($this->str_View . '/edit.php', $arr_Data);
                    $this->setFooter();
                }
                else {
                    $this->str_Notice   =   $this->arr_Notice['missing'];
                    $this->index();
                }                
            }
            else {
                redirect('/vortraege');
            }
        }
        
        /**
         *  Aktualisiert die Einstellungen für die Feed Konfiguration
         * 
         *  @param  integer $i_ID   ID
         */
        public function update($i_ID)
        {                        
            if ($this->grantAccess($i_ID) == true) {                
                $this->load->library('form_validation');

                if ($this->form_validation->run($this->str_View) == true) {
                    $arr_Data   =   $this->loadInputData();

                    if ($this->obj_Model->update($i_ID, $arr_Data) == true) {
                        $this->str_Notice   =   $this->arr_Notice['updated'];
                        $this->index();
                    }
                    else {
                        $this->str_Notice   =   $this->arr_Notice['existing'];
                        $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                        $this->edit($i_ID);
                    }
                }
                else {
                    $this->form_validation->set_error_delimiters('<div id="error" class="error">', '</div>');
                    $this->edit($i_ID);
                }                                
            }
            else {
                redirect('/vortraege');
            }
        }
        
        /**
         *  Löscht die Feed Konfiguration
         *
         *  @param integer $i_ID    ID
         */
        public function delete($i_ID) {
            if ($this->grantAccess($i_ID) == true) {
                $this->obj_Model->delete($i_ID);
                $this->str_Notice   =   $this->arr_Notice['deleted'];
                $this->index();
            }
            else {
                redirect('/vortraege');
            }
        }
     
        /**
         *  Abhängigen Daten, d.h. per ID verknüpfte Felder in anderen Tabelle laden
         *
         *  @return array
         */
        protected function loadDependingData()
        {            
            $this->load->model('TypeModel');            
            $this->load->model('OrtModel');            
            $this->load->model('StadtModel');
            $this->load->model('VeranstalterModel');
            
            return array(
                        'arr_Type'          =>  $this->TypeModel->getAll(),
                        'arr_Ort'           =>  $this->OrtModel->getAll(),
                        'arr_Stadt'         =>  $this->StadtModel->getAll(),
                        'arr_Veranstalter'  =>  $this->VeranstalterModel->getAll());
        }
        
        /**
         *  Lädt die in die Eingabe Felder eingebene Daten und gibt sie als Array zurück
         *
         *  @return array
         */
        protected function loadInputData() {
            $arr_Return =   array(
                                'link'          =>  $this->input->post('link'),
                                'titel'         =>  $this->input->post('titel'),
                                'configuration' =>  array(
                                    'ort'           =>  $this->input->post('ort'),
                                    'stadt'         =>  $this->input->post('stadt'),
                                    'veranstalter'  =>  $this->input->post('veranstalter'),
                                    'type'          =>  $this->input->post('type'),
                                    'kolloquium'    =>  $this->input->post('kolloquium')));
            
            foreach($arr_Return['configuration'] as $str_Option => $arr_Values) {
                foreach($arr_Return['configuration'][$str_Option] as $str_Key => $str_Value) {
                    if ($arr_Return['configuration'][$str_Option][$str_Key] == '') {
                        unset($arr_Return['configuration'][$str_Option][$str_Key]);
                    }
                }
                
                if (count($arr_Return['configuration'][$str_Option]) > 0) {
                    $arr_Return['configuration'][$str_Option]   =   array_values($arr_Return['configuration'][$str_Option]);
                }
                else {
                    $arr_Return['configuration'][$str_Option]   =   null;
                }
            }
            
            return $arr_Return;
        }
        
        /**
         *  Prüft, ob eine numerische ID übergeben wurde und ob ein Benutzer angemeldet ist.
         * 
         *  @param  intgere $i_ID   ID
         *  @return boolean
         */
        private function grantAccess($i_ID)
        {
            if (isset($i_ID) == true
                && is_int((int)$i_ID) == true
                && $this->b_LogedIn == true) {                      
                    return true;
            }      
            
            $this->str_Notice   =   $this->arr_Notice['noAccess'];
            return false;
        }
    }
?>
