<?php
    /**
     *
     *  @property boolean           $b_LogedIn
     *  @property VortraegeModel    $VortraegeModel
     *  @property PDF               $pdf
     */
    class Help extends Controller {
        protected   $b_LogedIn;
        protected   $i_PerPage;
        protected   $str_Notice;

	public function Help() {
            parent::Controller();

            // Paginierung und Auth
            $this->i_PerPage    =   50;
            $this->b_LogedIn    =   false;
            $this->load->library('tank_auth');
            $this->b_LogedIn    =   $this->tank_auth->is_logged_in();

            // Model laden
            $this->load->model('VortraegeModel');
	}

	/**
         *  Zeigt die Liste mit den aktuellen Vorträgen an
         */
	public function index() {            
            $this->vortraege();
	}

        /**
         *  Hilfe zum PDF
         */
        public function download() {            
            $arr_Data               =   array();
            $arr_Data['str_Notice'] =   $this->str_Notice;

            $this->setHeader();
            $this->load->view('help/download.php', $arr_Data);
            $this->setFooter();
        }

        /**
         *  Hilfe zum Google Kalender
         */
        public function calendar() {
            $arr_Data               =   array();
            $arr_Data['str_Notice'] =   $this->str_Notice;

            $this->setHeader();
            $this->load->view('help/calendar.php', $arr_Data);
            $this->setFooter();
        }

        /**
         *  Hilfe zum RSS Feed
         */
        public function rss() {
            $arr_Data               =   array();
            $arr_Data['str_Notice'] =   $this->str_Notice;

            $this->load->model('TypeModel');
            $this->load->model('StadtModel');
            $this->load->model('OrtModel');
            $this->load->model('VeranstalterModel');

            $arr_Data['arr_Type']           =   $this->TypeModel->getAll();
            $arr_Data['arr_Stadt']          =   $this->StadtModel->getAll();
            $arr_Data['arr_Veranstalter']   =   $this->VeranstalterModel->getAll();
            $arr_Data['arr_Ort']            =   $this->OrtModel->getAll();

            $this->setHeader();
            $this->load->view('help/rss.php', $arr_Data);
            $this->setFooter();
        }

        /**
         *  Hilfe zum einbinden per Googel Feed API
         */
        public function website() {
            $arr_Data               =   array();
            $arr_Data['str_Notice'] =   $this->str_Notice;

            $this->setHeader();
            $this->load->view('help/website.php', $arr_Data);
            $this->setFooter();
        }

        /**
         *  Hilfe Allgemein
         */
        public function vortraege() {
            $arr_Data               =   array();
            $arr_Data['str_Notice'] =   $this->str_Notice;

            $this->load->model('OrtModel');
            $this->load->model('VeranstalterModel');
            $arr_Data['arr_Ort']            =   $this->OrtModel->getAll();
            $arr_Data['arr_Veranstalter']   =   $this->VeranstalterModel->getAll(-1, 0, 'veranstalterID');


            $this->setHeader();
            $this->load->view('help/vortraege.php', $arr_Data);
            $this->setFooter();
        }

        /**
         *  Impressum
         */
        public function impressum() {
            $arr_Data               =   array();
            $arr_Data['str_Notice'] =   $this->str_Notice;

            $this->setHeader();
            $this->load->view('help/impressum.php', $arr_Data);
            $this->setFooter();
        }
       
        /**
         *  Header inkulsive Menü erstellen
         */
        protected function setHeader() {
            $this->load->view('layout/header.php');
            $arr_Data   =   array(
                                'str_Activ' => 'help',
                                'b_LogedIn' => $this->b_LogedIn,
                                'arr_SubMenu' => array(
                                                    array(
                                                        'str_Target' => 'vortraege',
                                                        'str_Class' => 'actual',
                                                        'str_Window' => '',
                                                        'str_Display' => 'Vortr&auml;ge',
                                                        'b_Foreign' =>  false,
                                                        'b_Login' => false),
                                                    array(
                                                        'str_Target' => 'download',
                                                        'str_Class' => 'pdf',
                                                        'str_Window' => '',
                                                        'str_Display' => 'Download',
                                                        'b_Foreign' =>  false,
                                                        'b_Login' => false),
                                                    array(
                                                        'str_Target' => 'calendar',
                                                        'str_Class' => 'calendar',
                                                        'str_Window' => '',
                                                        'str_Display' => 'Kalender',
                                                        'b_Foreign' =>  false,
                                                        'b_Login' => false),
                                                    array(
                                                        'str_Target' => 'rss',
                                                        'str_Class' => 'feed',
                                                        'str_Window' => '',
                                                        'str_Display' => 'RSS Feed',
                                                        'b_Foreign' =>  false,
                                                        'b_Login' => false),
                                                    array(
                                                        'str_Target' => 'website',
                                                        'str_Class' => 'website',
                                                        'str_Window' => '',
                                                        'str_Display' => 'Eigene Homepage',
                                                        'b_Foreign' =>  false,
                                                        'b_Login' => false),
                                                    array(
                                                        'str_Target' => 'impressum',
                                                        'str_Class' => 'impressum',
                                                        'str_Window' => '',
                                                        'str_Display' => 'Impressum',
                                                        'b_Foreign' =>  false,
                                                        'b_Login' => false)));

            $this->load->view('layout/navigation.php', $arr_Data);
        }

        /**
         *  Abschluss Zeile anzeigen
         */
        protected function setFooter() {
            $this->load->view('layout/footer.php', array('b_LogedIn' => $this->b_LogedIn));
        }        
    }
?>
