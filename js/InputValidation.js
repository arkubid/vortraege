    $(function() {
            $("#datum").datepicker({
                    showOn: 'button',
                    buttonImage: 'css/images/calendar.png',
                    buttonImageOnly: true,
                    regional: 'de'});
    });

    /**
     *  Übertrag den Beginn der Veranstalltung mit einem unterschied von plus 2 Stunden auf das Ende. Prüft zudem, ob dass
     *  Datum sinnvoll ist, d. h. der Beginn vor dem Ende liegt
     */
    function isRoomFree() {                
        var i_Hour      =   $('#beginnHour').val();
        var i_Minute    =   $('#beginnMinute').val();

        if (i_Hour > 0
            && $('#endeHour').val() == '00') {
                $('#endeHour').val(1 * i_Hour + 2);
        }

        if (i_Minute > 0
            && $('#endeMinute').val() == '00') {
                $('#endeMinute').val(i_Minute);
        }

        isDateCorrect();
    }

    /**
     *  Prüft ob die Zeitangaben korrekt sind, d.h. der Beginn zeitlich vor dem Ende liegt
     */
    function isDateCorrect() {
        var b_Result    =   false;

        if ($('#beginnHour').val() < $('#endeHour').val()) {
            b_Result    =   true;
        }
        else if ($('#beginnHour').val() == $('#endeHour').val()
            && $('#beginnMinute').val() < $('#endeMinute').val()) {
                b_Result    =   true;
        }

        if (b_Result == false) {
            $('#time').addClass('notice');
            $('#time').html('Der Beginn des Vortrages muss zeitlich vor dem Ende liegen');
            $('#save').attr('disabled', true);
        }
        else {
            $('#time').removeClass();
            $('#time').html('');
            $('#save').removeAttr('disabled');
        }

        return b_Result;
    }    
