
--
-- Tabellenstruktur für Tabelle `belegung`
--

CREATE TABLE IF NOT EXISTS `belegung` (
  `belegungID` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `beginn` time NOT NULL,
  `ende` time NOT NULL,
  `ct` tinyint(1) NOT NULL,
  `hoersaal` tinyint(1) NOT NULL,
  `museum` tinyint(1) NOT NULL,
  `vortrag` int(11) NOT NULL,
  `veranstaltung` int(11) NOT NULL,
  `url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`belegungID`),
  KEY `vortrag` (`vortrag`),
  KEY `veranstaltung` (`veranstaltung`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=443 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `captchaID` int(11) NOT NULL AUTO_INCREMENT,
  `zeit` int(10) NOT NULL,
  `ipadresse` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `word` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`captchaID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30311 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `feed`
--

CREATE TABLE IF NOT EXISTS `feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `titel` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `configuration` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `newsletterID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `activ` tinyint(1) NOT NULL,
  PRIMARY KEY (`newsletterID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=285 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `newsletter_test`
--

CREATE TABLE IF NOT EXISTS `newsletter_test` (
  `newsletterID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `activ` tinyint(1) NOT NULL,
  PRIMARY KEY (`newsletterID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=207 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ort`
--

CREATE TABLE IF NOT EXISTS `ort` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ort` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `maps` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `stadt` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stadt` (`stadt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `raum`
--

CREATE TABLE IF NOT EXISTS `raum` (
  `raumID` int(11) NOT NULL AUTO_INCREMENT,
  `raum` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`raumID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stadt`
--

CREATE TABLE IF NOT EXISTS `stadt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `titel` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstalter`
--

CREATE TABLE IF NOT EXISTS `veranstalter` (
  `veranstalterID` int(11) NOT NULL AUTO_INCREMENT,
  `veranstalterName` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `veranstalterShort` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `veranstalterDisplay` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `veranstalterType` int(11) NOT NULL,
  `website` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `activated` tinyint(1) NOT NULL,
  `flyer` int(11) NOT NULL,
  PRIMARY KEY (`veranstalterID`),
  KEY `type` (`veranstalterType`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstaltung`
--

CREATE TABLE IF NOT EXISTS `veranstaltung` (
  `veranstaltungID` int(11) NOT NULL,
  `titel` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`veranstaltungID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vortrag`
--

CREATE TABLE IF NOT EXISTS `vortrag` (
  `vortragID` int(11) NOT NULL AUTO_INCREMENT,
  `referent` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `herkunft` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `titel` varchar(800) COLLATE utf8_unicode_ci NOT NULL,
  `ort` int(11) NOT NULL,
  `veranstalter` int(11) NOT NULL,
  `kolloquium` tinyint(1) NOT NULL,
  `activ` tinyint(1) NOT NULL,
  `type` int(11) NOT NULL,
  `stadt` int(11) NOT NULL,
  `attachment` tinyint(1) NOT NULL,
  `link` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`vortragID`),
  KEY `ort` (`ort`,`veranstalter`),
  KEY `veranstalter` (`veranstalter`),
  KEY `type` (`type`,`stadt`),
  KEY `stadt` (`stadt`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=453 ;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `ort`
--
ALTER TABLE `ort`
  ADD CONSTRAINT `ort_ibfk_1` FOREIGN KEY (`stadt`) REFERENCES `stadt` (`id`);

--
-- Constraints der Tabelle `veranstalter`
--
ALTER TABLE `veranstalter`
  ADD CONSTRAINT `veranstalter_ibfk_1` FOREIGN KEY (`veranstalterType`) REFERENCES `type` (`id`);

--
-- Constraints der Tabelle `vortrag`
--
ALTER TABLE `vortrag`
  ADD CONSTRAINT `vortrag_ibfk_1` FOREIGN KEY (`ort`) REFERENCES `ort` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `vortrag_ibfk_2` FOREIGN KEY (`veranstalter`) REFERENCES `veranstalter` (`veranstalterID`) ON DELETE NO ACTION,
  ADD CONSTRAINT `vortrag_ibfk_3` FOREIGN KEY (`type`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `vortrag_ibfk_4` FOREIGN KEY (`stadt`) REFERENCES `stadt` (`id`);

--
-- Aktuelle Grunddaten
--

--
-- Daten für Tabelle `stadt`
--

INSERT INTO `stadt` (`id`, `name`) VALUES
(1, 'Bonn'),
(2, 'Köln');


--
-- Daten für Tabelle `type`
--

INSERT INTO `type` (`id`, `name`, `titel`) VALUES
(1, 'Klassisch', 'Klassisch archäologische'),
(2, 'Prähistorisch', 'Prähistorische'),
(3, 'Christlich', 'Christlich archäologische'),
(4, 'Ägyptologisch', 'Ägyptologische'),
(5, 'Althistorisch', 'Althistorische'),
(6, 'Provinzialrömisch', 'Provinzialrömische'),
(7, 'Altamerikanistisch', 'Altamerikanistische');

--
-- Daten für Tabelle `ort`
--

INSERT INTO `ort` (`id`, `ort`, `adresse`, `maps`, `website`, `stadt`, `activated`) VALUES
(1, 'AI-Bonn', 'Akademisches Kunstmuseum, Am Hofgarten 21, Bonn', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Akademisches+Kunstmuseum,+Am+Hofgarten+21,+53133+Bonn&sll=50.734533,7.106137&sspn=0.004876,0.011362&ie=UTF8&hq=Akademisches+Kunstmuseum&hnear=Akademisches+Kunstmuseum,+Am+Hofgarten+21,+Bonn,+Deutschland&ll=50.731614,7.106824&spn=0.004876,0.011362&t=h&z=17&iwloc=A', 'www.ai.uni-bonn.de', 1, 1),
(2, 'AI-Köln', 'Archäologisches Institut, Kerpener Straße 30 / Eingang Weyertal, Köln', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Arch%C3%A4ologisches+Institut,+Kerpener+Stra%C3%9Fe+30,+50931+K%C3%B6ln&sll=50.97767,6.849941&sspn=1.241664,2.90863&ie=UTF8&hq=Arch%C3%A4ologisches+Institut,&hnear=Kerpener+Stra%C3%9Fe+30,+K%C3%B6ln,+Deutschland&ll=50.924807,6.925646&spn=0.004856,0.011362&t=h&z=17&iwloc=A', 'www.archaeologie.uni-koeln.de', 2, 1),
(3, 'WZB', 'Wissenschafts Zentrum Bonn, Ahrstraße 45, Bonn', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Deutsches+Museum+Bonn,+Ahrstra%C3%9Fe+45,+53175+Bonn&sll=50.924807,6.925646&sspn=0.004856,0.011362&ie=UTF8&hq=Deutsches+Museum+Bonn&hnear=Deutsches+Museum+Bonn,+Ahrstra%C3%9Fe+45,+Bonn,+Deutschland&ll=50.700644,7.148978&spn=0.004879,0.011362&t=h&z=17&iwloc=near', 'www.deutsches-museum.de/bonn', 1, 1),
(4, 'LMB', 'Rheinisches Landesmuseum Bonn, Colmantstraße 14-16, Bonn', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Rheinisches+Landesmuseum+Bonn,+Colmantstra%C3%9Fe+14-16,+53115+Bonn&sll=50.700644,7.148978&sspn=0.004879,0.011362&ie=UTF8&hq=Rheinisches+LandesMuseum+Bonn&hnear=Rheinisches+LandesMuseum+Bonn,+Colmantstra%C3%9Fe+14-16,+Bonn,+Deutschland&ll=50.73179,7.093467&spn=0.004876,0.011362&t=h&z=17&iwloc=A', 'www.rlmb.lvr.de', 1, 1),
(5, 'KAH', 'Kunst- und Ausstellungshalle der BRD, F.-Ebert-Allee 4, Bonn', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Kunst-+und+Ausstellungshalle+der+Bundesrepublik+Deutschland,+Friedrich-Ebert-Allee+4,+53113+Bonn&sll=50.73179,7.093467&sspn=0.004876,0.011362&ie=UTF8&hq=Kunst-+und+Ausstellungshalle+der+Bundesrepublik+Deutschland,&hnear=Friedrich-Ebert-Allee+4,+Bonn,+Deutschland&ll=50.714334,7.122617&spn=0.004878,0.011362&t=h&z=17&iwloc=A', 'www.kah-bonn.de', 1, 1),
(6, 'RGM', 'Römisch-Germanisches Museum, Roncalliplatz 4, Köln', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=R%C3%B6misch-Germanisches+Museum,+Roncalliplatz+4,+50667+K%C3%B6ln&sll=50.714334,7.122617&sspn=0.004878,0.011362&ie=UTF8&hq=&hnear=R%C3%B6misches-Germanisches+Museum,+Roncalliplatz+4,+50667+K%C3%B6ln,+Deutschland&ll=50.940826,6.958101&spn=0.009708,0.022724&t=h&z=16&iwloc=A', 'www.museenkoeln.de/roemisch-germanisches-museum', 2, 1),
(7, 'KAAK', 'Kommission für Archäologie Außereuropäischer Kulturen, Dürenstraße 35-37, Bad Godesberg', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Kommission+f%C3%BCr+Arch%C3%A4ologie+Au%C3%9Fereurop%C3%A4ischer+Kulturen,+D%C3%BCrenstra%C3%9Fe.+35-37,+53173+Bonn+-+Bad+Godesberg&sll=50.940826,6.958101&sspn=0.009708,0.022724&ie=UTF8&cd=1&hq=Kommission+f%C3%BCr+Arch%C3%A4ologie+Au%C3%9Fereurop%C3%A4ischer+Kulturen,&hnear=D%C3%BCrenstra%C3%9Fe+35,+Bonn,+Deutschland&ll=50.686528,7.164545&spn=0.004881,0.011362&t=h&z=17&iwloc=A', 'www.dainst.org/kaak', 1, 1),
(8, 'IKM', 'Internationales Kolleg Morphomata, Weyertal 59 (Rückgebäude), Köln', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=Weyertal+59,+50937+K%C3%B6ln&sll=50.932314,6.934827&sspn=0.01942,0.045447&ie=UTF8&hq=&hnear=Weyertal+59,+Lindenthal+50937+K%C3%B6ln,+Nordrhein-Westfalen,+Deutschland&ll=50.924584,6.926579&spn=0.002428,0.005681&t=h&z=18&iwloc=A', 'www.ik-morphomata.uni-koeln.de', 2, 1),
(9, 'Ägyptologie Bonn', 'Ägyptisches Museum Regina-Pacis-Weg 7, Bonn', 'http://maps.google.com/maps?f=q&hl=de&q=%C3%84gyptisches+Museum,+Regina-Pacis-Weg+7,+53113+Bonn&ie=UTF8&hq=%C3%84gyptisches+Museum,&hnear=Regina-Pacis-Weg+7,+Bonn,+Deutschland&ll=50.734533,7.106137&spn=0.004876,0.011362&t=h&z=17&iwloc=A', 'www.ika.uni-bonn.de/abteilungen/aegyptologie', 1, 1),
(10, 'UFG-Köln', 'Hörsaalgebäude, Hörsaal D, Albertus Magnus Platz, Köln', 'http://maps.google.com/maps?q=H%C3%B6rsaalgeb%C3%A4ude,+H%C3%B6rsaal+D,+Albertus+Magnus+Platz,+50923+K%C3%B6ln&hl=de&cd=1&ei=0y7tTLSKKZusuwOgvs2fCA&sig2=lMWKuAtB3GbqB5qC0aKTig&sll=50.928244,6.92826&sspn=0.027325,0.069633&ie=UTF8&view=map&cid=1894004418251550363&hq=H%C3%B6rsaalgeb%C3%A4ude,+H%C3%B6rsaal+D,+Albertus+Magnus+Platz,+50923+K%C3%B6ln&hnear=&ll=50.931063,6.928253&spn=0.011198,0.027595&t=h&z=15&iwloc=A', 'http://www.ufg.uni-koeln.de/', 2, 1),
(11, 'Alte Geschichte Bonn', 'Hauptgebäude der Universität, Seminarräume der Alten Geschichte, Am Hof 1e, Bonn', 'http://maps.google.com/maps?f=q&source=s_q&hl=de&geocode=&q=+Am+Hof+1e,+53113+Bonn&sll=50.931063,6.928253&sspn=0.011198,0.027595&ie=UTF8&hq=&hnear=Am+Hof+1,+Bonn+53113+Bonn,+Nordrhein-Westfalen,+Deutschland&ll=50.733698,7.101551&spn=0.001406,0.003449&t=h&z=18', 'http://www.ancient.uni-bonn.de/', 1, 1),
(12, 'Uni Bonn, Hörsaal I', 'Hauptgebäude, Regina-Pacis-Weg 3, Bonn', '', 'www.uni-bonn.de', 1, 1),
(13, 'Uni Köln, Hörsaal XII', 'Hauptgebäude, Albertus-Magnus Platz 1, Köln', '', '', 2, 1),
(14, 'Uni Bonn, Hörsaal XIV', 'Hauptgebäude, Regina-Pacis-Weg 3, 53113 Bonn', '', '', 1, 1),
(15, 'Amélie-Thyssen-Auditorium', 'Apostelnkloster 13-15, Köln', '', '', 2, 1),
(16, 'DFG', 'Geschäftsstelle, Norbert Elias-Saal, Kennedyallee 40, Bad Godesberg', '', '', 1, 1),
(17, 'Altamerikanistik Bonn', 'Oxfordstraße 15, Bonn', '', 'www.aeb.uni-bonn.de', 1, 1),
(18, 'Uni Bonn, Hörsaal XV', 'Hauptgebäude, Regina-Pacis-Weg 3, Bonn', '', '', 1, 1),
(19, 'Uni Bonn, Hörsaal XVII', 'Hauptgebäude, Englisches Seminar, Regina-Pacis-Weg 3, Bonn', '', '', 1, 1),
(20, 'Uni Bonn, Hörsaal IV', 'Hauptgebäude, Regina-Pacis-Weg 3, Bonn', '', '', 1, 1);


--
-- Daten für Tabelle `veranstalter`
--

INSERT INTO `veranstalter` (`veranstalterID`, `veranstalterName`, `veranstalterShort`, `veranstalterDisplay`, `veranstalterType`, `website`, `activated`) VALUES
(1, 'Universität Bonn, Klassische Archäologie', 'Bonn, KA', '', 1, 'www.ai.uni-bonn.de', 1),
(2, 'Verein von Altertumsfreunden im Rheinlande', 'AV', 'AV-Vortrag', 1, 'www.av-rheinland.de', 1),
(3, 'Universität Bonn, Vor- und Frühgeschichtliche Archäologie', 'Bonn, VFG', 'Vortrag der VFG', 2, 'www.vfgarch.uni-bonn.de', 1),
(4, 'Universität Bonn, Christliche Archäologie', 'Bonn, CA', 'Vortrag der Christlichen Archäologie', 3, 'www.khi.uni-bonn.de/christliche-archaeologie', 1),
(5, 'Förderverein des Akademischen Kunstmuseums', 'Föderverein', 'Vortrag des Fördervereins', 1, 'www.antikensammlung.uni-bonn.de/foerderverein', 1),
(6, 'Theodor Wiegand Gesellschaft', 'TWG', 'TWG Vortrag', 1, 'www.twges.de', 1),
(7, 'Römisch-Germanisches Museum', 'RGM Köln', '', 1, 'www.museenkoeln.de/roemisch-germanisches-museum', 1),
(8, 'Universität Bonn, Ägyptologie', 'Bonn, ÄG', '', 4, 'www.ika.uni-bonn.de/abteilungen/aegyptologie', 1),
(9, 'Kommission für Archäologie Außereuropäischer Kulturen', 'KAAK', 'Vortrag der KAAK', 1, 'www.dainst.org/kaak', 1),
(10, 'Universität Köln, Klassische Archäologie', 'Köln, KA', '', 1, 'www.archaeologie.uni-koeln.de', 1),
(11, 'Universität Köln, Ur- und Frühgeschichtliche Archäologie', 'Köln, UFG', '', 2, 'http://www.ufg.uni-koeln.de/', 1),
(12, 'Verbund archäologischer Institutionen KölnBonn', 'VarI', 'VarI Veranstaltung', 1, 'www.varinst.de', 1),
(13, 'Deutsche Hellas-Gesellschaft', 'Hellas', 'Vortrag der deutschen Hellas-Gesellschaft', 1, 'http://www.deutsche-hellas-gesellschaft.de/wp/', 1),
(15, 'LVR LandesMuseum Bonn', 'LMB', 'Landesmuseum', 6, 'http://www.rlmb.lvr.de/', 1),
(16, 'Deutsch-Türkische Gesellschaft in Bonn', 'DTG', 'Deutsch-Türkische Gesellschaft', 1, 'http://www.dtgbonn.de/', 0),
(17, 'Institut für Geschichtswissenschaft/Alte Geschichte', 'Alte Geschichte', 'Alte Geschichte', 5, '', 1),
(18, 'Institut für Geschichtswissenschaft/Alte Geschichte', 'Alte Geschichte', 'Alte Geschichte', 5, '', 0),
(19, 'Minervia e.V. - Verein von Freunden und Förderern der Alten Geschichte an der Rheinischen Friedrich-Wilhelms-Universität Bonn', 'Minervia', 'Minervia Vortrag', 5, '', 1),
(20, 'Fachausschuss Klassische Archäologie', '', 'Fachausschuss Klassische Archäologie', 1, '', 1),
(21, 'Deutsche Forschungsgemeinschaft', 'DFG', 'DFG', 1, '', 1),
(22, 'Universität Bonn, Altamerikanistik', 'Bonn, AEB', '', 7, 'www.aeb.uni-bonn.de', 1),
(23, 'Verein von Altertumsfreunden im Rheinlande', 'Altertumsfreunde', 'Verein von Altertumsfreunden im Rheinlande', 1, '', 0),
(24, 'Landesmuseum Bonn', 'LMB', '', 1, '', 1);